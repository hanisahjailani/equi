@extends('layouts.admin.dashboard')

@section('content1')
<center><h2>Dashboard</h2></center>
	&nbsp;Dashboard /
	<hr>
	<style type="text/css">
		#click:hover
		{
			background-color: #95afc0;
		}
		#pickup:hover
		{
			background-color: #fdcb6e;
		}
		#return:hover
		{
			background-color: #00b894;
		}
		#overdue:hover
		{
			background-color: #d63031;
		}
		#app:hover
		{
			background-color: #2980b9;
		}
	</style>

	<div class="row">
		@foreach($roleAccesses as $roleAcces)
		@if($roleAcces->access->menu == "Asset")
		<div class="col-md-4">
			<a href="{{ route('adminitems.index') }}"><div class="col-md-12 alert-default" id="click" style="border-radius: 15px; border: 2px solid #2980b9; color: black">
				<center>
					<label style="font-size: 60px">{{ $asset }}</label>
					<p style="font-size: 20px">Total Assets </p>
				</center>
			</div></a>
		</div>
		@endif
		@endforeach
		@foreach($roleAccesses as $roleAcces)
		@if($roleAcces->access->menu == "Consumable")
		<div class="col-md-4">
			<a href="{{ route('adminconsumable.index') }}"><div class="col-md-12 alert-default" id="click" style="border-radius: 15px; border: 2px solid #2980b9; color: black">
				<center>
					<label style="font-size: 60px">{{ $consumable }}</label>
					<p style="font-size: 20px">Total Consumables </p>
				</center>
			</div></a>
		</div>
		@endif
		@endforeach
		@foreach($roleAccesses as $roleAcces)
		@if($roleAcces->access->menu == "License")
		<div class="col-md-4">
			<a href="{{ route('adminlicense.index') }}"><div class="col-md-12 alert-default" id="click" style="border-radius: 15px; border: 2px solid #2980b9; color: black">
				<center>
					<label style="font-size: 60px">{{ $license }}</label>
					<p style="font-size: 20px">Total Licenses </p>
				</center>
			</div></a>
		</div>
		@endif
		@endforeach
	</div>
	<br><br>
	<div class="row">
		@foreach($roleAccesses as $roleAcces)
		@if($roleAcces->access->menu == "Application")
		<div class="col-md-4">
			<div class="panel-group">
			    <div class="panel panel-default">
			      <div class="panel-heading">
			        <h4 class="panel-title">
			          <a data-toggle="collapse" href="#collapse1"><i class="far fa-calendar-check"></i>&nbsp;&nbsp;Application</a>
			        </h4>
			      </div>
			      <div id="collapse1" class="panel-collapse collapse in">
			        <div class="panel-body">
			        	<a href="{{ route('adminapplication.index') }}"><div class="col-md-12 alert-info" id="app" style="border-radius: 15px; border: 2px solid #2980b9; color: black">
			        		<center>
			        			<label style="font-size: 60px">{{ $notiReservation }}</label>
			        			<p style="font-size: 20px;">New Application(s)</p>
			        		</center>
			        	</div></a>
			        </div>
			      </div>
			    </div>
			</div>
		</div>
		@endif
		@endforeach
		@foreach($roleAccesses as $roleAcces)
		@if($roleAcces->access->menu == "Reservation")
		<div class="col-md-8">
			<div class="panel-group">
			    <div class="panel panel-default">
			      <div class="panel-heading">
			        <h4 class="panel-title">
			          <a data-toggle="collapse" href="#collapse2"><i class="far fa-calendar-alt"></i>&nbsp;&nbsp;Reservation (TODAY)</a>
			        </h4>
			      </div>
			      <div id="collapse2" class="panel-collapse collapse in">
			        <div class="panel-body">
			        	<div class="col-md-4">
			        		<a href="{{ route('adminreservation.showList',2) }}"><div class="col-md-12 alert-warning" id="pickup" style="border-radius: 15px; border: 2px solid #fdcb6e; color: black">
			        			<center>
			        				<label style="font-size: 60px">{{ $collection_date->where('application_status_id',2)->count() }}</label>
			        				<p style="font-size: 20px">Pickup </p>
			        			</center>
			        		</div></a>
			        	</div>
			        	<div class="col-md-4">
			        		<a href="{{ route('adminreservation.showList',5) }}"><div class="col-md-12 alert-success" id="return" style="border-radius: 15px; border: 2px solid #00b894; color: black">
			        			<center>
			        				<label style="font-size: 60px">{{ $return_date->where('application_status_id',5)->count() }}</label>
			        				<p style="font-size: 20px">Return </p>
			        			</center>
			        		</div></a>
			        	</div>
			        	<div class="col-md-4">
			        		<a href="{{ route('adminreservation.showList',7) }}"><div class="col-md-12 alert-danger" id="overdue" style="border-radius: 15px; border: 2px solid #d63031; color: black">
			        			<center>
			        				<label style="font-size: 60px">{{ $overdue }}</label>
			        				<p style="font-size: 20px">Overdue </p>
			        			</center>
			        		</div></a>
			        	</div>
			        </div>
			      </div>
			    </div>
			</div>
		</div>
		@endif
		@endforeach
	</div>

	<br>
<br>
@stop