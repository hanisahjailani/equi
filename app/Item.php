<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    
    public function staff()
    {
    	return $this->belongsTo('App\Staff','staff_id');
    }
    public function application_item()
    {
        return $this->hasMany('App\ApplicationItem');
    }
    public function sub_location()
    {
    	return $this->belongsTo('App\SubLocation','sub_location_id');
    }
    public function item_status()
    {
    	return $this->belongsTo('App\ItemStatus');
    }
    public function asset()
    {
        return $this->hasOne('App\Asset');
    }

    
}
