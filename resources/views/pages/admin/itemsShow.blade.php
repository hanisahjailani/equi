@extends('layouts.admin.item')

@section('content1')
<center><h2>Asset Details</h2></center>
&nbsp;<a href="{{ route('adminitems.index') }}">Assets</a> / {{ $asset->tag }} Details
<hr>
<div class="panel panel-info">
    <div class="panel-heading">Asset Info (<strong>{{ $asset->tag }}</strong>)</div>
	  	<div class="panel-body">
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="row">
						<div class="col-md-4"><label>Model</label></div>
						<div class="col-md-8">{{ $asset->model->name }} (#{{ $asset->model->model_no }})
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Serial Number / Service Tag</label></div>
						<div class="col-md-8">{{ $asset->serial_number }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Tagging Name</label></div>
						<div class="col-md-8">{{ $asset->tag }}
						</div>
					</div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="row">
						<div class="col-md-4"><label>Brand</label></div>
						<div class="col-md-8">{{ $asset->model->brand->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Type</label></div>
						<div class="col-md-8">{{ $asset->model->itemType->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Status</label></div>
						<div class="col-md-8">{{ $asset->item->item_status->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Location Central</label></div>
						<div class="col-md-8">{{ $asset->item->sub_location->name }}, {{ $asset->item->sub_location->central->name }}
						</div>
					</div>
	      		</div>
	    	</div>
    	</div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Inserted By</div>
	  	<div class="panel-body">
      		<div class="col-md-12">
      			<div class="row">
					<div class="col-md-2"><label>Name</label></div>
					<div class="col-md-10">{{ $asset->item->staff->staff_no }} - {{ $asset->item->staff->user->name }}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>Date</label></div>
					<div class="col-md-10">{{ $asset->item->created_at->format('d/m/Y') }}
					</div>
				</div>

    		</div>
    	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<a href="{{ route('adminitems.index') }}"><button id="btnBack" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>
		<br><br>
	</div>
</div>
<br>
@stop