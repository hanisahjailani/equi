<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\ItemType;
use App\ItemStatus;
use App\SubLocation;
use App\Brand;
use App\Item;
use App\Consumable;
use App\Category;
use App\ConsumableRestock;
use Session;

class ConsumableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consumables = Consumable::all();

        return view('pages.superAdmin.consumables',compact('consumables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $itemTypes = ItemType::all();
        $itemStatuses = ItemStatus::all();
        $sublocations = SubLocation::all();
        $brands = Brand::all();
        $itemTypePick = $request->session()->get('itemTypePick');
        $manufacturePick = $request->session()->get('manufacturePick');
        return view('pages.superAdmin.consumablesCreate',compact('itemTypes','itemStatuses','sublocations','brands','itemTypePick','manufacturePick'));
    }

    public function restockcreate(Request $request,$id)
    {
        $consumable = Consumable::findOrFail($id);
        $suppliers = ConsumableRestock::distinct()->get(['supplier_name']);
        return view ('pages.superAdmin.consumablesRestockCreate',compact('consumable','suppliers'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnSave') == 'addItemType')
        {
            $this->validate($request, [
                'nameItemType' => 'required',
            ]);
            $categories = Category::where('name','Consumable')->get();
            foreach($categories as $category)
            {
                $itemType = ItemType::create([
                    'name' => $request->input('nameItemType'),
                    'category_id' => $category->id,
                ]);
            }

            $request->session()->put('itemTypePick',$itemType->id);
            return redirect()->route('consumable.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addManufacture')
        {
            $this->validate($request, [
                'nameModel' => 'required',
            ]);

            $manufacture = Brand::create([
                'name' => $request->input('nameModel'),
            ]);

            $request->session()->put('manufacturePick',$manufacture->id);
            return redirect()->route('consumable.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addConsumable')
        {
            $this->validate($request, [
                'item_type_id' => 'required',
                'brand_id' => 'required',
                'item_status_id' => 'required',
                'name' => 'required',
                'sub_location_id' => 'required',
                'product_no' => 'required|unique:consumables'
            ]);

            $item = Item::create([
                'name' => $request->input('name'),
                'sub_location_id' => $request->input('sub_location_id'),
                'staff_id' => Auth::user()->staff->id,
                'item_status_id' => $request->input('item_status_id'),

            ]);

            $consumable = Consumable::create([
                'brand_id' => $request->input('brand_id'),
                'item_type_id' => $request->input('item_type_id'),
                'item_id' => $item->id,
                'product_no' => $request->input('product_no'),
                'total' => 0,
                'balance' => 0,
            ]);

            $request->session()->forget(['itemTypePick','manufacturePick']);
            return redirect()->route('consumable.index')->with('success','Data inserted!');
        }

    }

    public function restockstore(Request $request,$id)
    {
        $this->validate($request, [
            'supplier_name' => 'required',
            'purchase_cost' => 'required',
            'purchase_date' => 'required',
            'quantity' => 'required',
            'ref_no' => 'required',
        ]);

        $consumable = Consumable::findOrFail($id);
        $restock = ConsumableRestock::create([
            'supplier_name' => $request->input('supplier_name'),
            'purchase_cost' => $request->input('purchase_cost'),
            'purchase_date' => $request->input('purchase_date'),
            'quantity' => $request->input('quantity'),
            'ref_no' => $request->input('ref_no'),
            'consumable_id' => $id,
        ]);

        $consumable->total = $consumable->total+$request->input('quantity');
        $consumable->balance = $consumable->balance+$request->input('quantity');
        $consumable->save();
        // dd($consumable->total+$request->input('quantity'));
        return redirect()->route('consumable.index')->with('success','Item has been restock!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consumable = Consumable::findOrFail($id);
        $stocks = ConsumableRestock::where('consumable_id',$id)->get();
        return view('pages.superAdmin.consumablesShow',compact('consumable','stocks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consumable = Consumable::findOrFail($id);
        $itemTypes = ItemType::all();
        $itemStatuses = ItemStatus::all();
        $sublocations = SubLocation::all();
        $brands = Brand::all();
        return view('pages.superAdmin.consumablesEdit',compact('consumable','itemTypes','brands','itemStatuses','sublocations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'item_type_id' => 'required',
            'brand_id' => 'required',
            'item_status_id' => 'required',
            'name' => 'required',
            'sub_location_id' => 'required',
            'product_no' => 'required|unique:consumables,product_no,'.$id,
        ]);

        $consumable = Consumable::findOrFail($id);
        $consumable->item_type_id = $request->input('item_type_id');
        $consumable->brand_id = $request->input('brand_id');
        $consumable->save();

        $item = Item::find($consumable->item_id);
        $item->name = $request->input('name');
        $item->sub_location_id = $request->input('sub_location_id');
        $item->item_status_id = $request->input('item_status_id');
        $item->save();

        return redirect()->route('consumable.index')->with('success','Data updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $consumable = Consumable::find($id);
        $consumable->delete();

        $item = Item::destroy($consumable->item_id);
        return redirect()->route('consumable.index')->with('success', 'Data Successfully Removed!');
    }
}
