<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ItemType;
use App\Brand;
use App\Category;
use App\ItemModel;
use App\Asset;
use Session;

class ItemModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = ItemModel::all();
        $assets = Asset::all();
        return view('pages.superAdmin.itemModels',compact('models','assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // session()->keep(['brand','itemType']);
        // $request->session()->reflash();
        $itemTypes = ItemType::all();
        $brands = Brand::all();
        $categories = Category::all();
        $brandPick = $request->session()->get('brandPick');
        $itemTypePick = $request->session()->get('itemTypePick');
        return view('pages.superAdmin.itemModelsCreate',compact('itemTypes','brands','categories','brandPick','itemTypePick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnAdd') == 'addBrand')
        {
            $this->validate($request, [
                'nameBrand' => 'required',
            ]);

            $brand = Brand::create([
                'name' => $request->input('nameBrand'),
            ]);
            $request->session()->put('brandPick',$brand->id);
            return redirect()->route('itemmodels.create')->withInput();
        }
        elseif($request->input('btnAdd') == 'addItemType')
        {
            $this->validate($request, [
                'nameItemType' => 'required',
                'category_id' => 'required',
            ]);

            $itemTypes = ItemType::create([
                'name' => $request->input('nameItemType'),
                'category_id' => $request->input('category_id'),
            ]);
            $request->session()->put('itemTypePick',$itemTypes->id);
            return redirect()->route('itemmodels.create')->withInput();
        }
        elseif($request->input('btnAdd') == 'addModel')
        {
            $this->validate($request, [
                'name' => 'required|unique:item_models',
                'brand_id' => 'required',
                'item_type_id' => 'required',
            ]);
            // dd($request->input('name'));
            $model = ItemModel::create([
                'name' => $request->input('name'),
                'model_no' => $request->input('model_no'),
                'brand_id' => $request->input('brand_id'),
                'item_type_id' => $request->input('item_type_id'),
            ]);
            $request->session()->forget(['brandPick', 'itemTypePick']);
            return redirect()->route('itemmodels.index')->with('success','Data has been succesfully inserted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemModel = ItemModel::findOrFail($id);
        $itemTypes = ItemType::all();
        $brands = Brand::all();
        $categories = Category::all();
        return view('pages.superAdmin.itemModelsEdit',compact('itemTypes','brands','categories','itemModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required',
                'brand_id' => 'required',
                'item_type_id' => 'required',
            ]);

        $itemModel = ItemModel::findOrFail($id);
        $itemModel->fill($request->all());
        $itemModel->save();

        return redirect()->route('itemmodels.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
