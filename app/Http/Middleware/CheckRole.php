<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $request->user();
        // dd($user->staff->role);
        // dd($user->hasRole($role));
        if($user->hasRole($role))
        {
            return redirect('login');
        }

        return $next($request);
        
    }
}
