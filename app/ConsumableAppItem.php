<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumableAppItem extends Model
{
    protected $guarded = [];

    public function consumable_application()
    {
        return $this->belongsTo('App\ConsumableApplication');
    }

    public function consumable()
    {
        return $this->belongsTo('App\Consumable');
    }
}
