<?php

namespace App\Http\Controllers;

use App\Consumable;
use App\ConsumableAppItem;
use App\ConsumableApplication;
use App\ItemType;
use Illuminate\Http\Request;

class ApplicationConsumableController extends Controller
{
    public function apply(Request $request)
    {
        return view('pages/staff/consumables/applicationsCreate');
    }

    public function store(Request $request)
    {
        $request->validate([
            'request_date' => 'required|date',
        ]);

        $application = new ConsumableApplication($request->all());
        $application->application_status_id = 1;
        $application->staff_id = $request->user()->staff->id;
        $application->save();

        return redirect()->route('consumable.items', $application->id);

        // Insert into consumable_applications

    }

    public function items(Request $request, $consumableApplicationId)
    {
        $consumableApplication = ConsumableApplication::find($consumableApplicationId);
        $selectedItemType = $request->input('item_type');
        $itemTypes = ItemType::has('consumables')->get();
        $consumables = Consumable::where('item_type_id', $selectedItemType)->get();

        return view('pages/staff/consumables/applicationItems', [
            'consumableApplication' => $consumableApplication,
            'itemTypes' => $itemTypes,
            'consumables' => $consumables,
        ]);
    }

    public function addItems(Request $request, ConsumableApplication $consumableApplication)
    {
        foreach ($request->input('quantity') as $consumableId => $quantity) {
            if ($quantity > 0) {
                // Check if already exists
                $appItem = $consumableApplication
                    ->consumable_app_items()
                    ->where('consumable_id', $consumableId)
                    ->first();

                if ($appItem) {
                    // Update - increment quantity
                    $appItem->update([
                        'quantity' => $appItem->quantity + $quantity,
                    ]);
                } else {
                    $consumableApplication
                        ->consumable_app_items()
                        ->save(new ConsumableAppItem([
                            'consumable_id' => $consumableId,
                            'quantity' => $quantity,
                        ]));
                }

                // Tolak balance dalam consumables
                $consumable = Consumable::find($consumableId);

                $consumable->update([
                    'balance' => $consumable->balance - $quantity,
                ]);
            }
        }

        return redirect()->route('consumable.items', $consumableApplication);
    }

    public function removeItem(ConsumableAppItem $consumableAppItem)
    {
        $consumable = $consumableAppItem->consumable;
        $consumable->update([
            'balance' => $consumable->balance + $consumableAppItem->quantity,
        ]);

        $consumableAppItem->delete();
        
        return back();
    }
}
