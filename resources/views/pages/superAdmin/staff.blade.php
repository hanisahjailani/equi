@extends('layouts.superAdmin.staff')

@section('content1')
	<center><h2>Staff</h2></center>
	&nbsp;Setting / Staff /
	<hr>
	@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@elseif($message = Session::get('failure'))
	<div class="alert alert-danger">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div align="right">
		<a href="{{ route('staff.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Staff</button></a>
	</div>
	<br>
	<div class="table-responsive">
	<table id="tblStaff" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="45%">Staff</th>
				<th width="15%">Department</th>
				<th width="20%">Role</th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($staff as $staff)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td><a href="{{ route('staff.show',$staff->id) }}">{{ $staff->staff_no }} - {{ $staff->user->name }}</a></td>
				<td>{{ $staff->department->short_name }} ( {{ $staff->department->central->short_name }} )</td>
				<td>
					@if($staff->role_id != "")
						@if($staff->role->name == "Super Admin")
							{{ $staff->role->name }}
						@else
							@foreach($staff->adminCentral as $admin)
								<p>{{ $staff->role->name }} ({{ $admin->central->short_name }})</p>
							@endforeach
						@endif
					@elseif($staff->role_id == "")
						Staff
					@endif
				</td>
				<td><a href="{{ route('staff.edit',$staff->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>

	<br>
	<script>
		$(document).ready( function () {
			$('#tblStaff').DataTable();
		} );
	</script>
@stop