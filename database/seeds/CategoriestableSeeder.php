<?php

use Illuminate\Database\Seeder;

class CategoriestableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'id'=>'1',
        	'name'=>'Asset',
        ]);

        DB::table('categories')->insert([
        	'id'=>'2',
        	'name'=>'Consumable',
        ]);

        DB::table('categories')->insert([
        	'id'=>'3',
        	'name'=>'License',
        ]);

    }
}
