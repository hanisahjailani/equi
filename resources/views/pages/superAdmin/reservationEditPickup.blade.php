@extends('layouts.superAdmin.reservation')

@section('content1')
<center><h2>Pick-Up Item</h2></center>
	<a href=""  onClick="window.history.go(-1); return false;">Reservation</a> /  <a href="" onClick="window.history.go(-1); return false;">
	@if($applicationss->application_status->name == "Receive")
		Pickup Item
	@elseif($applicationss->application_status->name == "Overdue")
		Overdue Item
	@endif
	</a>  
	/ Pick-Up Item(s)
	<hr>
	<div class="table-responsive">
	<table id="tblItems" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="15%">Tag</th>
				<th width="40%">Item(s)</th>
				<th width="20%">Serial Number</th>
				<th width="20%">Type</th>
			</tr>
		</thead>
		<tbody>
			@foreach($applicationItems as $applicationItem)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $applicationItem->items->asset->tag }}</td>
				<td>{{ $applicationItem->items->asset->model->name }} - ({{ $applicationItem->items->asset->model->brand->name }})</td>
				<td>{{ $applicationItem->items->asset->serial_number }}</td>
				<td>{{ $applicationItem->items->asset->model->itemType->name }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
	<hr>
	<form method="POST" action="{{ route('reservation.update',$applicationss->id) }}">
		@method('PATCH')
		@csrf
		<input type="hidden" name="staff_id" value="{{ Auth::user()->staff->id }}">
		<div class="form-group">
			<div class="col-md-4"><label>Staff Pickup <label style="color: red">*</label></label></div>
			<div class="col-md-8">
				<select id="staff_id" name="staff_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Staff</option>
					@foreach($staff as $staff)
						<option value="{{ $staff->id }}">{{ $staff->staff_no }} - {{ $staff->user->name }}</option>
					@endforeach
				</select>
				<p style="color: red">{{ $errors->first('staff_id') }}</p>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="col-md-4"><label>Remark for internal </label></div>
			<div class="col-md-8"><textarea name="remark" id="remark" class="form-control" placeholder="Not required" value="{{ old('remark') }}"></textarea></div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-4"><label>Remark for Applicant </label></div>
			<div class="col-md-8"><textarea name="remark_to_applicant" id="remark_to_application" class="form-control" placeholder="Not required" value="{{ old('remark_to_applicant') }}"></textarea></div>
			<br><br>
		</div>
		<div class="form-group">
			<button id="btnBack" type="button" name="btnBack" class="btn btn-danger" onClick="window.history.go(-1); return false;"><i class="fas fa-chevron-left"></i>&nbsp;Back</button>&nbsp; 
			<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-calendar-check"></i>&nbsp;Confirm</button>
			<br><br>
		</div>
	</form>
<br><br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable();
	} );
</script>
<br>
@stop