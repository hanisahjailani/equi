<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Staff;
use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\ApplicationLog;
use App\ItemType;
use App\SubLocation;
use App\Configuration;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::where('application_status_id',1)->get();
        $configs = Configuration::all();
        return view('pages.superAdmin.applications',compact('applications','configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $collectDate = $request->query('collection_date');
        //dd($collectDate);
        $returnDate = $request->query('return_date');
        $itemtype = ItemType::where('category_id',1)->get();
        $centrals = SubLocation::all();
        $applications = Application::all();
        return view('pages.superAdmin.applicationsCreate',compact('itemtype','collectDate','returnDate','centrals','applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::findOrFail($id);
        $configs = Configuration::all();
        return view('pages.superAdmin.applicationsShow',compact('application','configs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $applicant = Staff::all();
        $application = Application::findOrFail($id);
        return view('pages.superAdmin.applicationsEdit',compact('applicant','application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'staff_id' => 'required',
        ]);

        $application = Application::findOrFail($id);
        $application->staff_id = $request->input('staff_id');
        $application->save();

        return redirect()->route('applicationitem.create',$application->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
