<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->staff->role_id != null)
        {
            if($request->user() && $request->user()->staff->role->name != "Super Admin")
            {
                return redirect('login');
            }
        }
        else
        {
            return redirect('login');
        }

        return $next($request);
    }
}
