<?php

use Illuminate\Database\Seeder;

class AccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate
        // Schema::disableForeignKeyConstraints();
        // DB::table('accesses')->truncate();

        DB::table('accesses')->insert([
        	'menu'=>'Reservation',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Application',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Asset',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Consumable',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'License',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Report',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Status Label',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Item Type',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Brand',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Item Model',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Branch',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Department',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Staff',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'Configuration',
        ]);

        DB::table('accesses')->insert([
        	'menu'=>'User Info',
        ]);

        // Schema::enableForeignKeyConstraints();
    }
}
