<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function role_access()
    {
        return $this->hasMany('App\RoleAccess');
    }
    public function staff()
    {
        return $this->hasMany('App\Staff');
    }
}
