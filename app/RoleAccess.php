<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    protected $guarded = [];

    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }

    public function access()
    {
        return $this->belongsTo('App\Access','access_id');
    }
}
