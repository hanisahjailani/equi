<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumableRestock extends Model
{
     protected $guarded = [];

    public function consumable()
    {
    	return $this->belongsTo('App\Consumable','consumable_id');
    }
}
