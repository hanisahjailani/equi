<?php

use Illuminate\Database\Seeder;

class SubLocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_locations')->insert([
            'name'=>'UTMSPACE Taman Universiti',
            'address'=>'No. 34-50, Jalan Kebudayaan 1, Taman Universiti, 81300 Skudai, Johor',
            'central_id'=>'1',
        ]);

        DB::table('sub_locations')->insert([
            'name'=>'UTMSPACE T05',
            'address'=>'Blok T05, UTM',
            'central_id'=>'1',
        ]);
    }
}
