<?php

use Illuminate\Database\Seeder;

class RoleAccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'1',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'2',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'3',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'4',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'5',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'6',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'7',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'8',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'9',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'10',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'11',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'12',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'13',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'14',
        ]);

        DB::table('role_accesses')->insert([
            'role_id'=>'1',
            'access_id'=>'15',
        ]);
    }
}
