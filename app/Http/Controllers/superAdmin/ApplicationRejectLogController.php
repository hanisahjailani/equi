<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\ApplicationLog;
use App\Item;

class ApplicationRejectLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $application = Application::findOrFail($id);
        $applicationitemtypes = ApplicationItemType::where('application_id',$id)->get();
        $applicationItems = ApplicationItem::where('application_id',$application->id)->get();
        // dd($applicationItems);
        return view('pages.superAdmin.applicationsCreateRejectLog',compact('application','applicationItems','applicationitemtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $this->validate($request, [
            'remark_to_applicant' => 'required',
        ]);

        //update application_status_id(3) = rejected
        $application = Application::findOrFail($id);
        $application->application_status_id = 3;
        $application->save();

        $applicationlog = ApplicationLog::create([
            'remark_to_applicant' => $request->input('remark_to_applicant'),
            'application_id' => $application->id,
            'staff_id' => Auth::user()->staff->id,
            'application_status_id' => 3,
        ]);
        
        $applicationItems = ApplicationItem::where('application_id',$id)->get();
        foreach($applicationItems as $applicationItem)
        {
            //update item_status_id(1) = available
            $item = Item::findOrFail($applicationItem->items->id);
            $item->item_status_id = 1;
            $item->save();

            $applicationItem->item_status_id = 1;
            $applicationItem->save();
        }

        return redirect()->route('applicationhistory.index')->with('success', 'Successfully submit the application!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
