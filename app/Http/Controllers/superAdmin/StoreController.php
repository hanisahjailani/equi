<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use DB;
use App\Staff;
use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\ApplicationLog;
use App\ItemType;
use App\Item;
use App\SubLocation;
use App\Configuration;
use App\StoreApplication;
use App\StoreApplicationConsumable;
use App\Consumable;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $store_consumables = StoreApplicationConsumable::all();
        $consumables = Consumable::all();

        return view('pages.superAdmin.store',compact('listStores','consumables'));

        // $senaraiStores = DB::table('store_application_consumables')
        // ->join('store_applications', 'store_applications.id', '=', 'store_application_consumables.store_application_id')
        // ->join('consumables', 'consumables.id', '=', 'store_application_consumables.consumable_id')
        // ->select([
        //     'store_application_consumables.id',
        //     'store_application_consumables.store_application_id',
        //     'store_application_consumables.consumable_id',
        //     'store_application_consumables.quantity',
        //     // 'store_applications.staff_id as staff_id',
        //     // 'consumables.id as consumable_id'
        // ])
        // ->paginate(2);

        //  //dd($senaraiStores);

        // return view('pages.superAdmin.store', compact('senaraiStores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $senaraiItem = ItemType::with(['consumables', 'consumables.item'])->has('consumables')->get();

        $senaraiItemType = ItemType::select([
            'id',
            'name'
        ])
        ->get();

        return view('pages/superAdmin/storeCreate', 
        compact(
            'senaraiItemType',
            'senaraiItem'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quantity' => 'required|integer',
            'consumable_id' => 'required|integer',
        ]);

        //insert dalam table store_applications
        $storeApplication = StoreApplication::create([
            'staff_id' => Auth::user()->staff->id,
        ]);

        //insert dalam table store_application_consumables
        $storeApplication->store_application_consumables()->create($request->all());

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return view('pages.superAdmin.storeShow'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
