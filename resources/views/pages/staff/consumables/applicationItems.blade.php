@extends('layouts.user.user')

@section('content1')
<h5 class="font-weight-bold">
	Consumable Application
</h5>

<span>{{ $consumableApplication->request_date }}</span>

<table class="table">
	<thead>
		<tr>
			<th>Item</th>
			<th>Quantity</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach ($consumableApplication->consumable_app_items as $appItem)
			<tr>
				<td>{{ $appItem->consumable->item->name }}</td>
				<td>
					{{ $appItem->quantity }}
				</td>
				<td>
					<form class="delete-form" action="{{ route('consumable.items.remove-item', $appItem) }}" method="POST">
						@csrf

						<button class="btn btn-danger">Remove</button>
					</form>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<button class="btn btn-primary">Save</button>

<div class="form-group">
	<label for="itemType">Item Types</label>
	<select class="form-control" id="itemType">
		<option value="">PLEASE SELECT AN ITEM TYPE</option>
		@foreach ($itemTypes as $itemType)
			<option value="{{ $itemType->id }}"{{ request('item_type') == $itemType->id ? ' selected' : '' }}>
				{{ $itemType->name }}
			</option>
		@endforeach
	</select>
</div>

<form action="{{ route('consumable.items.add-items', $consumableApplication) }}" method="POST">
	@csrf

	<div id="quantity-alert" class="alert alert-danger">
		Quantity has exceeded what is in stock.
	</div>

	<table class="table">
		<thead>
			<tr>
				<th>Item</th>
				<th>In Stock</th>
				<th>Quantity</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($consumables as $consumable)
				<tr>
					<td>{{ $consumable->item->name }}</td>
					<td>{{ $consumable->balance }}</td>
					<td>
						<input name="quantity[{{ $consumable->id }}]" id="quantity" data-stock="{{ $consumable->balance }}" class="form-control quantity-text" />
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	
	<button class="btn btn-secondary">Add</button>
</form>

<script>
	$(function () {
		$('#itemType').on('change', function () {
			var selected = $(this).val();
			window.location.href = window.location.pathname + "?item_type=" + selected;
			// window.href = "{{ url()->current() }}?item_type=" + selected;
		});

		$('.delete-form').on('submit', function () {
			return confirm('Are you sure?');
		});

		// Sorok dulu alert
		$('#quantity-alert').hide();

		$('.quantity-text').on('keyup', function () {
			var quantity = $(this).val();
			var stock = $(this).data('stock');

			if (quantity > stock) {
				$('#quantity-alert').show();
				$(this).val('');
			} else {
				$('#quantity-alert').hide();
			}
		});
	})
</script>

@stop