@extends('layouts.superAdmin.roleAccess')

@section('content1')
<center><h2>Edit Role Accessibility</h2></center>
&nbsp;<a href="{{ route('role.index') }}">Role Access</a> / Edit Role Accessibility
<hr>
<div class="row">
    <form id="frmEditRole" method="POST" action="{{ route('role.update',$role->id) }}">
        @csrf
        @method('PATCH')
    <div class="col-md-5">
         {{-- role --}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                Role
            </h4>
        </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-md-2">
                        Role
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $role->name }}">
                        <p style="color: red">{{ $errors->first('name') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        {{-- access --}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                Accessibility
            </h4>
        </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="tblaccess" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="90%">Menu</th>
                                <th width="10%"><i class="fas fa-eye"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($accesses as $access)
                            <tr>
                                <td>
                                    {{ $access->menu }}
                                </td>
                                <td>
                                    <input type="checkbox" id="access[]" name="access[]" value="{{ $access->id }}"
                                    @foreach($role->role_access as $role_access)
                                        @if($access->id == $role_access->access_id)
                                        checked>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div align="right">
            <a href="{{ route('role.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp;
            <button class="btn btn-success" name="btnSave" id="btnSave" type="submit"><i class="fas fa-save"></i> Save</button>
        </div>
        <br><br>
    </div>
</form>
</div>
@stop