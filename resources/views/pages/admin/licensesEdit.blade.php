@extends('layouts.admin.license')

@section('content1')
<script>
  $(document).ready(function() {
    $( "#purchase_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
    $( "#expired_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
    $( "#termination_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
  });
</script>
<center><h2>Edit License</h2></center><br>
&nbsp;<a href="{{ route('adminlicense.index') }}">Licenses</a> / Edit License /
<hr>
	<form id="frmAdd" action="{{ route('adminlicense.update',$license->id) }}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<div class=" col-md-3"><label>Software Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name',$license->item->name) }}">
			<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Item Type <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Item Type</option>
					@foreach($itemTypes->where('category_id',3) as $itemType)
						<option value="{{ $itemType->id }}"
							@if($itemType->id == $license->item_type_id)
								selected="selected"
							@endif
							>{{ $itemType->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_type_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Product Key <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="product_key" name="product_key" type="text" class="form-control" value="{{ old('product_key',$license->product_key) }}" >
			<p style="color: red">{{ $errors->first('product_key') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Manufacture<label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Manufacture</option>
					@foreach($brands as $brand)
						<option value="{{ $brand->id }}"
							@if($brand->id == $license->brand_id)
								selected="selected"
							@endif
							>{{ $brand->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('brand_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-3"><label>Supplier Name </label></div>
			<div class="col-md-9">
				<input type="text" name="supplier_name" id="supplier_name" class="form-control" list="supplier" value="{{ old('supplier_name',$license->supplier_name) }}" autocomplete="off">
				<datalist id="supplier">
					@foreach($licenses as $license)
					<option value="{{ $license->supplier_name }}"></option>
					@endforeach
				</datalist>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>License Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="license_name" name="license_name" type="text" class="form-control" value="{{ old('license_name',$license->license_name) }}" >
			<p style="color: red">{{ $errors->first('license_name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>License Email <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="license_email" name="license_email" type="text" class="form-control" value="{{ old('license_email',$license->license_email) }}" >
			<p style="color: red">{{ $errors->first('license_email') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Status <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_status_id" name="item_status_id" class="form-control">
					@foreach($itemStatuses as $itemStatus)
						<option value="{{ $itemStatus->id }}"
							@if($itemStatus->id == $license->item->item_status_id)
								selected="selected"
							@endif
							>{{ $itemStatus->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_status_id') }}</p>
			<!-- <input type="checkbox" name="requestable" value="Yes"> <strong>Requestable</strong> -->
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Location Central<label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Central</option>
					@foreach($sublocations as $sublocation)
						<option value="{{ $sublocation->id }}"
							@if($sublocation->id == $license->item->sub_location_id)
								selected="selected"
							@endif
							>{{ $sublocation->name }}, {{ $sublocation->central->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('sub_location_id') }}</p>

			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Date </label></div>
			<div class="col-md-3"><input id="purchase_date" name="purchase_date" type="text" class="form-control" value="{{ old('purchase_date',$license->purchase_date) }}" autocomplete="off">
			</div>
			<div class=" col-md-3" align="right"><label>Expired Date </label></div>
			<div class="col-md-3"><input id="expired_date" name="expired_date" type="text" class="form-control" value="{{ old('expired_date',$license->expired_date) }}" autocomplete="off">
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Termination Date </label></div>
			<div class="col-md-3"><input id="termination_date" name="termination_date" type="text" class="form-control" value="{{ old('termination_date',$license->termination_date) }}" autocomplete="off">
			</div>
			<div class="col-md-3" align="right"><label>Seat </label></div>
			<div class="col-md-3">
				<input type="number" name="seat" id="seat" class="form-control" min="1" value="{{ old('seat',$license->seat) }}">
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Cost </label></div>
			<div class="col-md-3">
				<div class="input-group">
				    <span class="input-group-addon">RM</span>
				    <input id="purchase_cost" type="text" class="form-control" name="purchase_cost" value="{{ old('purchase_cost',$license->purchase_cost) }}">
				 </div>
			</div>
			<div class=" col-md-3" align="right"><label>Purchase Order No </label></div>
			<div class="col-md-3"><input id="purchase_order_num" name="purchase_order_num" type="text" class="form-control" value="{{ old('purchase_order_num',$license->purchase_order_num) }}">
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"></div>
			<div class="col-md-9" style="float:right;">
				<a href="{{ route('adminlicense.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnSave" name="btnSave" value="addLicense" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop