@extends('layouts.admin.itemModel')

@section('content1')
<center><h2>Add Item Models</h2></center><br>
&nbsp;<a href="{{ route('adminitemmodels.index') }}">Setting</a> / <a href="{{ route('adminitemmodels.index') }}">Items Models</a> / Add Item Models /
<hr>
	<form id="frmAdd" action="{{ route('adminitemmodels.store') }}" method="POST">
		@csrf
		<input id="admin_id" name="admin_id" type="hidden" class="form-control" value="{{ Auth::user()->staff->id }}">
		<div class="form-group">
			<div class=" col-md-2"><label>Model Name<label style="color: red">*</label></label></div>
			<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" 
				
				value="{{ old('name') }}" placeholder="Eg : Aspire E 14">
			<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Brand <label style="color: red">*</label></label></div>
			<div class="col-md-6">
				<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Brand</option>
					@foreach($brands as $brand)
						<option value="{{ $brand->id }}"

								@if($brand->id  == $brandPick)
									selected="selected"
								@endif
							>{{ $brand->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('brand_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#modalBrand"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Brand</a>
				<p style="font-size: small; color: red">**If the brand is not in the list**</p>
			</div>
			<div class="modal fade" id="modalBrand" role="dialog">
			    <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">New Brand</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="col-md-2" align="right"><label>Name <label style="color: red">*</label></label></div>
								<div class="col-md-10"><input id="nameBrand" name="nameBrand" type="text" class="form-control" value="{{ old('nameBrand') }}">
								<p style="color: red">{{ $errors->first('nameBrand') }}</p></div>
							</div>
							<br><br>
						</div>
						<div class="modal-footer">
						  <button id="btnAdd" name="btnAdd" type="submit" value="addBrand" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Item Type <label style="color: red">*</label></label></div>
			<div class="col-md-6">
				<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Item type</option>
					@foreach($itemTypes as $itemTypes)
						<option value="{{ $itemTypes->id }}" 
							@if($itemTypes->id  == $itemTypePick)
								selected="selected"
							@endif
							>{{ $itemTypes->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_type_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Item Type</a>
				<p style="font-size: small; color: red">**If the item type is not in the list**</p>
			</div>
			<div class="modal fade" id="myModal" role="dialog">
			    <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">New Item Type</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
								<div class="col-md-9"><input id="nameItemType" name="nameItemType" type="text" class="form-control" value="{{ old('nameItemType') }}">
								<p style="color: red">{{ $errors->first('nameItemType') }}</p></div>
							</div>
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Category <label style="color: red">*</label></label></div>
								<div class="col-md-9">
									<select id="category_id" name="category_id" class="form-control selectpicker" data-live-search="true">
											<option value="">Choose Category</option>
										@foreach($categories as $category)
											<option value="{{ $category->id }}">{{ $category->name }}</option>
										@endforeach
									</select>
								<p style="color: red">{{ $errors->first('category_id') }}</p></div>
							</div>
							<br><br><br><br>
						</div>
						<div class="modal-footer">
						  <button id="btnAdd" name="btnAdd" type="submit" value="addItemType" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Model No.</label></div>
			<div class="col-md-10"><input id="model_no" name="model_no" type="text" class="form-control" value="{{ old('model_no') }}">
			<p style="color: red">{{ $errors->first('model_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<a href="{{ route('adminitemmodels.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnAdd" name="btnAdd" type="submit" value="addModel" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop