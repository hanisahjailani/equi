@extends('layouts.superAdmin.branch')

@section('content1')
	<center><h2>Branch</h2></center>
	&nbsp;Setting / Branch /
	<hr>
	@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@elseif($message = Session::get('failure'))
	<div class="alert alert-danger">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="panel-group" id="accordion">
	  <div class="panel panel-info">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong> <i class="fas fa-code-branch"></i> Branch </strong></a>
	      </h4>
	    </div>
	    <div id="collapse1" class="panel-collapse collapse in">
	      <div class="panel-body">
	      	<div align="right">		
	      		<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="far fa-plus-square"></i>&nbsp;New Branch</button>
	      	</div>
	      	<div class="modal fade" id="myModal" role="dialog">
	      	    <div class="modal-dialog">
	      			<div class="modal-content">
	      				<form id="frmItemType" method="POST" action="{{ route('branches.store') }}">
	      					@csrf
	      					<div class="modal-header">
	      					  <button type="button" class="close" data-dismiss="modal">&times;</button>
	      					  <h4 class="modal-title">New Branch</h4>
	      					</div>
	      					<div class="modal-body">
	      						<div class="form-group">
	      							<div class="col-md-3" align="right"><label>Short Name <label style="color: red">*</label></label></div>
	      							<div class="col-md-9"><input id="short_name" name="short_name" type="text" class="form-control" value="{{ old('short_name') }}">
	      							<p style="color: red">{{ $errors->first('name') }}</p></div>
	      						</div>
	      						<div class="form-group">
	      							<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
	      							<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
	      							<p style="color: red">{{ $errors->first('name') }}</p></div>
	      						</div>
	      						<br><br><br><br>
	      					</div>
	      					<div class="modal-footer">
	      					  <button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
	      					</div>
	      				</form>
	      			</div>
	      		</div>
	      	</div>
	      	<br>
	      	<div class="table-responsive">
	      	<table id="tblBranch" align="center" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th width="95%">Name</th>
						<th width="5%"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($branches as $branch)
					<tr>
						<td>{{ $branch->name }} ( {{ $branch->short_name }} )</td>
						<td>
							<form id="frmDelete" action="" method="POST">
								@csrf
								@method('DELETE')	
								<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this branch?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table></div>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-info">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><strong> <i class="fas fa-code-branch"></i> Sub Location </strong></a>
	      </h4>
	    </div>
	    <div id="collapse2" class="panel-collapse collapse">
	      <div class="panel-body">
	      	<div align="right">		
	      		<button class="btn btn-primary" data-toggle="modal" data-target="#modalSubLocation"><i class="far fa-plus-square"></i>&nbsp;New Sub Location</button>
	      	</div>
	      	<div class="modal fade" id="modalSubLocation" role="dialog">
	      	    <div class="modal-dialog">
	      			<div class="modal-content">
	      				<form id="frmItemType" method="POST" action="{{ route('subLocation.store') }}">
	      					@csrf
	      					<div class="modal-header">
	      					  <button type="button" class="close" data-dismiss="modal">&times;</button>
	      					  <h4 class="modal-title">New Branch</h4>
	      					</div>
	      					<div class="modal-body">
	      						<div class="form-group">
	      							<div class="col-md-3" align="right"><label>Branch <label style="color: red">*</label></label></div>
	      							<div class="col-md-9">
	      								<select id="central_id" name="central_id" class="form-control selectpicker" data-live-search="true">
	      										<option value="">Choose Branch</option>
	      									@foreach($branches as $branch)
	      										<option value="{{ $branch->id }}">{{ $branch->name }} ( {{ $branch->short_name }} )</option>
	      									@endforeach
	      								</select>
	      							<p style="color: red">{{ $errors->first('central_id') }}</p></div>
	      						</div>
	      						<div class="form-group">
	      							<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
	      							<div class="col-md-9"><input id="nameSub" name="nameSub" type="text" class="form-control" value="{{ old('nameSub') }}">
	      							<p style="color: red">{{ $errors->first('nameSub') }}</p></div>
	      						</div>
	      						<div class="form-group">
	      							<div class="col-md-3" align="right"><label>Address </label></div>
	      							<div class="col-md-9"><input id="address" name="address" type="text" class="form-control" value="{{ old('address') }}" placeholder="Not required">
	      							<p style="color: red">{{ $errors->first('address') }}</p></div>
	      						</div>
	      						<br><br><br><br><br><br>
	      					</div>
	      					<div class="modal-footer">
	      					  <button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
	      					</div>
	      				</form>
	      			</div>
	      		</div>
	      	</div>
	      	<br>
	      	<table id="tblSub" align="center" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th width="30%">Name</th>
						<th width="35%">Address</th>
						<th width="30%">Central</th>
						<th width="5%"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($subLocations as $subLocation)
					<tr>
						<td>{{ $subLocation->name }}</td>
						<td>{{ $subLocation->address }}</td>
						<td>{{ $subLocation->central->name }}</td>
						<td>
							<form id="frmDelete" action="" method="POST">
								@csrf
								@method('DELETE')	
								<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this sub location?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
	      </div>
	    </div>
	  </div>
	</div>
	<br>
	<script>
		$(document).ready( function () {
			$('#tblBranch').DataTable();
		} );
		$(document).ready( function () {
			$('#tblSub').DataTable();
		} );
	</script>
@stop