<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleIdToRoleAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_accesses', function (Blueprint $table) {
            $table->integer('access_id')->unsigned()->nullable();
            $table->foreign('access_id')->references('id')->on('accesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_accesses', function (Blueprint $table) {
            $table->dropColumn('access_id');
        });
    }
}
