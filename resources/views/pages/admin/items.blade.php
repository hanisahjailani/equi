@extends('layouts.admin.item')

@section('content1')
<center><h2>List of Assets</h2></center>
&nbsp;Assets /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="left" class="col-md-9">
	<i class="fas fa-check" style="color:green"></i> = Requestable &nbsp;&nbsp;&nbsp;
	<i class="fas fa-times" style="color:red"></i> = Not Requestable
</div>
<div align="right" class="col-md-3">
	<a href="{{ route('adminitems.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Asset</button></a>
</div>
<br><br><br>
<div class="table-responsive" style="overflow-x:auto;">
	<table id="tblItems" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="15%">Tag</th>
				<th width="20%">Model</th>
				<th width="15%">Serial Number</th>
				<th width="10%">Item Type</th>
				<th width="15%">Location</th>
				<th width="15%">Status</th>
				<th width="5%"></th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($assets as $asset)
			<tr>
				<td><a href="{{ route('adminitems.show',$asset->id) }}">{{ $asset->tag }}</a></td>
				<td>{{ $asset->model->name }} ( {{ $asset->model->brand->name }} )</td>
				<td>{{ $asset->serial_number }}</td>
				<td>{{ $asset->model->itemType->name }}</td>
				<td>{{ $asset->item->sub_location->name }}, {{ $asset->item->sub_location->central->name }}</td>
				<td style="background-color: #{{ $asset->item->item_status->color }}">
					<strong>{{ $asset->item->item_status->name }}</strong>
				</td>
				<td>
					@if($asset->requestable == "Yes")
					<i class="fas fa-check" style="color:green"></i>
					@elseif($asset->requestable == "No")
					<i class="fas fa-times" style="color:red"></i>
					@endif

				</td>
				<td>
					<a href="{{ route('adminitems.edit',$asset->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a>
				</td>
				<td>
					<form id="frmDelete" action="{{ route('adminitems.destroy', $asset->id) }}" method="POST">
						@csrf
						@method('DELETE')	
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable({
			"order": [[ 0, "desc" ]],
			"scrollX": true
		});
	} );
</script>
@stop