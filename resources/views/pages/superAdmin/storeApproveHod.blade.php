@extends('layouts.superAdmin.application')

@section('content1')
halaman index, papar senarai yang perlu di approve hod

<center><h2>List of HOD Approval</h2></center>
&nbsp;Stor /
<hr>

<div align="right">
    <a href="{{ route('store.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;Add New</button></a>
</div>

<br>
<div class="table-responsive">
<table id="tblStor" align="center" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="25%">Name</th>
            <th width="20%">Apply Date</th>
            <th width="5%">Status</th>
        </tr>
    </thead>
<tbody>
    @foreach ($senaraiStoreApprove as $item)

        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ Auth::user()->name }}</td>
            <td>{{ item->store_application->staff->name }}</td>
            <td></td>
            <td></td>
            <td>
            <a href="/store/{{ $item->id }}" class="btn btn-primary btn-sm">VIEW</a>
            <a href="/store/{{ $item->id }}/edit" class="btn btn-warning btn-sm">EDIT</a>
            
<!-- Button trigger modal -->
<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $item->id }}">
DELETE
</button>

<!-- Modal -->
<div class="modal fade" id="modal-delete-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <form method="POST" action="/store/{{ $item->id }}">
    @csrf
    @method('DELETE')
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Adakah anda bersetuju untuk delete rekod {{ $item->id }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Confirm</button>
      </div>
    </div>
    </form>


  </div>
</div>

            </td>
        </tr>

    @endforeach
    </tbody>
</table>

@stop