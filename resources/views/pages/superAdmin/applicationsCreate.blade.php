@extends('layouts.superAdmin.application')

@section('content1')
<script>
  $(document).ready(function() {
    $( "#collection_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      minDate:0,
      dateFormat: "yy-mm-dd",
      onSelect: function( selectedDate ) {
        $( "#return_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#return_date" ).datepicker({      
      changeMonth: true,   
      changeYear:true,
      minDate:0,
      dateFormat: "yy-mm-dd",
      onSelect: function( selectedDate ) {
        $( "#collection_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
</script>
<center><h2>NEW Application</h2></center>
&nbsp;<a href="{{ route('application.index') }}">Application</a> / Create New Application /
<hr>
<div class="alert alert-info">
	<center><i class="fas fa-sticky-note"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="fas fa-user" style="color:grey;"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="fas fa-warehouse" style="color:grey;"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="far fa-check-circle" style="color:grey;"></i></center> 
</div>
	<form id="frmAdd" action="{{ route('applicationitemtype.create')}}" method="GET">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-5"><label>Collection Date <label style="color: red">*</label></label></div>
							<div class="col-md-7"><input name="collection_date" id="collection_date" class="form-control" value="{{ old('collection_date',$collectDate) }}" />
								<p style="color: red">{{ $errors->first('collection_date') }}</p>
							</div>
							<br><br>
						</div>
						<div class="form-group">
							<div class="col-md-5"><label>Return Date <label style="color: red">*</label></label></div>
							<div class="col-md-7"><input name="return_date" id="return_date" class="form-control" value="{{ old('return_date',$returnDate) }}"/>
								<p style="color: red">{{ $errors->first('return_date') }}</p>
							</div>
							<br><br>
						</div>						
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-4"><label>Central Location <label style="color: red">*</label></label></div>
							<div class="col-md-8">
								<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true">
										<option value="">Choose Central</option>
									@foreach($centrals as $central)
										<option value="{{ $central->id }}">{{ $central->name }}, {{ $central->central->name }}</option>
									@endforeach
								</select>
								<p style="color: red">{{ $errors->first('sub_location_id') }}</p>
							</div>
							<br><br>
						</div>
					</div>
					<div class="col-md-12">
						<div class="panel-group">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-plus-square"></i> Other Infomation</strong></a>
						      </h4>
						    </div>
						    <div id="collapse1" class="panel-collapse collapse">
						      <div class="panel-body">
						      	<div class="form-group">
						      		<div class="col-md-4"><label>Location Use </label>(Optional)</div>
						      		<div class="col-md-8"><input name="location_use" type="text" list="location" class="form-control" value="{{ old('location_use') }}" />
						      			<datalist id="location">
						      				@foreach($applications as $application)
						      				<option value="{{ $application->location_use }}"></option>
						      				@endforeach
						      			</datalist>
						      			<p style="color: red">{{ $errors->first('location_use') }}</p>
						      		</div>
						      		<br><br>
						      	</div>
						      	<div class="form-group">
						      		<div class="col-md-4"><label>Purpose </label>(Optional)</div>
						      		<div class="col-md-8"><input name="purpose" type="text" class="form-control" value="{{ old('purpose') }}" />
						      			<p style="color: red">{{ $errors->first('purpose') }}</p>
						      		</div>
						      		<br><br>
						      	</div>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				</div>	
				<hr>
				<div class="row">
					<div class="col-md-12">
						<p style="color: red">{{ $errors->first('item_type_id') }}</p>
					<div class="table-responsive">
					<table id="tblItemsType" style="text-align: center;" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="70%">Items</th>
								<th width="20%">Quantity</th>
								<th width="5%">Choose</th>
							</tr>
						</thead>
						<tbody>
							@foreach($itemtype as $itemtype)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td style="text-align: left;">{{ $itemtype->name }} </td>
								<td><input type="number" name="quantity_{{ $itemtype->id }}" id="quantity_{{ $itemtype->id }}" min="1" value="1" class="form-control"></td>
								<td><input type="checkbox" name="item_type_id[]" value="{{ $itemtype->id }}"></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
					</div>
				</div>
				<br>					
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div class="col-md-12" style="float:right;">
						<a href="{{ route('application.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>
						<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-chevron-right"></i>&nbsp;Next</button>&nbsp; 
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script>
	$(document).ready( function () {
		$('#tblItemsType').DataTable();
	} );
</script>
<br>
@stop