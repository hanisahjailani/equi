<?php

use Illuminate\Database\Seeder;
use App\RoleAccess;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            AccessesTableSeeder::class,
            RoleAccessesTableSeeder::class,
            CentralsTableSeeder::class,
        	UsersTableSeeder::class,
            ApplicationStatusesTableSeeder::class,
            ItemStatusesTableSeeder::class,
            ConfigurationsTableSeeder::class,
            AdminCentralTableSeeder::class,
            CategoriestableSeeder::class,
            BrandsTableSeeder::class,
            ItemTypesTableSeeder::class,
            SubLocationTableSeeder::class,
            AssetTableSeeder::class,
        ]);
    }
}
