<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>EquI</title>

    @include('includes.head')
</head>
<body style="background-color: #dfe6e9; font-family: 'Century Gothic'; font-size: medium;">
    <header>
        <nav class="navbar navbar-default col-md-12">
            <div class="navbar-header">
                <img src="{{ URL::to ('/image/logo.png') }}" width="120px" height="40px">
                <a class="navbar-brand" href="" style="font-family: 'Forte' ; font-size:x-large; color: maroon">EquI</a>
            </div>
            <ul class="nav navbar-nav navbar-left" style="padding: 5px; left: 10%">
                
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @guest
                    <li>
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i>&nbsp;&nbsp;{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbardrop" class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">&nbsp;&nbsp;<i class="fas fa-sign-out-alt"></i>
                                {{ __('Logout') }}
                            </a>
                            </li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </li>
                @endguest
            </ul>
        </nav>
    </header>
    <br>
    <div id="main" class="row col-md-12">
        @yield('content')
    </div>
    
</body>
</html>
