@extends('layouts.superAdmin.department')

@section('content1')
<center><h2>Department</h2></center>
&nbsp;Setting / Department /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="right">		
	<a href="" data-toggle="modal" data-target="#modalDept"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Department</button></a>
</div>
<form id="addDept" method="POST" action="{{ route('department.store') }}">
@csrf
<div class="modal fade" id="modalDept" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">New Department</h4>
					</div>
					<div class="modal-body">
					<div class="row">
						<div class="form-group">
							<div class="col-md-3" align="right"><label>Short Name <label style="color: red">*</label></label></div>
							<div class="col-md-9"><input id="short_name" name="short_name" type="text" class="form-control" value="{{ old('short_name') }}">
							<p style="color: red">{{ $errors->first('short_name') }}</p></div>
						</div>
						<div class="form-group">
							<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
							<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
							<p style="color: red">{{ $errors->first('name') }}</p></div>
						</div>
						<div class="form-group">
							<div class="col-md-3" align="right"><label>Central <label style="color: red">*</label></label></div>
							<div class="col-md-9">
								<select id="central_id" name="central_id" class="form-control selectpicker" data-lived-search="true">
									<option value="">Choose Central</option>
									@foreach($centrals as $central)
										<option value="{{ $central->id }}" 
										@if($central->id == old('central_id'))
											selected="selected"
										@endif
											>({{ $central->short_name }}) {{ $central->name }}</option>
									@endforeach
								</select>
							<p style="color: red">{{ $errors->first('central_id') }}</p></div>
						</div>
						<div class="form-group">
							<div class="col-md-3" align="right"><label>PIC</label></div>
							<div class="col-md-9">
							<select id="pic_staff_id" name="pic_staff_id" class="form-control selectpicker" data-live-search="true">
									<option value="">Not Assign</option>
								@foreach($staffs as $staff)
									<option value="{{ $staff->id }}"
										@if($staff->id == old('pic_staff_id'))
											selected="selected"
										@endif
									>{{ $staff->staff_no }} - {{ $staff->user->name }}</option>
								@endforeach
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="btnSave" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</div>
</form>

<br>
<div class="table-responsive">
	<table id="tblStaff" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="15%">Central</th>
				<th width="50%">Name</th>
				<th width="20%">PIC</th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($department as $department)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $department->central->name }}</td>
				<td>{{ $department->short_name }} - {{ $department->name }}</td>
				<td>@if(empty($department->pic_staff_id))
					<p>Not Assign</p>
					@elseif(!empty($department->pic_staff_id))
					<p>{{ $department->pic->user->name }}</p>
					@endif

				</td>
				<td><a href="{{ route('department.edit',$department->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a></td>
				<td>
					<form id="frmDelete" action="{{ route('department.destroy',$department->id) }}" method="POST">
						@csrf
						@method('DELETE')	
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this department?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table></div>
	<br>
	<script>
		$(document).ready( function () {
			$('#tblStaff').DataTable();
		} );
	</script>
	@stop