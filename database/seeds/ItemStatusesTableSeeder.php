<?php

use Illuminate\Database\Seeder;

class ItemStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_statuses')->insert([
        	'name'=>'Available',
        	'description'=>'The item(s) still available to use',
        ]);

        DB::table('item_statuses')->insert([
        	'name'=>'In-Use',
        	'description'=>'The item(s) already been used',
        ]);

        DB::table('item_statuses')->insert([
        	'name'=>'Damaged',
        	'description'=>'The item(s) cannot be use',
        ]);
        
        DB::table('item_statuses')->insert([
        	'name'=>'Lost',
        	'description'=>'The item(s) not available',
        ]);

        DB::table('item_statuses')->insert([
            'name'=>'Reserved',
            'description'=>'The item(s) has been reserved for internal use',
        ]);

        DB::table('item_statuses')->insert([
            'name'=>'Booked',
            'description'=>'The item(s) has been booked by applicant',
        ]);

        DB::table('item_statuses')->insert([
            'name'=>'Discard',
            'description'=>'The item(s) has been eliminated/throwaway',
        ]);
    }
}
