@extends('layouts.superAdmin.application')

@section('content1')
Page untuk lihat senarai item/peralatan yang dimohon

<center><h2>List of Store Application</h2></center>

&nbsp;<a href="{{ route('store.index') }}">Store</a> / List of Store Application /
<hr>
<form method="POST" action="{{ route('store.store') }}">
    @csrf
    
<div class="table-responsive">
<table id="tblStoreShow" align="center" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="20%">Item</th>
      <th width="15%">Quantity</th>
      <th width="20%">Action</th>
    </tr>
  </thead>

</table>
@stop