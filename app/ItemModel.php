<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
    protected $guarded = [];

    public function brand()
    {
    	return $this->belongsTo('App\Brand','brand_id');
    }

    public function itemType()
    {
    	return $this->belongsTo('App\ItemType','item_type_id');
    }
    
}
