<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\User;
use App\Staff;
use App\Department;
use App\AdminCentral;
use App\Central;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
         * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $staff = Staff::all();
        $moderators = AdminCentral::all();
        // $superAdmins = $staff->role->where('name','Super Admin')->get();
        return view('pages.superAdmin.staff',compact('staff','moderators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $department = Department::all();
        $deptPick = $request->session()->get('deptPick');
        $centrals = Central::all();

        return view('pages.superAdmin.staffCreate',compact('department','deptPick','centrals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnSave') == 'addDepartment')
        {
            $this->validate($request, [
                'shortNameDept' => 'required',
                'nameDept' => 'required',
                'centralDept' => 'required',
            ]);
            $dept = Department::create([
                'short_name' => $request->input('shortNameDept'),
                'name' => $request->input('nameDept'),
                'central_id' => $request->input('centralDept'),
            ]);
            $request->session()->put('deptPick',$dept->id);
            return redirect()->route('staff.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addStaff')
        {
            $this->validate($request, [
                'staff_no' => 'required|unique:staff',
                'name' => 'required',
                'email' => 'required|unique:users|email',
                'phone_no' => 'required',
                'department_id' => 'required',
                'no_ic' => 'required',
            ]);

            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('no_ic')),
            ]);

            $staff = Staff::create([
                'staff_no' => $request->input('staff_no'),
                'phone_no' => $request->input('phone_no'),
                'department_id' => $request->input('department_id'),
                'role_id' => null,
                'user_id' => $user->id,

            ]);
            $request->session()->forget(['deptPick']);
            return redirect()->route('staff.index')->with('success', 'Data Inserted!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);
        $department = Department::all();
        return view('pages.superAdmin.staffShow', compact('staff','department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::findOrFail($id);
        $department = Department::all();
        return view('pages.superAdmin.staffEdit', compact('staff','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'staff_no' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone_no' => 'required',
            'department_id' => 'required',
        ]);

        $user = Staff::find($id);
        $user->fill($request->except('name','staff_no','email'));
        $user->save();
        return redirect()->route('staff.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

}
