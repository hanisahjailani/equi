<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('booking_id')->nullable();
            $table->dateTime('request_date');
            $table->date('collection_date');
            $table->date('return_date');
            $table->date('actual_collection_date')->nullable();
            $table->date('actual_return_date')->nullable();
            $table->string('location_use')->nullable();
            $table->string('purpose')->nullable();
            $table->integer('application_status_id')->unsigned();
            $table->foreign('application_status_id')->references('id')->on('application_statuses');
            $table->integer('staff_id')->unsigned()->nullable();
            $table->foreign('staff_id')->references('id')->on('staff');
            $table->integer('sub_location_id')->unsigned()->nullable();
            $table->foreign('sub_location_id')->references('id')->on('sub_locations');
            $table->integer('pick_staff_id')->unsigned()->nullable();
            $table->foreign('pick_staff_id')->references('id')->on('staff');
            $table->integer('return_staff_id')->unsigned()->nullable();
            $table->foreign('return_staff_id')->references('id')->on('staff');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
