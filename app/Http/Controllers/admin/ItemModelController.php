<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\ItemType;
use App\Brand;
use App\Category;
use App\ItemModel;
use App\AdminCentral;
use App\SubLocation;
use DB;
use Session;

class ItemModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = ItemModel::all();
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get(); 
        $assets = DB::table('assets')
                ->join('item_models','assets.model_id','=','item_models.id')
                ->join('items','assets.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        return view('pages.admin.itemModels',compact('models','assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $itemTypes = ItemType::all();
        $brands = Brand::all();
        $categories = Category::all();
        $brandPick = $request->session()->get('brandPick');
        $itemTypePick = $request->session()->get('itemTypePick');
        return view('pages.admin.itemModelsCreate',compact('itemTypes','brands','categories','brandPick','itemTypePick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnAdd') == 'addBrand')
        {
            $this->validate($request, [
                'nameBrand' => 'required',
            ]);

            $brand = Brand::create([
                'name' => $request->input('nameBrand'),
            ]);
            $request->session()->put('brandPick',$brand->id);
            return redirect()->route('adminitemmodels.create')->withInput();
        }
        elseif($request->input('btnAdd') == 'addItemType')
        {
            $this->validate($request, [
                'nameItemType' => 'required',
                'category_id' => 'required',
            ]);

            $itemTypes = ItemType::create([
                'name' => $request->input('nameItemType'),
                'category_id' => $request->input('category_id'),
            ]);
            $request->session()->put('itemTypePick',$itemTypes->id);
            return redirect()->route('adminitemmodels.create')->withInput();
        }
        elseif($request->input('btnAdd') == 'addModel')
        {
            $this->validate($request, [
                'name' => 'required',
                'brand_id' => 'required',
                'item_type_id' => 'required',
            ]);
            // dd($request->input('name'));
            $model = ItemModel::create([
                'name' => $request->input('name'),
                'model_no' => $request->input('model_no'),
                'brand_id' => $request->input('brand_id'),
                'item_type_id' => $request->input('item_type_id'),
            ]);
            $request->session()->forget(['brandPick', 'itemTypePick']);
            return redirect()->route('adminitemmodels.index')->with('success','Data has been succesfully inserted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemModel = ItemModel::findOrFail($id);
        $itemTypes = ItemType::all();
        $brands = Brand::all();
        $categories = Category::all();
        return view('pages.admin.itemModelsEdit',compact('itemTypes','brands','categories','itemModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required',
                'brand_id' => 'required',
                'item_type_id' => 'required',
            ]);

        $itemModel = ItemModel::findOrFail($id);
        $itemModel->fill($request->all());
        $itemModel->save();

        return redirect()->route('adminitemmodels.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
