<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
  	use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];

   	public function item()
	{
		return $this->belongsTo('App\Item','item_id');
	}

	public function model()
	{
		return $this->belongsTo('App\ItemModel','model_id');
	}

}
