<?php

use Illuminate\Database\Seeder;

class AdminCentralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_centrals')->insert([
        	'central_id'=> 1,
        	'staff_id'=> 2,
        ]);

        DB::table('admin_centrals')->insert([
            'central_id'=> 2,
            'staff_id'=> 2,
        ]);

        DB::table('admin_centrals')->insert([
        	'central_id'=> 2,
        	'staff_id'=> 5,
        ]);
    }
}
