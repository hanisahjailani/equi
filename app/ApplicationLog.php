<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationLog extends Model
{
    protected $guarded = [];

    public function application()
    {
    	return $this->belongsTo('App\Application','application_id');
    }
    public function application_status()
    {
    	return $this->belongsTo('App\ApplicationStatus','application_status_id');
    }
    public function staff()
    {
    	return $this->belongsTo('App\Staff','staff_id');
    }
}
