@extends('layouts.superAdmin.user')

@section('content1')
<center><h2>User Info</h2></center>
&nbsp;Setting / User Info /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<form id="frmEdit" method="POST" action="{{ route('user.update', Auth::user()->staff->id) }}">
	@csrf
	@method('PATCH')
	
	<div class="form-group">
		<div class="col-md-2"><label>Staff No</label></div>
		<div class="col-md-10"><input id="staff_no" name="staff_no" type="text" class="form-control" value="{{ old('staff_no',Auth::user()->staff->staff_no) }}" readonly="readonly">
			<p style="color: red">{{ $errors->first('staff_no') }}</p>
		</div>
		<br><br>
	</div>
	<div class="form-group">
		<div class="col-md-2"><label>Name</label></div>
		<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" value="{{ old('name',Auth::user()->name) }}">
			<p style="color: red">{{ $errors->first('name') }}</p>
		</div>
		<br><br>
	</div>
	<div class="form-group">
		<div class="col-md-2"><label>Email</label></div>
		<div class="col-md-10"><input id="email" name="email" type="email" class="form-control" value="{{ old('email',Auth::user()->email) }}" readonly="readonly">
			<p style="color: red">{{ $errors->first('email') }}</p>
		</div>
		<br><br>
	</div>
	<div class="form-group">
		<div class="col-md-2"><label>Phone No</label></div>
		<div class="col-md-10"><input id="phone_no" name="phone_no" type="text" class="form-control" value="{{ old('phone_no',Auth::user()->staff->phone_no) }}">
			<p style="color: red">{{ $errors->first('phone_no') }}</p>
		</div>
		<br><br>
	</div>
	<div class="form-group">
		<div class=" col-md-2"><label>Department</label></div>
		<div class="col-md-10">
			<select id="department_id" name="department_id" class="form-control selectpicker" data-live-search="true">
				@foreach($department as $department)
				<option value="{{ $department->id }}"
					@if($department->id == Auth::user()->staff->department_id)
					selected="selected"
					@endif
					>({{ $department->central->short_name }}) {{ $department->short_name }} - {{ $department->name }}</option>
					@endforeach
				</select>
				<p style="color: red">{{ $errors->first('department_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Role</label></div>
			<div class="col-md-10"><input type="text" name="role" id="role" class="form-control" readonly="readonly" value="{{ Auth::user()->staff->role }}">
				<p style="color: red">{{ $errors->first('role') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Password</label></div>
			<div class="col-md-10"><a href=""><i class="fas fa-unlock"></i>&nbsp;&nbsp;Change my password</a></div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<button id="btnSave" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Update</button>
				<br><br>
			</div>
		</div>
	</form>

	<br>
	
	@stop