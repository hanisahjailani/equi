<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Application;
use App\AdminCentral;
use App\SubLocation;
use App\Item;
use App\Asset;
use App\Consumable;
use App\License;
use App\Configuration;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(Auth::user()->staff->role);
        if(Auth::user()->staff->role_id != null)
        {
            if(Auth::user()->staff->role->name == "Super Admin")
            {
                $collection_date = Application::where('collection_date',date('Y-m-d'))->get();
                $return_date = Application::where('return_date',date('Y-m-d'))->get();
                $overdue = Application::where('application_status_id',7)->count();
                $asset = Asset::all()->count();
                $consumable = Consumable::all()->count();
                $license = License::all()->count();
                return view('pages.superAdmin.dashboard',compact('collection_date','return_date','overdue','asset','consumable','license'));
            }
            else
            {
                $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
                $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();
                $collection_date = Application::where('collection_date',date('Y-m-d'))->whereIn('sub_location_id',$sub_locations)->get();
                $return_date = Application::where('return_date',date('Y-m-d'))->whereIn('sub_location_id',$sub_locations)->get();
                $overdue = Application::where('application_status_id',7)->whereIn('sub_location_id',$sub_locations)->count();
                $items = Item::whereIn('sub_location_id',$sub_locations)->select('id')->get();
                $asset = Asset::whereIn('item_id',$items)->count();
                $consumable = Consumable::whereIn('item_id',$items)->count();
                $license = License::whereIn('item_id',$items)->count();
                return view('pages.admin.dashboard',compact('collection_date','return_date','overdue','admin','asset','consumable','license'));
            }
        }
        elseif(Auth::user()->staff->role_id == null)
        {
            $applications = Application::where('staff_id', Auth::user()->id)->get();
            $configs = Configuration::all();
            return view('pages.user.reservation',compact('applications','configs'));
        }

        // return view('home');
    }
}
