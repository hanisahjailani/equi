<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// StoreApplication ni belongsTo Staff
class StoreApplication extends Model
{
    protected $fillable = [
        'staff_id',
    ];
    
    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function store_application_consumables()
    {
        return $this->hasMany(StoreApplicationConsumable::class);
    }
}