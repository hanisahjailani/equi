<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubLocation extends Model
{
    protected $guarded = [];

    public function central()
    {
        return $this->belongsTo('App\Central','central_id');
    }
}
