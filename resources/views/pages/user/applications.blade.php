@extends('layouts.user.application')

@section('content1')
<center><h2>List of Application</h2></center>
&nbsp;Application /
<hr>
<div align="right">
	<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="far fa-plus-square"></i>&nbsp;New Application</button>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ route('userapplication.create') }}">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<strong>Terms & Conditions</strong>
					<br><br>
					@foreach($configs as $conf)
				 	{!! $conf->terms_condition !!}
				 	<hr>
				 	<strong>Service Level Agreement (SLA)</strong>
				 	<br><br>
				 	<ol>
				 		<li>
				 			Applicant should make the reservation <strong> {{ $conf->sla }} days early</strong> from the date they want to reserve
				 		</li>
				 	</ol>
				 	@endforeach
				 	<hr>
				 	<p><input type="checkbox" name="accept" value="Accept Terms & Conditions" required="required"> I have read and I aware of it</p>

				</div>
				<div class="modal-footer">
				  <button type="submit" class="btn btn-default"><i class="fas fa-arrow-right"></i> Next</button>
				</div>
			</form>
		</div>
	</div>
</div>

<br>
<div class="table-responsive">
<table id="tblApplication" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No.</th>
			<th width="15%">Booking ID</th>
			<th width="15%">Collection Date</th>
			<th width="15%">Return Date</th>
			<th width="25%">Item(s)</th>
			<th width="15%">Status</th>
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($applications as $application)	
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td><a href="{{ route('userapplication.show', $application->id) }}">#{{ $application->booking_id }}</a></td>
			<td>{{ $application->collection_date }}</td>
			<td>{{ $application->return_date }}</td>
			<td>
				@foreach($application->application_item_type as $app_item_type)
					{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
					@endforeach
			</td>
			<td>
				{{ $application->application_status->name }}
      			@foreach($configs as $config)
					@if(date('Y-m-d',strtotime($application->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($application->request_date)))
						<p><label style="color: red">( LATE REQUEST )</label></p>
					@endif
				@endforeach
			</td>
			<td>
				@if($application->application_status->name == "New" || $application->application_status->name == "Approved")
				<a href="{{ route('userapplication.edit',$application->id) }}"><button id="btnCancel" type="sumbit" class="btn btn-danger" onclick="return confirm('Are you sure want to cancel this application?');"><i class="fas fa-window-close"></i>&nbsp;Cancel</button></a><br>
				@elseif($application->application_status->name == "Rejected")
				<a href=""><button id="btnCancel" type="sumbit" class="btn btn-info"><i class="fas fa-info"></i>&nbsp;Info</button></a><br>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
	<script>
		$(document).ready( function () {
			$('#tblApplication').DataTable({
        "order": [[ 1, "desc" ]]
    	});
		} );
	</script>
@stop