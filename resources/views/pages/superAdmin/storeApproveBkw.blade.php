@extends('layouts.superAdmin.application')

@section('content1')
halaman index, Senarai yang terpapar untuk bkw

<center><h2>List of Application that need Approval</h2></center>
&nbsp;Store Approve (Finance) /
<hr>

<div align="right">
    <a href="{{ route('store.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;Add New</button></a>
</div>

<br>
<div class="table-responsive">
<table id="tblStoreApproveBkw" align="center" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="30%">Name</th>
            <th width="10%">Department</th>
            <th width="10%">Apply Date</th>
            <th width="10%">Hod Approve Status</th>
            <th width="10%">Hod Approve Date</th>
            <th width="10%">Finance Approve Status</th>
            <th width="10%">Finance Approve Date</th>
            <th width="10%">Receive Status</th>
            <th width="10%">Receive Date</th>
        </tr>
    </thead>

    <tbody>         
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

@stop