@extends('layouts.admin.consumable')

@section('content1')
<center><h2>Consumable Details</h2></center>
&nbsp;<a href="{{ route('adminconsumable.index') }}">Consumable</a> / {{ $consumable->item->name }}Details
<hr>
<div class="row">
	<div class="col-md-7">
		<div class="panel panel-default">
			<div class="panel-heading">Stock ( <strong>Total - {{ $consumable->total }}</strong> )</div>
			<div class="panel-body">
				@foreach($stocks as $stock)
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-5"><label>Supplier</label></div>
							<div class="col-md-7"></div>{{ $stock->supplier_name }}
						</div>
						<div class="row">
							<div class="col-md-5"><label>Ref No</label></div>
							<div class="col-md-7"></div>{{ $stock->ref_no }}
						</div>
						<div class="row">
							<div class="col-md-5"><label>Quantity</label></div>
							<div class="col-md-7"></div>{{ $stock->quantity }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-7"><label>Purchase Date</label></div>
							<div class="col-md-5"></div>{{ $stock->purchase_date }}
						</div>
						<div class="row">
							<div class="col-md-7"><label>Purchase Cost (Per Unit)</label></div>
							<div class="col-md-5"></div>RM {{ $stock->purchase_cost }}
						</div>
					</div>
				</div>
				<hr>
				@endforeach
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-info">
			<div class="panel-heading">Details</div>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4"><label>Name</label></div>
						<div class="col-md-8">{{ $consumable->item->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Item Type</label></div>
						<div class="col-md-8">{{ $consumable->itemType->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Location</label></div>
						<div class="col-md-8">{{ $consumable->item->sub_location->name }}, {{ $consumable->item->sub_location->central->name }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">Inserted By</div>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-2"><label>Name</label></div>
						<div class="col-md-10">{{ $consumable->item->staff->staff_no }} - {{ $consumable->item->staff->user->name }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-2"><label>Date</label></div>
						<div class="col-md-10">{{ $consumable->item->created_at->format('d/m/Y') }}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop