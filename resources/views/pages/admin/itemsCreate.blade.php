@extends('layouts.admin.item')

@section('content1')
<script type="text/javascript">
	$(document).ready(function() {
		const choices = new Choices('#tag_name',{
			removeItemButton: true,
			duplicateItemsAllowed : false,
		});

	$("#tag").hide();
	$("#manual_tag").click(function(){
			if($(this).is(":checked"))
			{
				$("#tag").show();
			}
			else
			{
				$("#tag").hide();
			}
		});
	});
	
</script>
<center><h2>Add Asset</h2></center><br>
&nbsp;<a href="{{ route('adminitems.index') }}">Assets</a> / Add Asset /
<hr>
<form id="frmAdd" action="{{ route('adminitems.store') }}" method="POST">
	@csrf
	<div class="form-group">
		<div class=" col-md-3"><label>Model <label style="color: red">*</label></label></div>
		<div class="col-md-5">
			<select id="model_id" name="model_id" class="form-control selectpicker" data-live-search="true">
				<option value="">Choose Model</option>
				@foreach($itemModels as $itemModel)
				<option value="{{ $itemModel->id }}"
					@if($itemModel->id  == old('model_id',$modelPick))
					selected="selected"
					@endif
					>{{ $itemModel->itemType->name }} - {{ $itemModel->brand->name }} {{ $itemModel->name }} (#{{ $itemModel->model_no}})</option>
					@endforeach
				</select>
				<p style="color: red">{{ $errors->first('model_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#modalBrand"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Model</a>
				<p style="font-size: small; color: red">**If the model is not in the list**</p>
			</div>
			<div class="modal fade" id="modalBrand" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">New Model</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3" align="right"><label>Model Name <label style="color: red">*</label></label></div>
									<div class="col-md-9"><input id="nameModel" name="nameModel" type="text" class="form-control" value="{{ old('nameModel') }}">
										<p style="color: red">{{ $errors->first('nameModel') }}</p></div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-3" align="right"><label>Brand <label style="color: red">*</label></label></div>
										<div class="col-md-9">
											<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
												<option value="">Choose Brand</option>
												@foreach($brands as $brand)
												<option value="{{ $brand->id }}">{{ $brand->name }}</option>
												@endforeach
											</select>
											<p style="color: red">{{ $errors->first('brand_id') }}</p></div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3" align="right"><label>Item Type <label style="color: red">*</label></label></div>
											<div class="col-md-9">
												<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose Item type</option>
													@foreach($itemTypes->where('category_id',1) as $itemTypes)
													<option value="{{ $itemTypes->id }}">{{ $itemTypes->name }}</option>
													@endforeach
												</select>
												<p style="color: red">{{ $errors->first('item_type_id') }}</p></div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class=" col-md-3" align="right"><label>Model No.</label></div>
												<div class="col-md-9"><input id="model_no" name="model_no" type="text" class="form-control" value="{{ old('model_no') }}">
													<p style="color: red">{{ $errors->first('model_no') }}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button id="btnSave" name="btnSave" type="submit" value="addModel" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
									</div>
								</div>
							</div>
						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class=" col-md-3"><label>Serial Number / Service Tag <label style="color: red">*</label></label></div>
						<div class="col-md-9"><input id="serial_number" name="serial_number" type="text" class="form-control" value="{{ old('serial_number') }}">
							<p style="color: red">{{ $errors->first('serial_number') }}</p>
						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class=" col-md-3"><label>Status <label style="color: red">*</label></label></div>
						<div class="col-md-9">
							<select id="item_status_id" name="item_status_id" class="form-control">
								@foreach($itemStatuses as $itemStatuses)
								<option value="{{ $itemStatuses->id }}"
									@if( $itemStatuses->id == old('item_status_id'))
									selected="selected"
									@endif
									>{{ $itemStatuses->name }}</option>
								@endforeach
							</select>
							<p style="color: red">{{ $errors->first('item_status_id') }}</p>
							<input type="checkbox" name="requestable" value="Yes" > <strong>Requestable</strong>
						</div>
						<br><br><br>
					</div>
					<div class="form-group">
						<div class=" col-md-3"><label>Location Central<label style="color: red">*</label></label></div>
						<div class="col-md-9">
							<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true" >
								<option value="">Choose Central</option>
								@foreach($sublocations as $sublocation)
								<option value="{{ $sublocation->id }}"
									@if( $sublocation->id == old('sub_location_id'))
									selected="selected"
									@endif
									>{{ $sublocation->name }}, {{ $sublocation->central->name }}</option>
								@endforeach
							</select>
							<p style="color: red">{{ $errors->first('sub_location_id') }}</p>

						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class="col-md-3"><label>Tag(s) </label></div>
						<div class="col-md-9">
							<input type="text" name="tag_name" id="tag_name" class="form-control" multiple="multiple" value="{{ old('tag_name') }}">
						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class=" col-md-3"><label>Tagging Name </label></div>
						<div class="col-md-9">
							<input type="checkbox" name="manual_tag" id="manual_tag" value="Yes" >  <label>Manual Tagging</label>

							<input id="tag" name="tag" type="text" class="form-control" value="{{ old('tag') }}">{{ $errors->first('tag') }}</p>
						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class=" col-md-3"></div>
						<div class="col-md-9" style="float:right;">
							<a href="{{ route('adminitems.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
							<button id="btnSave" name="btnSave" value="addAsset" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
							<br><br>
						</div>
					</div>
				</form>

				@stop
