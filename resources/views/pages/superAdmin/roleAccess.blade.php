@extends('layouts.superAdmin.roleAccess')

@section('content1')
<center><h2>List of Role</h2></center>
&nbsp;Role Access /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="right">
<a href="{{ route('role.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Role</button></a>
</div>
<br>
<div class="table-responsive">
<table id="tblRole" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="10%">No</th>
			<th width="70%">Role</th>
			<th width="10%">Staff</th>
			<th width="10%">Permission</th>
		</tr>
	</thead>
	<tbody>
		@foreach($roles as $role)
        <tr>
			<td>{{ $loop->iteration }}</td>
            <td>{{ $role->name }} ( {{ $staff->where('role_id',$role->id)->count() }} )</td>
			<td><a href="{{ route('roleStaff.create',$role->id) }}"><button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Staff"><i class="fas fa-user-check"></i></button></a></td>
            <td><a href="{{ route('role.edit',$role->id) }}"><button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Permission"><i class="fas fa-eye"></i></button></a></td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
<br><br>
	<script>
		$(document).ready( function () {
			$('#tblRole').DataTable();
		} );
	</script>
@stop