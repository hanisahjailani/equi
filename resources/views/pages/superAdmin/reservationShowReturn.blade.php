@extends('layouts.superAdmin.reservationReturn')
@section('content1')
<center><h2>List of Reservation (TODAY)</h2></center>
	&nbsp;Reservation / Return Item
	<hr>
	<div class="alert alert-info">
		<center><strong>Return Item</strong></center>
	</div>
	<div class="table-responsive">
	<table id="tblReturn" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">Booking No</th>
			<th width="25%">Name</th>
			<th width="25%">Item(s)</th>
			<th width="30%">Remark(s)</th>							
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($return_date as $returnDate)
			<tr>
				<td><a href="{{ route('reservation.show',$returnDate->id) }}">#{{ $returnDate->booking_id }}</a></td>
				<td>{{ $returnDate->staff->staff_no }} - {{ $returnDate->staff->user->name }}</td>
				<td>
					@foreach($returnDate->application_item_type as $app_item_type)
					{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
					@endforeach
				</td>
				<td>
					@foreach($returnDate->application_logs->where('application_status_id',$returnDate->application_status_id) as $remarks_return_date)
		                    @if(empty($remarks_return_date->remark))
								Does not have remark
							@elseif(!empty($remarks_return_date->remark))
								{{ $remarks_return_date->remark }}
							@endif
		            @endforeach
				</td>
				<td>
					<a href="{{ route('reservation.edit',$returnDate->id) }}"><button id="btnEdit" type="button" class="btn btn-success"><i class="fas fa-exchange-alt"></i>&nbsp;Return</button></a><br>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
	<script>
		$(document).ready( function () {
			$('#tblReturn').DataTable();
		} );
	</script>
@stop