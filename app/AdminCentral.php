<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminCentral extends Model
{
    protected $guarded = [];
    public function central()
    {
    	return $this->belongsTo('App\Central','central_id');
    }

    public function staff()
    {
    	return $this->belongsTo('App\Staff','staff_id');
    }
}
