@extends('layouts.admin.consumable')

@section('content1')
<script>
  $(document).ready(function() {
    $( "#purchase_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
  });
</script>
<center><h2>Restock Consumable</h2></center><br>
&nbsp;<a href="{{ route('adminconsumable.index') }}">Consumables</a> / Restock Consumable /
<hr>
	<form id="frmRestock" action="{{ route('adminconsumable.restockstore',$consumable->id) }}" method="POST">
		@csrf
		<div class="form-group">
			<div class=" col-md-3"><label>Consumable Name </label></div>
			<div class="col-md-9">{{ $consumable->item->name }}
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Supplier Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="supplier_name" name="supplier_name" type="text" class="form-control" value="{{ old('supplier_name')}}" list="supplier" autocomplete="off">
				<datalist id="supplier">
					@foreach($suppliers as $supplier)
					<option value="{{ $supplier->supplier_name }}"></option>
					@endforeach
				</datalist>
			<p style="color: red">{{ $errors->first('supplier_name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Date <label style="color: red">*</label></label></div>
			<div class="col-md-4"><input id="purchase_date" name="purchase_date" type="text" class="form-control" value="{{ old('purchase_date') }}" autocomplete="off">
			<p style="color: red">{{ $errors->first('purchase_date') }}</p>
			</div>
			<div class=" col-md-2" align="right"><label>Ref. No <label style="color: red">*</label></label></div>
			<div class="col-md-3">
				<input id="ref_no" type="text" class="form-control" name="ref_no" value="{{ old('ref_no') }}">
			<p style="color: red">{{ $errors->first('ref_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Cost (per Unit) <label style="color: red">*</label></label></div>
			<div class="col-md-4">
				<div class="input-group">
				    <span class="input-group-addon">RM</span>
				    <input id="purchase_cost" type="text" class="form-control" name="purchase_cost" value="{{ old('purchase_cost') }}">
				 </div>
			<p style="color: red">{{ $errors->first('purchase_cost') }}</p>
			</div>
			<div class=" col-md-2" align="right"><label>Quantity <label style="color: red">*</label></label></div>
			<div class="col-md-3">
				<input id="quantity" type="number" class="form-control" name="quantity" min="0" value="{{ 0,old('quantity') }}">
			<p style="color: red">{{ $errors->first('quantity') }}</p>
			</div>
			<br><br>
		</div>

		<div class="form-group">
			<div class=" col-md-3"></div>
			<div class="col-md-9" style="float:right;">
				<a href="{{ route('adminconsumable.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnSave" name="btnSave" value="restockConsumable" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop