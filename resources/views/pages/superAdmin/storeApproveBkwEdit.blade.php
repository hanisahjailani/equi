@extends('layouts.superAdmin.application')

@section('content1')
page approve EDIT bkw

<center><h2>EDIT BKW</h2></center>
&nbsp;Stor /
<hr>

<div align="right">
    <a href="{{ route('store.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;Add New</button></a>
</div>

<br>
<div class="table-responsive">
<table id="tblStor" align="center" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="20%">Item Type</th>
            <th width="20%">Item Name</th>
            <th width="15%">Quantity Needed</th>
            <th width="15%">Remarks for Applicant</th>
            <th width="15%">Remarks for Internal</th>
            <th width="15%">Pickup by</th>
            <th width="5%">Action</th>
        </tr>
    </thead>
</table>

@stop