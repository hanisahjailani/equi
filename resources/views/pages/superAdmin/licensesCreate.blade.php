@extends('layouts.superAdmin.license')

@section('content1')
<script>
  $(document).ready(function() {
    $( "#purchase_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
    $( "#expired_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
    $( "#termination_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      dateFormat: "yy-mm-dd",
    });
  });
</script>
<center><h2>Add License</h2></center><br>
&nbsp;<a href="{{ route('license.index') }}">Licenses</a> / Add License /
<hr>
	<form id="frmAdd" action="{{ route('license.store') }}" method="POST">
		@csrf
		<div class="form-group">
			<div class=" col-md-3"><label>Software Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
			<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Item Type <label style="color: red">*</label></label></div>
			<div class="col-md-5">
				<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Item Type</option>
					@foreach($itemTypes->where('category_id',3) as $itemType)
						<option value="{{ $itemType->id }}"
							@if($itemType->id == $itemTypePick)
								selected="selected"
							@endif
							>{{ $itemType->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_type_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#modalItemType"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Item Type</a>
				<p style="font-size: small; color: red">**If the item type is not in the list**</p>
			</div>
			<div class="modal fade" id="modalItemType" role="dialog">
			    <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">New Item Type</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
								<div class="col-md-9"><input id="nameItemType" name="nameItemType" type="text" class="form-control" value="{{ old('nameItemType') }}">
								<p style="color: red">{{ $errors->first('nameItemType') }}</p></div>
							</div>
							<br><br>
						</div>
						<div class="modal-footer">
						  <button id="btnSave" name="btnSave" type="submit" value="addItemType" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Product Key <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="product_key" name="product_key" type="text" class="form-control" value="{{ old('product_key') }}" >
			<p style="color: red">{{ $errors->first('product_key') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Manufacture<label style="color: red">*</label></label></div>
			<div class="col-md-5">
				<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Manufacture</option>
					@foreach($brands as $brand)
						<option value="{{ $brand->id }}"
							@if($brand->id == $manufacturePick)
								selected="selected"
							@endif
							>{{ $brand->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('brand_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#modalManufacture"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Manufacture</a>
				<p style="font-size: small; color: red">**If the manufacture is not in the list**</p>
			</div>
			<div class="modal fade" id="modalManufacture" role="dialog">
			    <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">New Manufacture</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="row">
								<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
								<div class="col-md-9"><input id="nameModel" name="nameModel" type="text" class="form-control" value="{{ old('nameModel') }}">
								<p style="color: red">{{ $errors->first('nameModel') }}</p></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
						  <button id="btnSave" name="btnSave" type="submit" value="addManufacture" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-3"><label>Supplier Name </label></div>
			<div class="col-md-9">
				<input type="text" name="supplier_name" id="supplier_name" class="form-control" list="supplier" value="{{ old('supplier_name') }}" autocomplete="off">
				<datalist id="supplier">
					@foreach($licenses as $license)
					<option value="{{ $license->supplier_name }}"></option>
					@endforeach
				</datalist>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>License Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="license_name" name="license_name" type="text" class="form-control" value="{{ old('license_name') }}" >
			<p style="color: red">{{ $errors->first('license_name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>License Email <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="license_email" name="license_email" type="text" class="form-control" value="{{ old('license_email') }}" >
			<p style="color: red">{{ $errors->first('license_email') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Status <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_status_id" name="item_status_id" class="form-control">
					@foreach($itemStatuses as $itemStatus)
						<option value="{{ $itemStatus->id }}">{{ $itemStatus->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_status_id') }}</p>
			<!-- <input type="checkbox" name="requestable" value="Yes"> <strong>Requestable</strong> -->
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Location Central<label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Central</option>
					@foreach($sublocations as $sublocation)
						<option value="{{ $sublocation->id }}">{{ $sublocation->name }}, {{ $sublocation->central->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('sub_location_id') }}</p>

			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Date </label></div>
			<div class="col-md-3"><input id="purchase_date" name="purchase_date" type="text" class="form-control" value="{{ old('purchase_date') }}" autocomplete="off">
			<p style="color: red">{{ $errors->first('purchase_date') }}</p>
			</div>
			<div class=" col-md-3" align="right"><label>Expired Date </label></div>
			<div class="col-md-3"><input id="expired_date" name="expired_date" type="text" class="form-control" value="{{ old('expired_date') }}" autocomplete="off">
			<p style="color: red">{{ $errors->first('expired_date') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Termination Date </label></div>
			<div class="col-md-3"><input id="termination_date" name="termination_date" type="text" class="form-control" value="{{ old('termination_date') }}" autocomplete="off">
			<p style="color: red">{{ $errors->first('termination_date') }}</p>
			</div>
			<div class="col-md-3" align="right"><label>Seat </label></div>
			<div class="col-md-3">
				<input type="number" name="seat" id="seat" class="form-control" min="1" value="{{ old('seat',1) }}">
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Purchase Cost </label></div>
			<div class="col-md-3">
				<div class="input-group">
				    <span class="input-group-addon">RM</span>
				    <input id="purchase_cost" type="text" class="form-control" name="purchase_cost" value="{{ old('purchase_cost') }}">
				 </div>
			<p style="color: red">{{ $errors->first('purchase_cost') }}</p>
			</div>
			<div class=" col-md-3" align="right"><label>Purchase Order No </label></div>
			<div class="col-md-3"><input id="purchase_order_num" name="purchase_order_num" type="text" class="form-control" value="{{ old('purchase_order_num') }}">
			<p style="color: red">{{ $errors->first('purchase_order_num') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"></div>
			<div class="col-md-9" style="float:right;">
				<a href="{{ route('license.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnSave" name="btnSave" value="addLicense" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop