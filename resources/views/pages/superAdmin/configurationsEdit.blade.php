@extends('layouts.superAdmin.configuration')

@section('content1')
<center><h2>Edit Configuration</h2></center>
	&nbsp;<a href="" onClick="window.history.go(-1); return false;">Configuration</a> / Edit Configuration
	<hr>
	@foreach($config as $conf)
	<form id="frmUpdateConfig" method="POST" action="{{ route('configuration.update',$conf->id) }}">
		@csrf
		@method('PATCH')
		<div class="panel panel-info">
			<div class="panel-heading"><strong>Service Level Agreement (SLA)</strong></div>
			<div class="panel-body">
				<div class="col-md-6">
					<div class="row">
						<div class=" col-md-6"><label>Duration before make reservation </label></div>
						<div class="col-md-6"><input type="number" name="sla" min="0" class="form-control" value="{{ $conf->sla }}"><br>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class=" col-md-5"><label>Maximum Item </label></div>
						<div class="col-md-7"><input type="number" name="max_item" min="0" class="form-control" value="{{ $conf->max_item }}"><br>
						</div>
					</div>
				</div>
			</div>
	  	</div>

	  	<div class="panel panel-info">
			<div class="panel-heading"><strong>Terms & Conditions</strong></div>
			<div class="panel-body">
				<textarea id="terms_condition" name="terms_condition" class="form-control">{{ $conf->terms_condition }}</textarea>
			</div>
	  	</div>
		<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Save</button>
	</form>
	@endforeach
	<br><br>
	<script src="//cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>
	<script>
        CKEDITOR.replace( 'terms_condition' );
    </script>
	<!-- <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=tyqyy54dqa6t1n8qnn485z3hdgtmncyy81q5wmfemlusueek"></script>
  	<script>tinymce.init({selector:'textarea'});</script> -->
@stop