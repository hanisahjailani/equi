@extends('layouts.superAdmin.itemType')

@section('content1')
<center><h2>New Item Type</h2></center>
&nbsp;Setting / Item Type /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="right">
	<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="far fa-plus-square"></i>&nbsp;New Item Type</button>
</div>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="frmItemType" method="POST" action="{{ route('itemtypes.store') }}">
				@csrf
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Item Type</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
						<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
							<p style="color: red">{{ $errors->first('name') }}</p></div>
						</div>
						<div class="form-group">
							<div class="col-md-3" align="right"><label>Category <label style="color: red">*</label></label></div>
							<div class="col-md-9">
								<select id="category_id" name="category_id" class="form-control selectpicker" data-live-search="true">
									<option value="">Choose Category</option>
									@foreach($categories as $category)
									<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
								</select>
								<p style="color: red">{{ $errors->first('category_id') }}</p></div>
							</div>
							<br><br><br><br>
						</div>
						<div class="modal-footer">
							<button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<br>	
		<table id="tblItems" align="center" data-show-columns="true" data-show-refresh="true" class="table table-striped table-bordered table-hover" data-advanced-search="true">
			<thead>
				<tr>
					<th width="55%">Name</th>
					<th width="15%">Category</th>
					<th width="10%">Assets</th>
					<th width="10%">Consumables</th>
					<th width="10%">Licenses</th>
					<th width="5%"></th>
					<th width="5%"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($itemTypes as $itemTypes)
				<tr>
					<td>{{ $itemTypes->name }}</td>
					<td>{{ $itemTypes->category->name }}</td>
					<td>
						@if($itemTypes->category->name == "Asset")
						{{ $assets->where('item_type_id',$itemTypes->id)->count() }}
						@elseif($itemTypes->category->name != "Asset")
						-
						@endif
					</td>
					<td>
						@if($itemTypes->category->name == "Consumable")
						{{ $consumable->where('item_type_id',$itemTypes->id)->count() }}
						@elseif($itemTypes->category->name != "Consumable")
						-
						@endif
					</td>
					<td>
						@if($itemTypes->category->name == "License")
						{{ $license->where('item_type_id',$itemTypes->id)->count() }}
						@elseif($itemTypes->category->name != "License")
						-
						@endif
					</td>
					<td>
						<button id="btnEdit" type="button" class="btn btn-info" data-toggle="modal" data-target="#modalUpdate{{ $itemTypes->id }}" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button>
						<div class="modal fade" id="modalUpdate{{ $itemTypes->id }}" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
									<form id="frmItemType" method="POST" action="{{ route('itemtypes.update',$itemTypes->id) }}">
										@csrf
										@method('PATCH')
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Edit Item Type</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
												<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name',$itemTypes->name) }}">
													<p style="color: red">{{ $errors->first('name') }}</p></div>
												</div>
												<div class="form-group">
													<div class="col-md-3" align="right"><label>Category <label style="color: red">*</label></label></div>
													<div class="col-md-9">
														<select id="category_id" name="category_id" class="form-control selectpicker" data-live-search="true">
															<option value="">Choose Category</option>
															@foreach($categories as $category)
															<option value="{{ $category->id }}"
																@if($category->id == $itemTypes->category_id)
																selected="selected"
																@endif
																>{{ $category->name }}</option>
																@endforeach
															</select>
															<p style="color: red">{{ $errors->first('category_id') }}</p></div>
														</div>
														<br><br><br><br>
													</div>
													<div class="modal-footer">
														<button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Update</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</td>
								<td>
									<form id="frmDelete" action="{{ route('itemtypes.destroy', $itemTypes->id) }}" method="POST">
										@csrf
										@method('DELETE')	
										<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<br>
					<script>
						$(document).ready( function () {
							$('#tblItems').DataTable();
						} );
					</script>
					@stop

