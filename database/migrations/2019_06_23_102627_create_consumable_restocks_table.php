<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableRestocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumable_restocks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('purchase_date');
            $table->string('purchase_cost');
            $table->integer('quantity');
            $table->string('supplier_name');
            $table->string('ref_no');
            $table->integer('consumable_id')->unsigned()->nullable();
            $table->foreign('consumable_id')->references('id')->on('consumables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_restocks');
    }
}
