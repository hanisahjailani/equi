<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Schema::disableForeignKeyConstraints();
        // DB::table('users')->truncate();
        // DB::table('departments')->truncate();
        // DB::table('staff')->truncate();
        //users
        DB::table('users')->insert([
            'id'=>'1',
            'name'=>'Super Admin',
            'email'=>'superadmin@utmspace.edu.my',
            'password'=>Hash::make('superadmin123'),
        ]);
        DB::table('users')->insert([
            'id'=>'2',
            'name'=>'Amir',
            'email'=>'amir@utmspace.edu.my',
            'password'=>Hash::make('amir'),
        ]);
        DB::table('users')->insert([
            'id'=>'3',
            'name'=>'Nur Atiqah',
            'email'=>'atiqah@utmspace.edu.my',
            'password'=>Hash::make('atiqah'),
        ]);
        DB::table('users')->insert([
            'id'=>'4',
            'name'=>'Nur Amalina binti Hassan',
            'email'=>'amalina@utmspace.edu.my',
            'password'=>Hash::make('amalina'),
        ]);
        DB::table('users')->insert([
            'id'=>'5',
            'name'=>'Afiq Ahmad bin Zul',
            'email'=>'afiq@utmspace.edu.my',
            'password'=>Hash::make('afiq'),
        ]);
        DB::table('users')->insert([
            'id'=>'6',
            'name'=>'Nur Baiduri bin Zulkepli',
            'email'=>'baiduri@utmspace.edu.my',
            'password'=>Hash::make('baiduri'),
        ]);

        //department
        DB::table('departments')->insert([
            'id'=>'1',
            'short_name'=>'BTMK',
            'name'=>'Bahagian Teknologi Maklumat & Komunikasi',
            'central_id'=>'1',
        ]);
        DB::table('departments')->insert([
            'id'=>'2',
            'short_name'=>'PPI',
            'name'=>'Pusat Pengajian Ijazah',
            'central_id'=>'1',
        ]);
        DB::table('departments')->insert([
            'id'=>'3',
            'short_name'=>'PPSM',
            'name'=>'Pusat Pengajian Separuh Masa',
            'central_id'=>'1',
        ]);
        DB::table('departments')->insert([
            'id'=>'4',
            'short_name'=>'BTMK',
            'name'=>'Bahagian Teknologi Maklumat & Komunikasi',
            'central_id'=>'2',
        ]);

        //staff
        DB::table('staff')->insert([
            'id'=>'1',
        	'staff_no'=>'A0029',
        	'phone_no'=>'01127525054',
            'user_id'=>'1',
            'department_id'=>'1',
            'role_id'=>'1',
        ]);
        DB::table('staff')->insert([
            'id'=>'2',
            'staff_no'=>'M0016',
            'phone_no'=>'0137013742',
            'user_id'=>'2',
            'department_id'=>'1',
            'role_id'=>'2',
        ]);
        DB::table('staff')->insert([
            'id'=>'3',
            'staff_no'=>'S0024',
            'phone_no'=>'0193019382',
            'user_id'=>'3',
            'department_id'=>'1',
            'role_id'=>'2',
        ]);
        DB::table('staff')->insert([
            'id'=>'4',
            'staff_no'=>'S0020',
            'phone_no'=>'0193019382',
            'user_id'=>'4',
            'department_id'=>'2',
            'role_id'=>'2',
        ]);
        DB::table('staff')->insert([
            'id'=>'5',
            'staff_no'=>'M0012',
            'phone_no'=>'0173019382',
            'user_id'=>'5',
            'department_id'=>'4',
            'role_id'=>'2',
        ]);
        DB::table('staff')->insert([
            'id'=>'6',
            'staff_no'=>'S0052',
            'phone_no'=>'0193012382',
            'user_id'=>'6',
            'department_id'=>'4',
            'role_id'=>'1',
        ]);

        // Schema::enableForeignKeyConstraints();

    }
}
