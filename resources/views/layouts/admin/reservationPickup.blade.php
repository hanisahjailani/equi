@extends('layouts.app')

@section('content')
<div id="main" class="row" >
	<div class="col-md-3" >
		<ul class="nav nav-pills nav-stacked" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888; padding: 10px;">
			<li><a href="{{ route('admindashboard.index') }}">
				<i class="fas fa-tachometer-alt"></i>&nbsp;&nbsp;Dashboard</a></li>
			@foreach($roleAccesses as $roleAccess)
			@if($roleAccess->access->menu == "Reservation")
            <li class="parent"><a data-toggle="collapse" href="#sub-item-4">
    			<i class="far fa-calendar-alt"></i>&nbsp;&nbsp;Reservation
    			<span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
    			</a>
    			<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-4">
    				<li class="active"><a href="{{ route('adminreservation.showList',2) }}">
    					<i class="far fa-check-square" style="color: orange"></i> Pickup
    				</a></li>
    				<li><a href="{{ route('adminreservation.showList',5) }}">
    					<i class="far fa-check-square" style="color: green"></i> Return
    				</a></li>
    				<li><a href="{{ route('adminreservation.showList',7) }}">
    					<i class="far fa-check-square" style="color: red"></i> Overdue
    				</a></li>
    			</ul>
			</li>
			@elseif($roleAccess->access->menu == "Application")
            <li class="parent"><a data-toggle="collapse" href="#sub-item-1">
				<i class="far fa-calendar-check">&nbsp;</i> Application
					@if($notiReservation > 0)
					<span style="padding: 3px 8px; border-radius: 50%; background: red; color: white;">
					{{ $notiReservation }}
					</span>
					@endif
				<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-1">
					<li><a href="{{ route('adminapplication.index') }}">
						<i class="far fa-file-alt"></i> New Application List
						@if($notiReservation > 0)
						<span style="padding: 3px 8px; border-radius: 50%; background: red; color: white;">
						{{ $notiReservation }}
						</span>
						@endif
					</a></li>
					<li><a class="" href="{{ route('adminapplicationhistory.index') }}">
						<i class="fas fa-history"></i> History Application
					</a></li>
				</ul>
			</li>
			@elseif($roleAccess->access->menu == "Asset")
            <li class="parent"><a data-toggle="collapse" href="#sub-item-3">
    			<i class="fas fa-warehouse"></i> Asset
    			<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
    			</a>
    			<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-3">
    				<li><a href="{{ route('adminitems.index') }}">
    				    <i class="far fa-circle"></i>&nbsp;&nbsp;All Asset</a></li>
    				<li><a href="{{ route('adminitems.indexRequestable') }}">
    				    <i class="fas fa-check"></i>&nbsp;&nbsp;Requestable Asset</a></li>
    			</ul>
			</li>
			@elseif($roleAccess->access->menu == "Consumable")
            <li><a href="{{ route('adminconsumable.index') }}">
				<i class="fas fa-tint"></i>&nbsp;&nbsp;Consumables</a></li>
			@elseif($roleAccess->access->menu == "License")
            <li><a href="{{ route('adminlicense.index') }}">
				<i class="fas fa-barcode"></i></i>&nbsp;&nbsp;License</a></li>
			@elseif($roleAccess->access->menu == "Report")
            <li><a href="">
                <i class="fas fa-chart-area"></i>&nbsp;&nbsp;Report</a></li>
            <li class="parent"><a data-toggle="collapse" href="#sub-item-2">
				<i class="fas fa-cog">&nbsp;</i> Setting <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-2">
					@elseif($roleAccess->access->menu == "Status Label")
					<li><a href="{{ route('adminstatuslabels.index') }}">
						<i class="fas fa-tag"></i> Status Label
					</a></li>
					@elseif($roleAccess->access->menu == "Item Type")
					<li><a href="{{ route('adminitemtypes.index') }}">
						<i class="fas fa-tasks"></i> Item Type
					</a></li>
					@elseif($roleAccess->access->menu == "Brand")
					<li><a href="{{ route('adminbrands.index') }}">
						<i class="fas fa-industry"></i> Brand/Manufacture
					</a></li>
					@elseif($roleAccess->access->menu == "Item Model")
					<li><a href="{{ route('adminitemmodels.index') }}">
						<i class="fas fa-layer-group"></i> Item Model
					</a></li>
					@elseif($roleAccess->access->menu == "Configuration")
					<li><a href="{{ route('adminconfiguration.index') }}">
							<i class="fas fa-cogs"></i> Configuration
					</a></li>
					@elseif($roleAccess->access->menu == "User Info")
					<li><a href="{{ route('adminuser.index') }}">
						<i class="fas fa-user-cog"></i> User Info
					</a></li>
				</ul>
			</li>
			@endif
			@endforeach
      </ul>
	<br />
	</div>
	<div class="col-md-9" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888;">
		@yield('content1')
	</div>
</div>
@stop