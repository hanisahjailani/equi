<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

use App\Application;
use App\AdminCentral;
use App\SubLocation;

class NotiComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();

        $view->with('notiReservation', $this->application->where('application_status_id',1)->whereIn('sub_location_id',$sub_locations)->count());
        $view->with('notiReservationSAdmin', $this->application->where('application_status_id',1)->count());
    }
}