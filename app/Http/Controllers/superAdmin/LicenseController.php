<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\ItemType;
use App\ItemStatus;
use App\SubLocation;
use App\Brand;
use App\Category;
use App\Item;
use App\License;
use Session;

class LicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $licenses = License::all();

        return view('pages.superAdmin.licenses',compact('licenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $itemTypes = ItemType::all();
        $itemStatuses = ItemStatus::all();
        $sublocations = SubLocation::all();
        $brands = Brand::all();
        $licenses = License::all();
        $itemTypePick = $request->session()->get('itemTypePick');
        $manufacturePick = $request->session()->get('manufacturePick');
        return view('pages.superAdmin.licensesCreate',compact('itemTypes','itemStatuses','sublocations','brands','itemTypePick','manufacturePick','licenses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnSave') == 'addItemType')
        {
            $this->validate($request, [
                'nameItemType' => 'required',
            ]);
            $categories = Category::where('name','License')->get();
            foreach($categories as $category)
            {
                $itemType = ItemType::create([
                    'name' => $request->input('nameItemType'),
                    'category_id' => $category->id,
                ]);
            }

            $request->session()->put('itemTypePick',$itemType->id);
            return redirect()->route('license.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addManufacture')
        {
            $this->validate($request, [
                'nameModel' => 'required',
            ]);

            $manufacture = Brand::create([
                'name' => $request->input('nameModel'),
            ]);

            $request->session()->put('manufacturePick',$manufacture->id);
            return redirect()->route('license.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addLicense')
        {
            $this->validate($request, [
                'item_type_id' => 'required',
                'brand_id' => 'required',
                'item_status_id' => 'required',
                'name' => 'required',
                'product_key' => 'required',
                'license_name' => 'required',
                'license_email' => 'required',
                'sub_location_id' => 'required',
            ]);

            $item = Item::create([
                'name' => $request->input('name'),
                'sub_location_id' => $request->input('sub_location_id'),
                'staff_id' => Auth::user()->staff->id,
                'item_status_id' => $request->input('item_status_id'),
            ]);

            $license = License::create([
                'purchase_date' => $request->input('purchase_date'),
                'purchase_cost' => $request->input('purchase_cost'),
                'brand_id' => $request->input('brand_id'),
                'item_type_id' => $request->input('item_type_id'),
                'item_id' => $item->id,
                'termination_date' => $request->input('termination_date'),
                'purchase_order_num' => $request->input('purchase_order_num'),
                'expired_date' => $request->input('expired_date'),
                'product_key' => $request->input('product_key'),
                'license_name' => $request->input('license_name'),
                'license_email' => $request->input('license_email'),
                'supplier_name' => $request->input('supplier_name'),
                'seat' => $request->input('seat'),
            ]);

            $request->session()->forget(['itemTypePick','manufacturePick']);
            return redirect()->route('license.index')->with('success','Data inserted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.superAdmin.licensesShow');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $license = License::findOrFail($id);
        $licenses = License::all();
        $itemTypes = ItemType::all();
        $itemStatuses = ItemStatus::all();
        $sublocations = SubLocation::all();
        $brands = Brand::all();
        return view('pages.superAdmin.licensesEdit',compact('license','itemTypes','itemStatuses','sublocations','brands','licenses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'item_type_id' => 'required',
            'brand_id' => 'required',
            'item_status_id' => 'required',
            'name' => 'required',
            'product_key' => 'required',
            'license_name' => 'required',
            'license_email' => 'required',
            'sub_location_id' => 'required',
        ]);

        $license = License::findOrFail($id);
        $license->item_type_id = $request->input('item_type_id');
        $license->brand_id = $request->input('brand_id');
        $license->purchase_date = $request->input('purchase_date');
        $license->purchase_cost = $request->input('purchase_cost');
        $license->termination_date = $request->input('termination_date');
        $license->purchase_order_num = $request->input('purchase_order_num');
        $license->expired_date = $request->input('expired_date');
        $license->product_key = $request->input('product_key');
        $license->license_name = $request->input('license_name');
        $license->license_email = $request->input('license_email');
        $license->seat = $request->input('seat');
        $license->supplier_name = $request->input('supplier_name');
        $license->save();

        $item = Item::find($license->item_id);
        $item->name = $request->input('name');
        $item->sub_location_id = $request->input('sub_location_id');
        $item->item_status_id = $request->input('item_status_id');
        $item->save();

        return redirect()->route('license.index')->with('success','Data updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $license = License::find($id);
        $license->delete();

        $item = Item::destroy($license->item_id);
        return redirect()->route('license.index')->with('success','Data has been successfully remove!');
    }
}
