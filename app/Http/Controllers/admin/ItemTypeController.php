<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Item;
use App\ItemType;
use App\Category;
use App\AdminCentral;
use App\SubLocation;
use DB;

class ItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemTypes = ItemType::all();
        $categories = Category::all();
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();       
        $assets = DB::table('assets')
                ->join('item_models','assets.model_id','=','item_models.id')
                ->join('items','assets.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        $consumable = DB::table('consumables')
                ->join('items','consumables.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        $license = DB::table('licenses')
                ->join('items','licenses.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        return view('pages.admin.itemTypes', compact('itemTypes','categories','assets','consumable','license'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:item_types',   
            'category_id' => 'required',         
        ]);

        $itemType = ItemType::create($request->all());
        return redirect()->route('adminitemtypes.index')->with('success', 'Data Inserted!');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itemType = ItemType::findOrFail($id);
        $itemType->fill($request->all());
        $itemType->save();
        return redirect()->route('adminitemtypes.index')->with('success','Data updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemTypes = ItemType::destroy($id);
        return redirect()->route('adminitemtypes.index')->with('success', 'Data Successfully Removed!');
    }
}
