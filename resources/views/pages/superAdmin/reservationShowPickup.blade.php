@extends('layouts.superAdmin.reservationPickup')

@section('content1')
<center><h2>List of Reservation (TODAY)</h2></center>
	&nbsp;Reservation / Pickup Item
	<hr>
	<div class="alert alert-info">
		<center><strong>Pickup Item</strong></center>
	</div>
	<div class="table-responsive">
	<table id="tblCollection" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">Booking No</th>
				<th width="25%">Name</th>
				<th width="25%">Item(s)</th>
				<th width="30%">Remark(s)</th>						
				<th width="10%">Action</th>
			</tr>
		</thead>
		<tbody> 
			@foreach($collection_date as $collectionDate)
				<tr>
					<td><a href="{{ route('reservation.show',$collectionDate->id) }}">#{{ $collectionDate->booking_id }}</a></td>
					<td>{{ $collectionDate->staff->staff_no }} - {{ $collectionDate->staff->user->name }}</td>
					<td>
						@foreach($collectionDate->application_item_type as $app_item_type)
						{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
						@endforeach
					</td>
					<td>
						@foreach($collectionDate->application_logs->where('application_status_id',$collectionDate->application_status_id) as $remarks_collection_date)
		                    @if(empty($remarks_collection_date->remark))
								Does not have remark
							@elseif(!empty($remarks_collection_date->remark))
								{{ $remarks_collection_date->remark }}
							@endif
		            	@endforeach
					</td>
					<td>
						<a href="{{ route('reservation.edit',$collectionDate->id) }}"><button id="btnEdit" type="button" class="btn btn-success"><i class="fas fa-check-circle"></i>&nbsp;Pickup</button></a><br>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<br>
	<script>
		$(document).ready( function () {
			$('#tblCollection').DataTable();
		} );
	</script>
@stop