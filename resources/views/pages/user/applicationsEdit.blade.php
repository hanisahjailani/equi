@extends('layouts.user.application')

@section('content1')

<center><h2>New Application</h2></center>
&nbsp;<a href="{{ route('userapplication.index') }}">Application</a> / Create New Application /
<hr>
<div class="alert alert-info">
	<center><i class="fas fa-sticky-note"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="fas fa-user" ></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="fas fa-warehouse" style="color:grey;"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="far fa-check-circle" style="color:grey;"></i></center> 
</div>
	<form id="frmAdd" action="{{ route('userapplication.update',$application->id)}}" method="POST">
		@csrf
		@method('PATCH')
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-md-2"><label>Applicant <label style="color: red">*</label></label></div>
							<div class="col-md-6">
								<select id="staff_id" name="staff_id" class="form-control selectpicker" data-live-search="true">
										<option value="">Choose Staff</option>
									@foreach($applicant as $staff)
										<option value="{{ $staff->id }}">{{ $staff->staff_no }} - {{ $staff->user->name }}</option>
									@endforeach
								</select>
								<p style="color: red">{{ $errors->first('staff_id') }}</p>
							</div>
							<div class="col-md-4">

							</div>
						</div>
					</div>
				</div>	
				<br>					
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div class="col-md-12" style="float:right;">
						<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-chevron-right"></i>&nbsp;Next</button>&nbsp; 
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</form>
	
<br>
@stop