<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableAppItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // - id
        // - consumable_application_id
        // - consumable_id
        // - quantity
        Schema::create('consumable_app_items', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('consumable_application_id');
            $table->unsignedInteger('consumable_id');

            $table->foreign('consumable_application_id')->references('id')->on('consumable_applications');
            $table->foreign('consumable_id')->references('id')->on('consumables');

            $table->integer('quantity');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_app_items');
    }
}
