@extends('layouts.superAdmin.statusLabel')

@section('content1')
<center><h2>Status Labels</h2></center>
&nbsp;Setting / Status Labels /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<br>
<div class="table-responsive">
	
	<table id="tblItems" align="center" data-show-columns="true" data-show-refresh="true" class="table table-striped table-bordered table-hover" data-advanced-search="true">
		<thead>
			<tr>
				<th width="25%">Status Name</th>
				<th width="60%">Description</th>
				<th width="15%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($statuses as $status)
			<tr>
				<td style="background-color: #{{$status->color}}">
					{{ $status->name }}
				</td>
				<td>{{ $status->description }}</td>
				<form id="frmEditColor" method="POST" action="{{ route('statuslabels.update',$status->id) }}">
					@csrf
					@method("PATCH")
					<td>
						<input id="color" name="color" type="text" class="form-control jscolor {value:'{{$status->color}}'}" value="{{ old('color')}}">
						<p style="color: red">{{ $errors->first('color') }}</p>
					</td>
					<td>
						<button id="btnEdit" type="submit" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Update"><i class="fas fa-sync-alt"></i></button>
					</td>
				</form>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable();
		// $('#color').colorpicker();
	} );
	
</script>
@stop

