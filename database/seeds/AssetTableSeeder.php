<?php

use Illuminate\Database\Seeder;

class AssetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//items
    	DB::table('items')->insert([
            'id'=>'1',
            'name'=>'pembaris besi',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

        DB::table('items')->insert([
            'id'=>'2',
            'name'=>'pembaris kayu',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

        DB::table('items')->insert([
            'id'=>'3',
            'name'=>'pembaris panjang',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

        DB::table('items')->insert([
            'id'=>'4',
            'name'=>'pembaris pendek',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

        DB::table('items')->insert([
            'id'=>'5',
            'name'=>'pemadam papan hitam',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

        DB::table('items')->insert([
            'id'=>'6',
            'name'=>'pemadam getah',
            'sub_location_id'=>'1',
            'staff_id'=>'1',
            'item_status_id'=>'1',

        ]);

    	//item_models
    	DB::table('item_models')->insert([
            'id'=>'1',
            'name'=>'Computer',
            'model_no'=>'a123',
            'brand_id'=>'1',
            'item_type_id'=>'1',

        ]);

        DB::table('item_models')->insert([
            'id'=>'2',
            'name'=>'Printer',
            'model_no'=>'a456',
            'brand_id'=>'1',
            'item_type_id'=>'2',

        ]);

        DB::table('item_models')->insert([
            'id'=>'3',
            'name'=>'Komputer riba',
            'model_no'=>'a789',
            'brand_id'=>'1',
            'item_type_id'=>'1',

        ]);

    	//assets
        DB::table('assets')->insert([
            'tag'=>'BTMKJB 10',
            'serial_number'=>'2CW0ZN2',
            'requestable'=>'Yes',
            'model_id'=>'1',
            'item_id'=>'1',

        ]);

        DB::table('assets')->insert([
            'tag'=>'BTMKJB 12',
            'serial_number'=>'5A154724W',
            'requestable'=>'Yes',
            'model_id'=>'2',
            'item_id'=>'2',

        ]);

        DB::table('assets')->insert([
            'tag'=>'BTMKJB 07',
            'serial_number'=>'FBW0ZN2',
            'requestable'=>'Yes',
            'model_id'=>'1',
            'item_id'=>'3',

        ]);

        DB::table('assets')->insert([
            'tag'=>'BTMKJB 08',
            'serial_number'=>'FCW0ZN2',
            'requestable'=>'Yes',
            'model_id'=>'1',
            'item_id'=>'4',

        ]);

        DB::table('assets')->insert([
            'tag'=>'NETBOOK UICT 06',
            'serial_number'=>'JJQWRQ1',
            'requestable'=>'Yes',
            'model_id'=>'1',
            'item_id'=>'5',

        ]);

         DB::table('assets')->insert([
            'tag'=>'NETBOOK UICT 03',
            'serial_number'=>'3KQWRQ1',
            'requestable'=>'No',
            'model_id'=>'1',
            'item_id'=>'6',
   
        ]);

         //consumables
         DB::table('consumables')->insert([
            'total'=>'50',
            'balance'=>'49',
            'item_id'=>'1',
            'brand_id'=>'9',
            'item_type_id'=>'8',
            'product_no'=>'b009',
   
        ]);

         DB::table('consumables')->insert([
            'total'=>'50',
            'balance'=>'48',
            'item_id'=>'2',
            'brand_id'=>'9',
            'item_type_id'=>'8',
            'product_no'=>'b010',
   
        ]);

         DB::table('consumables')->insert([
            'total'=>'50',
            'balance'=>'47',
            'item_id'=>'3',
            'brand_id'=>'9',
            'item_type_id'=>'8',
            'product_no'=>'b011',
   
        ]);

         DB::table('consumables')->insert([
            'total'=>'50',
            'balance'=>'46',
            'item_id'=>'5',
            'brand_id'=>'9',
            'item_type_id'=>'9',
            'product_no'=>'b012',
   
        ]);

         DB::table('consumables')->insert([
            'total'=>'50',
            'balance'=>'45',
            'item_id'=>'6',
            'brand_id'=>'9',
            'item_type_id'=>'9',
            'product_no'=>'b013',
   
        ]);

         //item_tags
         DB::table('item_tags')->insert([
            'name'=>'BTMK JB 01',
            'asset_id'=>'1', 
        ]);

          DB::table('item_tags')->insert([
            'name'=>'BTMK JB 02',
            'asset_id'=>'2', 
        ]);

           DB::table('item_tags')->insert([
            'name'=>'BTMK JB 03',
            'asset_id'=>'3', 
        ]);

            DB::table('item_tags')->insert([
            'name'=>'BTMK JB 04',
            'asset_id'=>'4', 
        ]);

             DB::table('item_tags')->insert([
            'name'=>'BTMK JB 05',
            'asset_id'=>'5', 
        ]);

              DB::table('item_tags')->insert([
            'name'=>'BTMK JB 06',
            'asset_id'=>'6', 
        ]);
    }
}
