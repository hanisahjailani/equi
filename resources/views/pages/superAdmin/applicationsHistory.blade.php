@extends('layouts.superAdmin.applicationhistory')

@section('content1')
<center><h2>History Application</h2></center>
&nbsp;History Application /
<hr>
@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
@endif
<div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fas fa-filter"></i> Filter</div>
      <div class="panel-body">
      	<div class="row">
      		<div class="col-md-3">
      			<div class="form-group">
      				<div class="col-md-3" align="right">
      					Year
      				</div>
      				<div class="col-md-9">
      					<select id="year" name="year" class="form-control selectpicker" data-live-search="true">
		      				<option value="2019">2019</option>
		      				<option value="2018">2018</option>
		      			</select>
      				</div>
	      		</div>
      		</div>
      		<div class="col-md-3">
      			<div class="form-group">
      				<div class="col-md-3" align="right">
      					Status
      				</div>
      				<div class="col-md-9">
      					<select id="status" name="status" class="form-control selectpicker" data-live-search="true">
		      				<option value="1">All</option>
		      				<option value="1">New</option>
		      				<option value="2">Approved</option>
		      			</select>
      				</div>
	      		</div>
      		</div>
      		<div class="col-md-4">
      		</div>
      		<div class="col-md-2">
      			<button id="search" name="search" class="btn btn-primary"><i class="fas fa-search"></i> Search</button>
      		</div>
      	</div>
      </div>
    </div>
</div>
<br>
<div class="table-responsive">
<table id="tblHistoryApplication" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">Booking ID</th>
			<th width="20%">Applicants</th>
			<th width="20%">Item(s)</th>
			<th width="20%">Collection Date</th>
			<th width="20%">Return Date</th>
			<th width="15%">Application Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($applicationHistory as $appHistory)
		<tr>
			<td><a href="{{ route('applicationhistory.show',$appHistory->id) }}">#{{ $appHistory->booking_id }}</a></td>
			<td>
				{{ $appHistory->staff->staff_no }} - {{ $appHistory->staff->user->name }}
			</td>
			<td>
				@foreach($appHistory->application_item_type as $app_item_type)
				{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
				@endforeach
			</td>
			<td>{{ $appHistory->collection_date }}</td>
			<td>{{ $appHistory->return_date }}</td>
			<td>{{ $appHistory->application_status->name }}
				@foreach($configs as $config)
					@if(date('Y-m-d',strtotime($appHistory->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($appHistory->request_date)))
						<p><label style="color: red">( LATE REQUEST )</label></p>
					@endif
				@endforeach
				@if($appHistory->actual_return_date > $appHistory->return_date)
					<p><label style="color: red">( OVERDUE )</label></p>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblHistoryApplication').DataTable({
        "order": [[ 0, "desc" ]]
    	});
	} );
</script>					
@stop