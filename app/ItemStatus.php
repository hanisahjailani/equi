<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
    protected $guarded = [];

    public function items()
    {
    	return $this->hasMany('App\Item');
    }
}
