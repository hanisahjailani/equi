<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\Item;
use DB;

class ApplicationApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$applicationid)
    {
        $application = Application::findOrFail($applicationid);
        $items = Item::findOrFail($id);

        //check quantity
        $applicationItemTypes = ApplicationItemType::where('application_id',$application->id)->where('item_type_id', $items->asset->model->item_type_id)->get();

        $applicationItems = DB::table('items')
                                ->join('application_items','items.id','=','application_items.item_id')
                                ->join('assets','items.id','=','assets.item_id')
                                ->join('item_models','assets.model_id','=','item_models.id')
                                ->where('application_items.application_id',$application->id)
                                ->where('item_models.item_type_id',$items->asset->model->item_type_id)
                                ->whereNull('items.deleted_at')
                                ->count();

        foreach ($applicationItemTypes as $applicationItemType) {
            // dd($applicationItemType->quantity,$applicationItems);
            if($applicationItems >= $applicationItemType->quantity)
            {
                return redirect()->route('applicationapprove.edit',$application->id)->with('failure', 'This item cannot be select because over limit!');
            }
            else
            {
                $applicationItem = ApplicationItem::create([
                    'application_id' => $application->id,
                    'item_id' => $items->id,
                ]);

                //update item_status_id(6) = booked
                $items->item_status_id = 6;
                $items->save();

                return redirect()->route('applicationapprove.edit',$application->id)->with('success', 'Successfully booked the item!');
            }
           
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicationItems = ApplicationItem::where('application_id',$id)->get();
        $applications = Application::findOrFail($id);
        $applicationitemtype = ApplicationItemType::where('application_id',$id)->select('item_type_id')->get();
        $applicationitemtypes = ApplicationItemType::where('application_id',$id)->get();
       
        $items = DB::table('assets')
                    ->join('items','assets.item_id','=','items.id')
                    ->join('item_models','assets.model_id','=','item_models.id')
                    ->join('item_types','item_models.item_type_id','=','item_types.id')
                    ->join('item_statuses','items.item_status_id','=','item_statuses.id')
                    ->join('brands','item_models.brand_id','=','brands.id')
                    ->whereIn('item_types.id',$applicationitemtype)
                    ->where('items.sub_location_id',$applications->sub_location_id)
                    ->whereIn('items.item_status_id',['1','2','5','6'])
                    ->where('assets.requestable','Yes')
                    ->whereNotIn('items.id',function ($query) use ($applications) {
                                    $query->select('application_items.item_id')
                                    ->from('application_items')
                                    ->join('applications','application_items.application_id','=','applications.id')
                                    ->where('applications.collection_date','<=',$applications->return_date)
                                    ->Where('applications.return_date','>=',$applications->collection_date);
                                })
                    ->select('items.*','assets.tag','item_models.name as item_model_name','item_types.name as item_type_name','item_statuses.name as item_status_name','brands.name as brand_name')
                    ->get();
        return view('pages.superAdmin.applicationsEditApprove',compact('items','applicationItems','applications','applicationitemtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$applicationid)
    {
         //update item_status_id(1) = available
        $applicationItems = ApplicationItem::findOrFail($id);
        $items = Item::findOrFail($applicationItems->item_id);
        $items->item_status_id = 1;
        $items->save();
        $application = Application::findOrFail($applicationid);

        $deleteitem = ApplicationItem::destroy($id);
        return redirect()->route('applicationapprove.edit',$application->id);
    }
}
