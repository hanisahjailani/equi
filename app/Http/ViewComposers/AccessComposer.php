<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

use App\RoleAccess;
use App\Staff;

class AccessComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(RoleAccess $roleAccess)
    {
        $this->roleAccess = $roleAccess;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $staff = Staff::find(Auth::user()->id);

        $view->with('roleAccesses', $this->roleAccess->where('role_id',$staff->role_id)->orderBy('access_id','asc')->get());
    }
}