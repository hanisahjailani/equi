@extends('layouts.app')

@section('content')
<div class="col-md-3" >
                
</div>
<div class="col-md-6" style="background-color: white; border-radius: 15px; box-shadow: 10px 10px #888888;">
    <br>
    <center><h2>Login</h2></center>
    <hr>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <div class="col-md-offset-1 col-md-2"><label>{{ __('E-Mail Address') }}</label></div>
            <div class="col-md-9"><input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="eg : email@utmspace.edu.my" required autofocus>
            <p style="color: red">{{ $errors->first('email') }}</p>
            </div>
            <br><br>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-2"><label>{{ __('Password') }}</label></div>
            <div class="col-md-9"><input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            <p style="color: red">{{ $errors->first('password') }}</p>
            </div>
            <br><br>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-2"></div>
            <div class="col-md-9">
                <div class="form-check">
                    <input type="checkbox" name="remember" id="remember" class="form-check-input{{ old('remember') ? 'checked' : '' }}">
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <br><br>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-2"></div>
            <div class="col-md-9" style="float:right;">
                <button id="btnSave" type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>&nbsp;{{ __('Login') }}</button>

                 @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                <br><br>
            </div>
        </div>
    </form>
</div>
<div class="col-md-3" >
    
</div>

@endsection
