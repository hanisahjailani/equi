<?php

namespace App\Http\Middleware;

use Closure;

class CheckStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = $request->user()->staff->role;

        if ($role->name != "Staff") {
            // Logout current user
            return redirect()->route('login');
        }

        return $next($request);
    }
}
