@extends('layouts.admin.applicationhistory')

@section('content1')
<center><h2>Application Detail</h2></center>
&nbsp;<a href="{{ route('adminapplicationhistory.index') }}">History Application</a> / Application Details
<hr>
<div class="panel panel-info">
      <div class="panel-heading">Applicant Info</div>
      <div class="panel-body">
      		<div class="col-md-6">
      			<div class="row">
					<div class=" col-md-4"><label>Staff ID </label></div>
					<div class="col-md-8">{{ $application->staff->staff_no }}
					</div>
				</div>
				<div class="row">
					<div class=" col-md-4"><label>Name </label></div>
					<div class="col-md-8">{{ $application->staff->user->name }}
					</div>
				</div>
      		</div>
      		<div class="col-md-6">
      			<div class="row">
					<div class=" col-md-4"><label>Email </label></div>
					<div class="col-md-8">{{ $application->staff->user->email }}
					</div>
				</div>
				<div class="row">
					<div class=" col-md-4"><label>Phone No </label></div>
					<div class="col-md-8">{{ $application->staff->phone_no }}
					</div>
				</div>
      		</div>
      		<div class="col-md-12">
      			<hr>
      			<div class="row">
					<div class="col-md-3"><label>Central </label></div>
					<div class="col-md-9">{{ $application->staff->department->central->name }} ( {{ $application->staff->department->central->short_name }} )
					</div>
				</div>
      			<div class="row">
					<div class="col-md-3"><label>Department </label></div>
					<div class="col-md-9">{{ $application->staff->department->name }} ( {{ $application->staff->department->short_name }} )
					</div>
				</div>
				<div class="row">
					<div class="col-md-3"><label>PIC Department </label></div>
					<div class="col-md-9">
						@if(empty($application->staff->department->pic_staff_id))
							<p>Not Assign</p>
						@elseif(!empty($application->staff->department->pic_staff_id))
							<p>{{ $application->staff->department->pic->user->name }}</p>
						@endif
						
					</div>
				</div>
      		</div>
      </div>
</div>
<div class="panel panel-info">
	<div class="panel-heading">Application Info <strong>( {{ $application->application_status->name }} )</strong></div>
	<div class="panel-body">
  		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4"><label>Central Location</label></div>
				<div class="col-md-8">
					@if(empty($application->sub_location_id))
						Not decide
					@elseif(!empty($application->sub_location_id))
						{{ $application->sub_location->name }}, {{ $application->sub_location->central->name }} 
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label>Location Use</label></div>
				<div class="col-md-8">
					@if(empty($application->location_use))
						Not decide
					@elseif(!empty($application->location_use))
						{{ $application->location_use }}
					@endif
				</div>
			</div>
   		</div>
  		<div class="col-md-6">
  			<div class="row">
				<div class="col-md-3"><label>Purpose </label></div>
				<div class="col-md-9">
					@if(empty($application->purpose))
						Not decide
					@elseif(!empty($application->purpose))
						{{ $application->purpose }}
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-3"><label>Collect Location </label></div>
				<div class="col-md-9">
					@if(empty($application->collection_location))
						Not decide
					@elseif(!empty($application->collection_location))
						{{ $application->collection_location }}
					@endif
				</div>
			</div>
  		</div>
  		<div class="col-md-12">
  			<hr>
  			<!-- <div class="row">
				<div class="col-md-6"><label>Reservation Date </label></div>
				<div class="col-md-6">
				</div>
			</div> -->
			<div class="row">
				<div class="table-responsive">
				<table id="tbldate" align="center" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="20%"></th>
							<th width="40%" colspan="2">Date</th>
							<th width="40%">PIC Handle</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Reservation</td>
							<td colspan="2">{{ $application->request_date }}
								@foreach($configs as $config)
									@if(date('Y-m-d',strtotime($application->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($application->request_date)))
										<p><label style="color: red">( LATE REQUEST )</label></p>
									@endif
								@endforeach
							</td>
							<td>
								@foreach($application->application_logs->whereIn('application_status_id',[2,3]) as $applicationLog)
								{{ $applicationLog->staff->staff_no }} - {{ $applicationLog->staff->user->name }}
								<br>
								<strong>Action : </strong>{{ $applicationLog->application_status->name }}
								@endforeach
							</td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th width="20%"></th>
							<th width="20%">Predict</th>
							<th width="20%">Actual</th>
							<th width="40%">PIC Handle</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Pickup/Collection</td>
							<td>{{ $application->collection_date }}</td>
							<td>
								@if(empty($application->actual_collection_date))
									-
								@elseif(!empty($application->actual_collection_date))
									{{ $application->actual_collection_date }}
								@endif
							</td>
							<td>
								@foreach($application->application_logs->where('application_status_id',5) as $applicationLog)
								{{ $applicationLog->staff->staff_no }} - {{ $applicationLog->staff->user->name }}
								<br>
								<strong>Action : </strong>
									@if($applicationLog->application_status->name == "Receive")
									Hands Out
									@endif
								@endforeach
							</td>
						</tr>
						<tr>
							<td>Return</td>
							<td>{{ $application->return_date }}
							</td>
							<td>
								@if(empty($application->actual_return_date))
									-
								@elseif(!empty($application->actual_return_date))
									{{ $application->actual_return_date }}
								@endif
								@if($application->actual_return_date > $application->return_date)
									<p><label style="color: red">( OVERDUE )</label></p>
								@endif
							</td>
							<td>
								@foreach($application->application_logs->where('application_status_id',6) as $applicationLog)
								{{ $applicationLog->staff->staff_no }} - {{ $applicationLog->staff->user->name }}
								<br>
								<strong>Action : </strong>
									@if($applicationLog->application_status->name == "Returned")
									Received
									@endif
								@endforeach
							</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
  		</div>
  		<div class="col-md-12">
  			<hr>
  			<label>Item(s) : 
  				@foreach($application->application_item_type as $applicationItemType)
  					{{ $applicationItemType->item_type->name }} - {{ $applicationItemType->quantity }};
  				@endforeach
  			</label>
  			<div class="table-responsive">
  			<table id="tblItems" align="center" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th width="15%">Tag</th>
							<th width="35%">Item(s)</th>
							<th width="15%">Serial Number</th>
							<th width="15%">Type</th>
							<th width="15%">Condition</th>
						</tr>
					</thead>
					<tbody>
						@foreach($application->application_item as $applicationItem)
						@php
							$items = $applicationItem->items()->withTrashed()->first();
							$asset = $items->asset()->withTrashed()->first();
						@endphp
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $asset->tag }}</td>
							<td>{{ $asset->model->name }} - ({{ $asset->model->brand->name }})</td>
							<td>{{ $asset->serial_number }}</td>
							<td>{{ $asset->model->itemType->name }}</td>
							<td>
							@if($applicationItem->item_status_id == null)
							-
							@elseif($applicationItem->item_status->name == 'Available')
								Good
							@else
								{{ $applicationItem->item_status->name }}	
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				</div>
  		</div>
  </div>
</div>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable();
	} );
</script>
<br><br>

@stop