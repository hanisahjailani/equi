@extends('layouts.superAdmin.staff')

@section('content1')
<center><h2>Staff Details</h2></center>
&nbsp;<a href="{{ route('staff.index') }}">Setting</a> / <a href="{{ route('staff.index') }}">Staff</a> / Staff Details
<hr>
<div class="panel panel-info">
    <div class="panel-heading">Staff Info </div>
	  	<div class="panel-body">
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="row">
						<div class="col-md-4"><label>Staff No</label></div>
						<div class="col-md-8">{{ $staff->staff_no }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Name</label></div>
						<div class="col-md-8">{{ $staff->user->name }}
						</div>
					</div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="row">
						<div class="col-md-4"><label>Email</label></div>
						<div class="col-md-8">{{ $staff->user->email }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><label>Phone No</label></div>
						<div class="col-md-8">{{ $staff->phone_no }}
						</div>
					</div>
	      		</div>
	    	</div>
    	</div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Department Info</div>
	  	<div class="panel-body">
      		<div class="col-md-12">
      			<div class="row">
					<div class="col-md-3"><label>Name</label></div>
					<div class="col-md-9">{{ $staff->department->short_name }} - {{ $staff->department->name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-3"><label>Central</label></div>
					<div class="col-md-9">{{ $staff->department->central->short_name }} - {{ $staff->department->central->name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-3"><label>PIC Department</label></div>
					<div class="col-md-9">
						@if(empty($staff->department->pic_staff_id))
							<p>Not Assign</p>
						@elseif(!empty($staff->department->pic_staff_id))
							<p>{{ $staff->department->pic->user->name }}</p>
						@endif
					</div>
				</div>

    		</div>
    	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<a href="{{ route('staff.index') }}"><button id="btnBack" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>
		<br><br>
	</div>
</div>
<br>
@stop