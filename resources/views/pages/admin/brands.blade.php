@extends('layouts.admin.brand')

@section('content1')
<center><h2>New Brand</h2></center>
&nbsp;Setting / Brand /
<hr>
	@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div align="right">
		<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="far fa-plus-square"></i>&nbsp;New Brand</button>
	</div>
	<div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog">
			<div class="modal-content">
				<form id="frmItemType" method="POST" action="{{ route('adminbrands.store') }}">
					@csrf
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">New Brand</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-md-2" align="right"><label>Name <label style="color: red">*</label></label></div>
							<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
							<p style="color: red">{{ $errors->first('name') }}</p></div>
						</div>
						<br><br>
					</div>
					<div class="modal-footer">
					  <button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br>	
	<div class="table-responsive">
	<table id="tblItems" align="center" data-show-columns="true" data-show-refresh="true" class="table table-striped table-bordered table-hover" data-advanced-search="true">
		<thead>
			<tr>
				<th width="50%">Name</th>
				<th width="10%">Assets</th>
				<th width="10%">Consumables</th>
				<th width="10%">Licenses</th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($brands as $brand)
			<tr>
				<td>{{ $brand->name }}</td>
				<td>{{ $assets->where('brand_id',$brand->id)->count() }}</td>
				<td>{{ $consumable->where('brand_id',$brand->id)->count() }}</td>
				<td>{{ $licenses->where('brand_id',$brand->id)->count() }}</td>
				<td>
					<button id="btnEdit" type="button" class="btn btn-info" data-toggle="modal" data-target="#editBrand{{ $brand->id }}" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button>
					<div class="modal fade" id="editBrand{{ $brand->id }}" role="dialog">
					    <div class="modal-dialog">
							<div class="modal-content">
								<form id="frmItemType" method="POST" action="{{ route('brands.update',$brand->id) }}">
									@csrf
									@method('PATCH')
									<div class="modal-header">
									  <button type="button" class="close" data-dismiss="modal">&times;</button>
									  <h4 class="modal-title">New Brand</h4>
									</div>
									<div class="modal-body">
										<div class="form-group">
											<div class="col-md-2" align="right"><label>Name</label></div>
											<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" value="{{ $brand->name }}">
											<p style="color: red">{{ $errors->first('name') }}</p></div>
										</div>
										<br><br>
									</div>
									<div class="modal-footer">
									  <button id="btnEdit" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Update</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</td>
				<td>
					<form id="frmDelete" action="{{ route('adminbrands.destroy', $brand->id) }}" method="POST">
						@csrf
						@method('DELETE')	
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table></div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable();
	} );
</script>
@stop

