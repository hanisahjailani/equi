@extends('layouts.superAdmin.item')

@section('content1')
<script>
	$(document).ready(function() {
		$("#discard_date" ).datepicker({
			changeMonth: true,  
			changeYear:true,      
			dateFormat: "yy-mm-dd",
		});

		$("#item_status_id").change(function(){
			var optionValue = $(this).val();
			if(optionValue == "7")
			{
				$("#discard").show();
				$("#requestable").checked = false;
			}
			else
			{
				$("#discard").hide();
			}
		}).change();

		const choices = new Choices('#tag_name',{
			removeItemButton: true,
			duplicateItemsAllowed : false,
		});
		
	});
</script>
<center><h2>Edit Asset</h2></center>
&nbsp;<a href="{{ route('items.index') }}">Assets</a> / Edit Asset
<hr>
<form id="frmEdit" method="POST" action="{{ route('items.update', $asset->id) }}">
	@csrf
	@method('PATCH')
	<div class="form-group">
		<div class="col-md-3"><label>Model</label></div>
		<div class="col-md-9">
			<select id="model_id" name="model_id" class="form-control selectpicker" data-live-search="true">
				<option value="">Choose Model</option>
				@foreach($itemModels as $itemModel)
				<option value="{{ $itemModel->id }}"
					@if( $itemModel->id == old('model_id',$asset->model_id))
					selected="selected"
					@endif
					>{{ $itemModel->itemType->name }} - {{ $itemModel->brand->name }} {{ $itemModel->name }} (#{{ $itemModel->model_no}})</option>
					@endforeach
				</select>
				<p style="color: red">{{ $errors->first('model_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-3"><label>Serial Number / Service Tag <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="serial_number" name="serial_number" type="text" class="form-control" value="{{ old('serial_number',$asset->serial_number) }}">
				<p style="color: red">{{ $errors->first('serial_number') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Status <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_status_id" name="item_status_id" class="form-control">
					@foreach($itemStatuses as $itemStatuses)
					<option value="{{ $itemStatuses->id }}"
						@if($itemStatuses->id == old('item_status_id',$asset->item->item_status_id))
						selected="selected"
						@endif
						>{{ $itemStatuses->name }}</option>
						@endforeach
					</select>

					<div class="panel-group" name="discard" id="discard">
						<div class="panel panel-default">
							<div class="panel-heading"> Discard Details</div>
							<div class="panel-body">
								<div class="form-group">
									<div class=" col-md-2"><label>Date <label style="color: red">*</label></label></div>
									<div class="col-md-4"><input id="discard_date" name="discard_date" type="text" class="form-control" value="{{ old('discard_date',$asset->discard_date) }}">
										<p style="color: red">{{ $errors->first('discard_date') }}</p>
									</div>
									<div class=" col-md-2"><label>Ref. No <label style="color: red">*</label></label></div>
									<div class="col-md-4"><input id="ref_no" name="ref_no" type="text" class="form-control" value="{{ old('ref_no',$asset->ref_no) }}">
										<p style="color: red">{{ $errors->first('ref_no') }}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p style="color: red">{{ $errors->first('item_status_id') }}</p>
					<input type="checkbox" name="requestable" id="requestable" value="Yes"
					@if( $asset->requestable == "Yes")
					checked="checked"
					@endif
					> <strong>Requestable</strong>
				</div>
				<br><br><br>
			</div>
			<div class="form-group">
				<div class=" col-md-3"><label>Location Central <label style="color: red">*</label></label></div>
				<div class="col-md-9">
					<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Central</option>
						@foreach($sublocations as $sublocation)
						<option value="{{ $sublocation->id }}"
							@if( $sublocation->id == old('sub_location_id',$asset->item->sub_location_id))
							selected="selected"
							@endif
							>{{ $sublocation->name }}, {{ $sublocation->central->name }}</option>
							@endforeach
						</select>
						<p style="color: red">{{ $errors->first('sub_location_id') }}</p>

					</div>
					<br><br>
				</div>
				<div class="form-group">
					<div class="col-md-3"><label>Tag(s) </label></div>
					<div class="col-md-9">
						<input type="text" name="tag_name" id="tag_name" class="form-control" list="tagging" multiple="multiple" value=
						"
						@foreach($tags as $tag)
						{{ $tag->name }},
						@endforeach
						">
					</div>
					<br><br>
				</div>
				<div class="form-group">
					<div class=" col-md-3"><label>Tagging Name <label style="color: red">*</label></label></div>
					<div class="col-md-9"><input id="tag" name="tag" type="text" class="form-control" value="{{ old('tag',$asset->tag) }}">
						<p style="color: red">{{ $errors->first('tag') }}</p>
					</div>
					<br><br>
				</div>
				<div class="form-group">
					<div class="col-md-3"></div>
					<div class="col-md-9" style="float:right;">
						<a href="{{ route('items.index') }}"><button id="btnBack" type="button" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
						<button id="btnSave" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						<br><br>
					</div>
				</div>
			</form>
			@stop