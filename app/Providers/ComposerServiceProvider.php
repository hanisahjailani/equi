<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['layouts/admin/*','pages/admin/dashboard','layouts/superAdmin/*','pages/superAdmin/dashboard'],
            'App\Http\ViewComposers\NotiComposer'
        );

        view()->composer(
            ['layouts/admin/*','pages/admin/dashboard','layouts/superAdmin/*','pages/superAdmin/dashboard'],
            'App\Http\ViewComposers\AccessComposer'
        );
    }
}
