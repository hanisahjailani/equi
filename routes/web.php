﻿<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

// /consumable/apply

Route::group(['prefix' => 'consumable', 'middleware' => ['auth', 'checkStaff']], function () {

	Route::get('/apply', 'ApplicationConsumableController@apply')
		->name('consumable.apply');

	Route::post('/apply', 'ApplicationConsumableController@store')
		->name('consumable.apply.store');

	Route::get('/items/{consumableApplicationId}', 'ApplicationConsumableController@items')
		->name('consumable.items');

	Route::post('/items/{consumableApplication}/add-items', 'ApplicationConsumableController@addItems')
		->name('consumable.items.add-items');

	Route::post('/items/{consumableAppItem}/remove-item', 'ApplicationConsumableController@removeItem')
		->name('consumable.items.remove-item');
});

Route::get('/test', 'TestController@index')->name('test');

//--------------------------------- [ SUPER ADMIN ] -----------------------------------------------
//STAFF
// /staff/
Route::group(['prefix' => 'staff', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\StaffController@index')->name('staff.index');
	Route::get('/create','superAdmin\StaffController@create')->name('staff.create');
	Route::post('/','superAdmin\StaffController@store')->name('staff.store');
	Route::get('/{staff}','superAdmin\StaffController@show')->name('staff.show');
	Route::get('/{staff}/edit','superAdmin\StaffController@edit')->name('staff.edit');
	Route::patch('/{staff}','superAdmin\StaffController@update')->name('staff.update');
});

//ROLE ACCESS
Route::group(['prefix' => 'role', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/', 'superAdmin\AccessController@index')->name('role.index');
	Route::get('/create', 'superAdmin\AccessController@create')->name('role.create');
	Route::post('/', 'superAdmin\AccessController@store')->name('role.store');
	Route::get('/{id}/edit', 'superAdmin\AccessController@edit')->name('role.edit');
	Route::patch('/{id}', 'superAdmin\AccessController@update')->name('role.update');
	Route::get('/{id}/create', 'superAdmin\AccessController@createStaff')->name('roleStaff.create');
	Route::post('/{id}', 'superAdmin\AccessController@storeStaff')->name('roleStaff.store');
	Route::post('/superadmin/{id}', 'superAdmin\AccessController@storeSuperAdmin')->name('roleSuperAdmin.store');
	Route::delete('/staff/{id}/{staff}/{roleid}', 'superAdmin\AccessController@destroyStaff')->name('roleStaff.destroy');
	Route::delete('/superadmin/{id},{roleid}', 'superAdmin\AccessController@destroySuperAdmin')->name('roleSuperAdmin.destroy');
});

//USER
Route::group(['prefix' => 'user', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\UserController@index')->name('user.index');
	Route::patch('/{user}','superAdmin\UserController@update')->name('user.update');
});

//DEPARTMENT
Route::group(['prefix' => 'department', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\DepartmentController@index')->name('department.index');
	Route::post('/','superAdmin\DepartmentController@store')->name('department.store');
	Route::get('/{department}/edit','superAdmin\DepartmentController@edit')->name('department.edit');
	Route::patch('/{department}','superAdmin\DepartmentController@update')->name('department.update');
	Route::delete('/{department}','superAdmin\DepartmentController@destroy')->name('department.destroy');
});

//ITEM
Route::group(['prefix' => 'items', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ItemController@index')->name('items.index');
	Route::get('/requestable','superAdmin\ItemController@indexRequestable')->name('items.indexRequestable');
	Route::get('/create','superAdmin\ItemController@create')->name('items.create');
	Route::post('/','superAdmin\ItemController@store')->name('items.store');
	Route::get('/{items}/edit','superAdmin\ItemController@edit')->name('items.edit');
	Route::patch('/{items}','superAdmin\ItemController@update')->name('items.update');
	Route::get('/{items}','superAdmin\ItemController@show')->name('items.show');
	Route::delete('/{items}','superAdmin\ItemController@destroy')->name('items.destroy');
});

//ITEM TYPE
Route::group(['prefix' => 'itemtypes', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ItemTypeController@index')->name('itemtypes.index');
	Route::post('/','superAdmin\ItemTypeController@store')->name('itemtypes.store');
	Route::patch('/{id}','superAdmin\ItemTypeController@update')->name('itemtypes.update');
	Route::delete('/{itemtypes}','superAdmin\ItemTypeController@destroy')->name('itemtypes.destroy');
});

//STATUS LABEL
Route::group(['prefix' => 'statuslabels', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\StatusLabelController@index')->name('statuslabels.index');
	Route::post('/','superAdmin\StatusLabelController@store')->name('statuslabels.store');
	Route::patch('/{id}','superAdmin\StatusLabelController@update')->name('statuslabels.update');
	Route::delete('/{statuslabels}','superAdmin\StatusLabelController@destroy')->name('statuslabels.destroy');
});

//BRAND
Route::group(['prefix' => 'brands', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\BrandController@index')->name('brands.index');
	Route::post('/','superAdmin\BrandController@store')->name('brands.store');
	Route::patch('/{id}','superAdmin\BrandController@update')->name('brands.update');
	Route::delete('/{brands}','superAdmin\BrandController@destroy')->name('brands.destroy');
});

//BRANCH
Route::group(['prefix' => 'branches', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\BranchController@index')->name('branches.index');
	Route::post('/','superAdmin\BranchController@store')->name('branches.store');
	Route::post('/subLocation','superAdmin\BranchController@subLocationstore')->name('subLocation.store');
	Route::patch('/{id}','superAdmin\BranchController@update')->name('branches.update');
	Route::delete('/{branches}','superAdmin\BranchController@destroy')->name('branches.destroy');
});

//ITEM MODEL
Route::group(['prefix' => 'itemmodels', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ItemModelController@index')->name('itemmodels.index');
	Route::get('/create','superAdmin\ItemModelController@create')->name('itemmodels.create');
	Route::post('/','superAdmin\ItemModelController@store')->name('itemmodels.store');
	Route::get('/{id}/edit','superAdmin\ItemModelController@edit')->name('itemmodels.edit');
	Route::patch('/{id}','superAdmin\ItemModelController@update')->name('itemmodels.update');
	Route::delete('/{itemmodels}','superAdmin\ItemModelController@destroy')->name('itemmodels.destroy');
});

//APPLICATION
http://sisteminventory.com/application/create
Route::group(['prefix' => 'application', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ApplicationController@index')->name('application.index');
	Route::get('/create','superAdmin\ApplicationController@create')->name('application.create');
	Route::get('/{id}','superAdmin\ApplicationController@show')->name('application.show');
	Route::post('/','superAdmin\ApplicationController@store')->name('application.store');
	Route::get('/{id}/edit','superAdmin\ApplicationController@edit')->name('application.edit');
	Route::patch('/{id}','superAdmin\ApplicationController@update')->name('application.update');
});


//APPLICATION APPROVE
Route::group(['prefix' => 'applicationapprove', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/{id}','superAdmin\ApplicationApproveController@edit')->name('applicationapprove.edit');
	Route::post('/{id}/{applicationid}','superAdmin\ApplicationApproveController@store')->name('applicationapprove.store');
	Route::delete('/{id}/{applicationid}','superAdmin\ApplicationApproveController@destroy')->name('applicationapprove.destroy');
});

Route::group(['prefix' => 'applicationapprovelog', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/create/{id}','superAdmin\ApplicationApproveLogController@create')->name('applicationapprovelog.create');
	Route::post('/{id}','superAdmin\ApplicationApproveLogController@store')->name('applicationapprovelog.store');
});

//APPLICATION REJECT
Route::group(['prefix' => 'applicationreject', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/create/{id}','superAdmin\ApplicationRejectLogController@create')->name('applicationrejectlog.create');
	Route::post('/{id}','superAdmin\ApplicationRejectLogController@store')->name('applicationrejectlog.store');
});

Route::group(['prefix' => 'applicationitemtype', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/create','superAdmin\ApplicationItemTypeController@create')->name('applicationitemtype.create');
	Route::post('/','superAdmin\ApplicationItemTypeController@store')->name('applicationitemtype.store');
});

Route::group(['prefix' => 'applicationitem', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/create/{id}','superAdmin\ApplicationItemController@create')->name('applicationitem.create');
	Route::post('/{id}/{applicationid}','superAdmin\ApplicationItemController@store')->name('applicationitem.store');
	Route::delete('/{id}/{applicationid}','superAdmin\ApplicationItemController@destroy')->name('applicationitem.destroy');
});

Route::group(['prefix' => 'applicationlog', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/create/{id}','superAdmin\ApplicationLogController@create')->name('applicationlog.create');
	Route::post('/{id}','superAdmin\ApplicationLogController@store')->name('applicationlog.store');
});

//RESERVATION
Route::group(['prefix' => 'reservation', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ReservationController@index')->name('dashboard.index');
	Route::get('/{id}/edit','superAdmin\ReservationController@edit')->name('reservation.edit');
	Route::patch('/{id}','superAdmin\ReservationController@update')->name('reservation.update');
	Route::get('/{id}/cancel','superAdmin\ReservationController@cancel')->name('reservation.cancel');
	Route::get('/{id}','superAdmin\ReservationController@show')->name('reservation.show');
	Route::get('/{id}/list','superAdmin\ReservationController@showList')->name('reservation.showList');
});

//APPLICATION HISTORY
Route::group(['prefix' => 'applicationhistory', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ApplicationHistoryController@index')->name('applicationhistory.index');
	Route::get('/{id}','superAdmin\ApplicationHistoryController@show')->name('applicationhistory.show');
});

//CONFIGURTION
Route::group(['prefix' => 'configuration', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ConfigurationController@index')->name('configuration.index');
	Route::get('/{id}/edit','superAdmin\ConfigurationController@edit')->name('configuration.edit');
	Route::patch('/{id}','superAdmin\ConfigurationController@update')->name('configuration.update');
});

//CONSUMABLE
Route::group(['prefix' => 'consumable', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\ConsumableController@index')->name('consumable.index');
	Route::get('/create','superAdmin\ConsumableController@create')->name('consumable.create');
	Route::get('/{id}/create','superAdmin\ConsumableController@restockcreate')->name('consumable.restockcreate');
	Route::post('/','superAdmin\ConsumableController@store')->name('consumable.store');
	Route::post('/{id}','superAdmin\ConsumableController@restockstore')->name('consumable.restockstore');
	Route::get('/{id}/edit','superAdmin\ConsumableController@edit')->name('consumable.edit');
	Route::patch('/{id}','superAdmin\ConsumableController@update')->name('consumable.update');
	Route::get('/{id}','superAdmin\ConsumableController@show')->name('consumable.show');
	Route::delete('/{id}','superAdmin\ConsumableController@destroy')->name('consumable.destroy');
});

//LICENSE
Route::group(['prefix' => 'license', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\LicenseController@index')->name('license.index');
	Route::get('/create','superAdmin\LicenseController@create')->name('license.create');
	Route::post('/','superAdmin\LicenseController@store')->name('license.store');
	Route::get('/{id}/edit','superAdmin\LicenseController@edit')->name('license.edit');
	Route::patch('/{id}','superAdmin\LicenseController@update')->name('license.update');
	Route::get('/{id}','superAdmin\LicenseController@show')->name('license.show');
	Route::delete('/{id}','superAdmin\LicenseController@destroy')->name('license.destroy');
});

//-------------------------------------- [ STAFF ] ------------------------------------------------

//RESERVATION
Route::group(['prefix' => 'userreservation', 'middleware' => ['auth','staff']], function () {
	Route::get('/','user\ReservationController@index')->name('userreservation.index');
});

//USER
Route::group(['prefix' => 'useruser', 'middleware' => ['auth','staff']], function () {
	Route::get('/','user\UserController@index')->name('useruser.index');
	Route::patch('/{id}','user\UserController@update')->name('useruser.update');
});

//APPLICATIONS
Route::group(['prefix' => 'userapplication', 'middleware' => ['auth','staff']], function () {
	Route::get('/','user\ApplicationController@index')->name('userapplication.index');
	Route::get('/create','user\ApplicationController@create')->name('userapplication.create');
	Route::get('/{id}','user\ApplicationController@show')->name('userapplication.show');
	Route::get('/{id}/edit','user\ApplicationController@edit')->name('userapplication.edit');
	Route::patch('/{id}','user\ApplicationController@update')->name('userapplication.update');
});

//APPLICATION LOG
Route::group(['prefix' => 'userapplicationlog', 'middleware' => ['auth','staff']], function () {
	Route::get('/create/{id}','user\ApplicationLogController@create')->name('userapplicationlog.create');
	Route::post('/{id}','user\ApplicationLogController@store')->name('userapplicationlog.store');
});

//APPLICATION HISTORY
Route::group(['prefix' => 'userapplicationhistory', 'middleware' => ['auth','staff']], function () {
	Route::get('/','user\ApplicationHistoryController@index')->name('userapplicationhistory.index');
	Route::get('/{id}','user\ApplicationHistoryController@show')->name('userapplicationhistory.show');
});

//APPLICATION ITEM TYPE
Route::group(['prefix' => 'userapplicationitemtype', 'middleware' => ['auth','staff']], function () {
	Route::get('/create','user\ApplicationItemTypeController@create')->name('userapplicationitemtype.create');
	Route::post('/','user\ApplicationItemTypeController@store')->name('userapplicationitemtype.store');
});


//-------------------------------------- [ ADMIN ] ------------------------------------------------

//CONFIGURTION
Route::group(['prefix' => 'adminconfiguration', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ConfigurationController@index')->name('adminconfiguration.index');
	Route::get('/{id}/edit','admin\ConfigurationController@edit')->name('adminconfiguration.edit');
	Route::patch('/{id}','admin\ConfigurationController@update')->name('adminconfiguration.update');
});

//STATUS LABEL
Route::group(['prefix' => 'adminstatuslabels', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\StatusLabelController@index')->name('adminstatuslabels.index');
	Route::post('/','admin\StatusLabelController@store')->name('adminstatuslabels.store');
	Route::patch('/{id}','admin\StatusLabelController@update')->name('adminstatuslabels.update');
	Route::delete('/{adminstatuslabels}','admin\StatusLabelController@destroy')->name('adminstatuslabels.destroy');
});

//ITEM MODEL
Route::group(['prefix' => 'adminitemmodels', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ItemModelController@index')->name('adminitemmodels.index');
	Route::get('/create','admin\ItemModelController@create')->name('adminitemmodels.create');
	Route::post('/','admin\ItemModelController@store')->name('adminitemmodels.store');
	Route::get('/{id}/edit','admin\ItemModelController@edit')->name('adminitemmodels.edit');
	Route::patch('/{id}','admin\ItemModelController@update')->name('adminitemmodels.update');
	Route::delete('/{adminitemmodels}','admin\ItemModelController@destroy')->name('adminitemmodels.destroy');
});

//BRAND
Route::group(['prefix' => 'adminbrands', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\BrandController@index')->name('adminbrands.index');
	Route::post('/','admin\BrandController@store')->name('adminbrands.store');
	Route::patch('/{id}','admin\BrandController@update')->name('adminbrands.update');
	Route::delete('/{adminbrands}','admin\BrandController@destroy')->name('adminbrands.destroy');
});

//CONSUMABLE
Route::group(['prefix' => 'adminconsumable', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ConsumableController@index')->name('adminconsumable.index');
	Route::get('/create','admin\ConsumableController@create')->name('adminconsumable.create');
	Route::get('/{id}/create','admin\ConsumableController@restockcreate')->name('adminconsumable.restockcreate');
	Route::post('/','admin\ConsumableController@store')->name('adminconsumable.store');
	Route::post('/{id}','admin\ConsumableController@restockstore')->name('adminconsumable.restockstore');
	Route::get('/{id}/edit','admin\ConsumableController@edit')->name('adminconsumable.edit');
	Route::patch('/{id}','admin\ConsumableController@update')->name('adminconsumable.update');
	Route::get('/{id}','admin\ConsumableController@show')->name('adminconsumable.show');
	Route::delete('/{id}','admin\ConsumableController@destroy')->name('adminconsumable.destroy');
});

//LICENSE
Route::group(['prefix' => 'adminlicense', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\LicenseController@index')->name('adminlicense.index');
	Route::get('/create','admin\LicenseController@create')->name('adminlicense.create');
	Route::post('/','admin\LicenseController@store')->name('adminlicense.store');
	Route::get('/{id}/edit','admin\LicenseController@edit')->name('adminlicense.edit');
	Route::patch('/{id}','admin\LicenseController@update')->name('adminlicense.update');
	Route::get('/{id}','admin\LicenseController@show')->name('adminlicense.show');
	Route::delete('/{id}','admin\LicenseController@destroy')->name('adminlicense.destroy');
});

//RESERVATION
Route::group(['prefix' => 'adminreservation', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ReservationController@index')->name('admindashboard.index');
	Route::get('/{id}','admin\ReservationController@show')->name('adminreservation.show');
	Route::get('/{id}/list','admin\ReservationController@showList')->name('adminreservation.showList');
	Route::get('/{id}/edit','admin\ReservationController@edit')->name('adminreservation.edit');
	Route::patch('/{id}','admin\ReservationController@update')->name('adminreservation.update');
	Route::get('/{id}/cancel','admin\ReservationController@cancel')->name('adminreservation.cancel');
});

//APPLICATION
Route::group(['prefix' => 'adminapplication', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ApplicationController@index')->name('adminapplication.index');
	Route::get('/create','admin\ApplicationController@create')->name('adminapplication.create');
	Route::get('/{id}','admin\ApplicationController@show')->name('adminapplication.show');
	Route::post('/','admin\ApplicationController@store')->name('adminapplication.store');
	Route::get('/{id}/edit','admin\ApplicationController@edit')->name('adminapplication.edit');
	Route::patch('/{id}','admin\ApplicationController@update')->name('adminapplication.update');
});

//APPLICATION APPROVE
Route::group(['prefix' => 'adminapplicationapprove', 'middleware' => ['auth','moderator']], function () {
	Route::get('/{id}','admin\ApplicationApproveController@edit')->name('adminapplicationapprove.edit');
	Route::post('/{id}/{applicationid}','admin\ApplicationApproveController@store')->name('adminapplicationapprove.store');
	Route::delete('/{id}/{applicationid}','admin\ApplicationApproveController@destroy')->name('adminapplicationapprove.destroy');
});

Route::group(['prefix' => 'adminapplicationapprovelog', 'middleware' => ['auth','moderator']], function () {
	Route::get('/create/{id}','admin\ApplicationApproveLogController@create')->name('adminapplicationapprovelog.create');
	Route::post('/{id}','admin\ApplicationApproveLogController@store')->name('adminapplicationapprovelog.store');
});

//APPLICATION REJECT
Route::group(['prefix' => 'adminapplicationrejectlog', 'middleware' => ['auth','moderator']], function () {
	Route::get('/create/{id}','admin\ApplicationRejectLogController@create')->name('adminapplicationrejectlog.create');
	Route::post('/{id}','admin\ApplicationRejectLogController@store')->name('adminapplicationrejectlog.store');
});

//APPLICATION ITEM TYPE
Route::group(['prefix' => 'adminapplicationitemtype', 'middleware' => ['auth','moderator']], function () {
	Route::get('/create','admin\ApplicationItemTypeController@create')->name('adminapplicationitemtype.create');
	Route::post('/','admin\ApplicationItemTypeController@store')->name('adminapplicationitemtype.store');
});

//APPLICATION ITEM
Route::group(['prefix' => 'adminapplicationitem', 'middleware' => ['auth','moderator']], function () {
	Route::get('/create/{id}','admin\ApplicationItemController@create')->name('adminapplicationitem.create');
	Route::post('/{id}/{applicationid}','admin\ApplicationItemController@store')->name('adminapplicationitem.store');
	Route::delete('/{id}/{applicationid}','admin\ApplicationItemController@destroy')->name('adminapplicationitem.destroy');
});

//APPLICATION LOG
Route::group(['prefix' => 'adminapplicationlog', 'middleware' => ['auth','moderator']], function () {
	Route::get('/create/{id}','admin\ApplicationLogController@create')->name('adminapplicationlog.create');
	Route::post('/{id}','admin\ApplicationLogController@store')->name('adminapplicationlog.store');
});

//ITEM TYPE
Route::group(['prefix' => 'adminitemtypes', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ItemTypeController@index')->name('adminitemtypes.index');
	Route::post('/','admin\ItemTypeController@store')->name('adminitemtypes.store');
	Route::delete('/{itemtypes}','admin\ItemTypeController@destroy')->name('adminitemtypes.destroy');
});

//ITEM
Route::group(['prefix' => 'adminitems', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ItemController@index')->name('adminitems.index');
	Route::get('/requestable','admin\ItemController@indexRequestable')->name('adminitems.indexRequestable');
	Route::get('/create','admin\ItemController@create')->name('adminitems.create');
	Route::post('/','admin\ItemController@store')->name('adminitems.store');
	Route::get('/{items}/edit','admin\ItemController@edit')->name('adminitems.edit');
	Route::patch('/{items}','admin\ItemController@update')->name('adminitems.update');
	Route::get('/{items}','admin\ItemController@show')->name('adminitems.show');
	Route::delete('/{items}','admin\ItemController@destroy')->name('adminitems.destroy');
});

//APPLICATION HISTORY
Route::group(['prefix' => 'adminapplicationhistory', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ApplicationHistoryController@index')->name('adminapplicationhistory.index');
	Route::get('/{id}','admin\ApplicationHistoryController@show')->name('adminapplicationhistory.show');
});

//ITEM TYPE
Route::group(['prefix' => 'adminitemtypes', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\ItemTypeController@index')->name('adminitemtypes.index');
	Route::post('/','admin\ItemTypeController@store')->name('adminitemtypes.store');
	Route::patch('/{id}','admin\ItemTypeController@update')->name('adminitemtypes.update');
	Route::delete('/{itemtypes}','admin\ItemTypeController@destroy')->name('adminitemtypes.destroy');
});


//USER
Route::group(['prefix' => 'adminuser', 'middleware' => ['auth','moderator']], function () {
	Route::get('/','admin\UserController@index')->name('adminuser.index');
	Route::patch('/{id}','admin\UserController@update')->name('adminuser.update');
});

//Aqid
//SUPERADMIN
//STORE 
Route::group(['prefix' => 'store', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\StoreController@index')->name('store.index');
	Route::get('/create','superAdmin\StoreController@create')->name('store.create');
	Route::post('/', 'superAdmin\StoreController@store')->name('store.store');
	//Route::resource('store', 'StoreController')->middleware ('auth');
});

//STOR APPROVE HOD
Route::group(['prefix' => 'storeApproveHod', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\StoreApproveHodController@index')->name('storeApproveHod.index');
	// Route::get('/{id}', 'superAdmin\StorApproveHodController@show')->name('storApproveHod.show');
});

//STOR APPROVE BKW
Route::group(['prefix' => 'storeApproveBkw', 'middleware' => ['auth','superAdmin']], function () {
	Route::get('/','superAdmin\StoreApproveBkwController@index')->name('storeApproveBkw.index');
	Route::get('/show','superAdmin\StoreApproveBkwController@show')->name('storeApproveBkw.show');
	// Route::get('/{id}', 'superAdmin\StorApproveBkwController@show')->name('storApproveBkw.show');
});

//-------------------------------------- [ LOGIN ] ------------------------------------------------

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');