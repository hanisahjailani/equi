<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreApplicationConsumable extends Model
{

    protected $fillable = [
        'store_application_id',
        'quantity',
        'consumable_id',
    ];

    public function store_applications()
    {
        return $this->belongsTo(Store_Applications::class);
    }

    public function store_consumables()
    {
        return $this->belongsTo(Consumables::class);
    }

    public function store_application()
    {
        return $this->hasMany(Store_Applications::class);
    }
}
