@extends('layouts.user.applicationhistory')

@section('content1')
<center><h2>History Application</h2></center>
&nbsp;History Application /
<hr>
@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
@endif
<br>
<div class="table-responsive">
<table id="tblHistoryApplication" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">Booking ID</th>
			<th width="25%">Item(s)</th>
			<th width="15%">Collection Date</th>
			<th width="15%">Return Date</th>
			<th width="20%">Remark(s)</th>
			<th width="20%">Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($applicationHistory as $appHistory)
		<tr>
			<td><a href="{{ route('userapplicationhistory.show',$appHistory->id) }}">#{{ $appHistory->booking_id }}</a></td>
			<td>
				@foreach($appHistory->application_item_type as $app_item_type)
				{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
				@endforeach
			</td>
			<td>{{ $appHistory->collection_date }}</td>
			<td>{{ $appHistory->return_date }}</td>
			<td>					@foreach($appHistory->application_logs->where('application_status_id',$appHistory->application_status_id) as $remarks_app_History)
		    @if(empty($remarks_app_History->remark_to_applicant))
			Does not have remark
			@elseif(!empty($remarks_app_History->remark_to_applicant))
			{{ $remarks_app_History->remark_to_applicant }}
			@endif
		    @endforeach</td>
			<td>{{ $appHistory->application_status->name }}
				@foreach($configs as $config)
					@if(date('Y-m-d',strtotime($appHistory->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($appHistory->request_date)))
						<p><label style="color: red">( LATE REQUEST )</label></p>
					@endif
				@endforeach
				@if($appHistory->actual_return_date > $appHistory->return_date)
					<p><label style="color: red">( OVERDUE )</label></p>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblHistoryApplication').DataTable({
        "order": [[ 0, "desc" ]]
    	});
	} );
</script>					
@stop