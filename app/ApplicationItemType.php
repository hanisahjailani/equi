<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationItemType extends Model
{
    protected $guarded = [];

    public function application()
    {
    	return $this->belongsTo('App\Application','application_id');
    }

    public function item_type()
    {
    	return $this->belongsTo('App\ItemType','item_type_id');
    }
}
