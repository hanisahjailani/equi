<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consumable extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    
    public function item()
    {
    	return $this->belongsTo('App\Item','item_id');
    }

    public function itemType()
    {
    	return $this->belongsTo('App\ItemType','item_type_id');
    }

    public function brand()
    {
    	return $this->belongsTo('App\Brand','brand_id');
    }
}
