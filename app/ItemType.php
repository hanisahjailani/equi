<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $guarded = [];

    public function items()
    {
    	return $this->hasMany('App\Item');
    }
    public function application_item_type()
    {
    	return $this->hasMany('App\ApplicationItemType');
    }
    public function category()
    {
    	return $this->belongsTo('App\Category','category_id');
    }
    public function consumables()
    {
        return $this->hasMany(Consumable::class);
    }

}
