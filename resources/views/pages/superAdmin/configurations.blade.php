@extends('layouts.superAdmin.configuration')

@section('content1')
<center><h2>Configuration</h2></center>
	&nbsp;Configuration /
	<hr>
	@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	@foreach($config as $conf)
	<div class="panel panel-info">
		<div class="panel-heading"><strong>Service Level Agreement (SLA)</strong></div>
		<div class="panel-body">
			<ol>
				<li>
					Applicant should make the reservation <strong> {{ $conf->sla }} days early</strong> from the date they want to reserve
				</li>
			</ol>
		</div>
  	</div>

  	<div class="panel panel-info">
		<div class="panel-heading"><strong>Terms & Conditions</strong></div>
		<div class="panel-body">
				@if(empty($conf->terms_condition))
					<p>Does have any terms and condition yet. Please update terms and condition</p>
				@elseif(!empty($conf->terms_condition))
					{!! $conf->terms_condition !!}
				@endif

		</div>
  	</div>
  	 @endforeach
  	<a href="{{ route('configuration.edit',2) }}"><button id="btnSave" type="button" class="btn btn-primary"><i class="fas fa-edit"></i>&nbsp;Update</button></a>
	<br><br>
@stop