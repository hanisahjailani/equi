<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Application;
use App\ApplicationItemType;
use DB;
use App\ApplicationItem;
use App\ApplicationLog;

class ApplicationApproveLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $application = Application::findOrFail($id);
        $locations = Application::distinct()->get(['collection_location']);

        $applicationItemTypes = ApplicationItemType::where('application_id',$application->id)->sum('quantity');

        $applicationItems = DB::table('items')
                                ->join('application_items','items.id','=','application_items.item_id')
                                ->where('application_items.application_id',$application->id)
                                ->count();

        // dd($applicationItemTypes,$applicationItems);
        if($applicationItems == $applicationItemTypes)
        {
            $applicationitemtypes = ApplicationItemType::where('application_id',$id)->get();
            $applicationItems = ApplicationItem::where('application_id',$application->id)->get();
            // dd($applicationItems);
            return view('pages.superAdmin.applicationsCreateApproveLog',compact('application','applicationItems','applicationitemtypes','locations'));
        }
        else
        {
            return redirect()->route('applicationapprove.edit',$application->id)->with('failure', 'Choosen item(s) is not enough!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $application = Application::findOrFail($id);
        $application->application_status_id = 2;
        $application->collection_location = $request->input('collection_location');
        $application->save();

        $applicationlog = ApplicationLog::create([
            'remark' => $request->input('remark'),
            'remark_to_applicant' => $request->input('remark_to_applicant'),
            'application_id' => $application->id,
            'staff_id' => Auth::user()->staff->id,
            'application_status_id' => 2,
        ]);
        
        return redirect()->route('applicationhistory.index')->with('success', 'Successfully submit the application!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
