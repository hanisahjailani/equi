@extends('layouts.user.reservation')

@section('content1')
<center><h2>List of Reservation</h2></center>
	&nbsp;Reservation /
	<hr>
	<div class="row">
		<div class="col-md-3">
			<div class="panel-group">
			  <div class="panel panel-info">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-spinner"></i> Pending</strong></a>
			      </h4>
			    </div>
			    <div id="collapse1" class="panel-collapse collapse in">
			      <div class="panel-body">
			      	@foreach($applications->where('application_status_id',1) as $application)
			      		<div class="col-md-12 alert-warning" style="border-radius: 15px; border: 2px solid #e1b12c; color: black">
			      			<p><label>Booking ID:</label><a href="{{ route('userapplication.show', $application->id) }}">#{{ $application->booking_id }}</a></p>
			      			<p><label>Collection Date:</label>{{ $application->collection_date }}</p>
			      			@foreach($configs as $config)
								@if(date('Y-m-d',strtotime($application->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($application->request_date)))
									<p><label style="color: red">( LATE REQUEST )</label></p>
								@endif
							@endforeach
			      		</div>
			      	@endforeach
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel-group">
			  <div class="panel panel-success">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-check"></i> Approved</strong></a>
			      </h4>
			    </div>
			    <div id="collapse1" class="panel-collapse collapse in">
			      <div class="panel-body">
			      		@foreach($applications->whereIn('application_status_id',[2,7])->where('actual_collection_date','') as $application)
						  @if($application->application_status_id == 5)
				      		<div class="col-md-12 alert-warning" style="border-radius: 15px; border: 2px solid #e1b12c; color: black">
				      		@elseif($application->application_status_id == 7)
				      		<div class="col-md-12 alert-danger" style="border-radius: 15px; border: 2px solid #c23616; color: black">
							  <strong><p style="color:maroon">**Please pickup your item(s)**</p></strong>
				      		@endif
				      			<p><label>Booking ID:</label><a href="{{ route('userapplication.show', $application->id) }}">#{{ $application->booking_id }}</a></p>
				      			<p><label>Collection Date:</label>{{ $application->collection_date }}</p>
				      			<p><label>Collect Location:</label>{{ $application->collection_location }}</p>
				      		</div>
				      	@endforeach
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel-group">
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-hands"></i> Receive</strong></a>
			      </h4>
			    </div>
			    <div id="collapse1" class="panel-collapse collapse in">
			      <div class="panel-body">
			      		@foreach($applications->whereIn('application_status_id',[5,7]) as $application)
						  @if($application->actual_collection_date != null)
			      			@if($application->application_status_id == 5)
				      		<div class="col-md-12 alert-warning" style="border-radius: 15px; border: 2px solid #e1b12c; color: black">
				      		@elseif($application->application_status_id == 7)
				      		<div class="col-md-12 alert-danger" style="border-radius: 15px; border: 2px solid #c23616; color: black">
							  <strong><p style="color:maroon">**Please return the item(s)**</p></strong>
				      		@endif
				      			<p><label>Booking ID:</label><a href="{{ route('userapplication.show', $application->id) }}">#{{ $application->booking_id }}</a></p>
				      			<p><label>Return Date:</label>{{ $application->return_date }}</p>
				      		</div>
							@endif
				      	@endforeach
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel-group">
			  <div class="panel panel-info">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-exchange-alt"></i> Return</strong></a>
			      </h4>
			    </div>
			    <div id="collapse1" class="panel-collapse collapse in">
			      <div class="panel-body">
	      	      		@foreach($applications->where('application_status_id',6) as $application)
	      	      			@if(date('Y-m-d') <= date('Y-m-d',strtotime($application->actual_return_date.'+ 1 day')))
	      		      		<div class="col-md-12 alert-warning" style="border-radius: 15px; border: 2px solid #e1b12c; color: black">
	      		      			<p><label>Booking ID:</label><a href="{{ route('userapplication.show', $application->id) }}">#{{ $application->booking_id }}</a></p>
	      		      			<p><label>Actual Return Date:</label>{{ $application->actual_return_date }}</p>
	      		      			@if($application->actual_return_date > $application->return_date)
	      		      				<label style="color: red"> Late Returned</label>
	      		      			@endif
	      		      		</div>
	      		      		@endif
	      		      	@endforeach
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<br>
<br>
@stop