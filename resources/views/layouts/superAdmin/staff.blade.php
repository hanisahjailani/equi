@extends('layouts.app')

@section('content')
	<div class="col-md-3" >
		<ul class="nav nav-pills nav-stacked" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888; padding: 10px;">
			<li><a href="{{ route('dashboard.index') }}">
            	<i class="fas fa-tachometer-alt"></i>&nbsp;&nbsp;Dashboard</a></li>
            <li class="parent"><a data-toggle="collapse" href="#sub-item-4">
    			 <i class="far fa-calendar-alt"></i>&nbsp;&nbsp;Reservation
    			<span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
    			</a>
    			<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-4">
    				<li><a href="{{ route('reservation.showList',2) }}">
    					<i class="far fa-check-square" style="color: orange"></i> Pickup
    				</a></li>
    				<li><a href="{{ route('reservation.showList',5) }}">
    					<i class="far fa-check-square" style="color: green"></i> Return
    				</a></li>
    				<li><a href="{{ route('reservation.showList',7) }}">
    					<i class="far fa-check-square" style="color: red"></i> Overdue
    				</a></li>
    			</ul>
    		</li>
            <li class="parent"><a data-toggle="collapse" href="#sub-item-1">
				<i class="far fa-calendar-check">&nbsp;</i> Application
					@if($notiReservationSAdmin > 0)
					<span style="padding: 3px 8px; border-radius: 50%; background: red; color: white;">
					{{ $notiReservationSAdmin }}
					</span>
					@endif
				<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-1">
					<li><a href="{{ route('application.index') }}">
						<i class="far fa-file-alt"></i> New Application List
						@if($notiReservationSAdmin > 0)
						<span style="padding: 3px 8px; border-radius: 50%; background: red; color: white;">
						{{ $notiReservationSAdmin }}
						</span>
						@endif
					</a></li>
					<li><a class="" href="{{ route('applicationhistory.index') }}">
						<i class="fas fa-history"></i> History Application
					</a></li>
				</ul>
			</li>
            <li class="parent"><a data-toggle="collapse" href="#sub-item-3">
            	<i class="fas fa-warehouse"></i> Asset
            	<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
            	</a>
            	<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-3">
            		<li><a href="{{ route('items.index') }}">
            		    <i class="far fa-circle"></i>&nbsp;&nbsp;All Asset</a></li>
            		<li><a href="{{ route('items.indexRequestable') }}">
            		    <i class="fas fa-check"></i>&nbsp;&nbsp;Requestable Asset</a></li>
            	</ul>
            </li>
            <li><a href="{{ route('consumable.index') }}">
                <i class="fas fa-tint"></i>&nbsp;&nbsp;Consumable</a></li>
            <li><a href="{{ route('license.index') }}">
                <i class="fas fa-barcode"></i></i>&nbsp;&nbsp;License</a></li>
            <li><a href="">
                <i class="fas fa-chart-area"></i>&nbsp;&nbsp;Report</a></li>
            <li class="parent"><a data-toggle="collapse" href="#sub-item-2">
				<i class="fas fa-cog">&nbsp;</i> Setting <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-2">
					<li><a href="{{ route('statuslabels.index') }}">
						<i class="fas fa-tag"></i> Status Label
					</a></li>
					<li><a href="{{ route('itemtypes.index') }}">
						<i class="fas fa-tasks"></i> Item Type
					</a></li>
					<li><a href="{{ route('brands.index') }}">
						<i class="fas fa-industry"></i> Brand/Manufacture
					</a></li>
					<li><a href="{{ route('itemmodels.index') }}">
						<i class="fas fa-layer-group"></i> Item Model
					</a></li>
					<li><a href="{{ route('branches.index') }}">
						<i class="fas fa-code-branch"></i> Branch
					</a></li>
					<li><a href="{{ route('department.index') }}">
						<i class="fas fa-building"></i> Department
					</a></li>
					<li class="active"><a href="{{ route('staff.index') }}">
						<i class="fas fa-users-cog"></i> Staff
					</a></li>
					<li><a href="{{ route('role.index') }}">
						<i class="fas fa-universal-access"></i> Role Accessibility
					</a></li>
					<li><a href="{{ route('configuration.index') }}">
						<i class="fas fa-cogs"></i> Configuration
					</a></li>
					<li><a href="{{ route('user.index') }}">
						<i class="fas fa-user-cog"></i> User Info
					</a></li>
				</ul>
			</li>
      </ul>
	<br />
	</div>
	<div class="col-md-9" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888;">
		@yield('content1')
	</div>
@stop