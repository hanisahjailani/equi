<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	'name'=>'Super Admin',
        ]);

        DB::table('roles')->insert([
        	'name'=>'Admin BTMK',
        ]);

        DB::table('roles')->insert([
            'name'=>'Admin Kewangan',
        ]);

        DB::table('roles')->insert([
            'name'=>'Staff',
        ]);
    }
}
