@extends('layouts.superAdmin.itemModel')

@section('content1')
<center><h2>Edit Item Models</h2></center><br>
&nbsp;<a href="{{ route('itemmodels.index') }}">Setting</a> / <a href="{{ route('itemmodels.index') }}">Items Models</a> / Edit Item Models /
<hr>
	<form id="frmAdd" action="{{ route('itemmodels.update',$itemModel->id) }}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<div class=" col-md-2"><label>Model Name<label style="color: red">*</label></label></div>
			<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" 
				
				value="{{ old('name',$itemModel->name) }}" placeholder="Eg : Aspire E 14">
			<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Brand <label style="color: red">*</label></label></div>
			<div class="col-md-10">
				<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Brand</option>
					@foreach($brands as $brand)
						<option value="{{ $brand->id }}"

								@if($brand->id  == $itemModel->brand_id)
									selected="selected"
								@endif
							>{{ $brand->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('brand_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Item Type <label style="color: red">*</label></label></div>
			<div class="col-md-10">
				<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Item type</option>
					@foreach($itemTypes as $itemTypes)
						<option value="{{ $itemTypes->id }}" 
							@if($itemTypes->id  == $itemModel->item_type_id)
								selected="selected"
							@endif
							>{{ $itemTypes->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_type_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Model No.</label></div>
			<div class="col-md-10"><input id="model_no" name="model_no" type="text" class="form-control" value="{{ old('model_no',$itemModel->model_no) }}">
			<p style="color: red">{{ $errors->first('model_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<a href="{{ route('itemmodels.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnAdd" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop