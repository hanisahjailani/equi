@extends('layouts.app')

@section('content')
<div class="col-md-3" >
	<ul class="nav nav-pills nav-stacked" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888; padding: 10px;">

		<li class="active"><a href="{{ route('userreservation.index') }}">
                <i class="far fa-calendar-alt"></i>&nbsp;&nbsp;Reservation</a></li>
        <li class="parent"><a data-toggle="collapse" href="#sub-item-1">
			<i class="far fa-calendar-check">&nbsp;</i> Application <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
			</a>
			<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-1">
				<li><a href="{{ route('userapplication.index') }}">
					<i class="far fa-file-alt"></i> New Application List
				</a></li>
				<li><a class="" href="{{ route('userapplicationhistory.index') }}">
					<i class="fas fa-history"></i> History Application
				</a></li>
			</ul>	
		</li>
        <li class="parent"><a data-toggle="collapse" href="#sub-item-2">
			<i class="fas fa-cog">&nbsp;</i> Setting <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
			</a>
			<ul class="nav nav-pills nav-stacked children collapse" id="sub-item-2">

				<li><a href="{{ route('useruser.index') }}">
					<i class="fas fa-user-cog"></i> User Info
				</a></li>
			</ul>	
		</li>
  </ul>
<br />
</div>
<div class="col-md-9" style="background-color: white; border-radius: 15px; box-shadow: 7px 7px #888888;">
	@yield('content1')
</div>
@stop