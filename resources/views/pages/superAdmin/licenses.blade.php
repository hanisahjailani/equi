@extends('layouts.superAdmin.license')

@section('content1')
<center><h2>List of License</h2></center>
&nbsp;License /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="right">
	<a href="{{ route('license.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New License</button></a>
</div>
<br>
<div class="table-responsive" style="overflow-x:auto;">
	<table id="tblItems" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="15%">License</th>
				<th width="20%">Product Key</th>
				<th width="15%">Expired Date</th>
				<th width="10%">License to Email</th>
				<th width="10%">License to Name</th>
				<th width="10%">Manufacture</th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($licenses as $license)
			<tr>
				<td>{{ $license->item->name }}</td>
				<td><a href="{{ route('license.show',$license->id) }}">{{ $license->product_key }}</a></td>
				<td>{{ $license->expired_date }}</td>
				<td>{{ $license->license_email }}</td>
				<td>{{ $license->license_name }}</td>
				<td>{{ $license->brand->name}}</td>
				<td>
					<a href="{{ route('license.edit',$license->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a>
				</td>
				<td>
					<form id="frmDelete" action="{{ route('license.destroy',$license->id) }}" method="POST">
						@csrf
						@method('DELETE')	
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this license?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable({
			"order": [[ 0, "desc" ]]
		});
	} );
</script>
@stop