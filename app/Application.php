<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $guarded = [];
    public function application_item_type()
    {
    	return $this->hasMany('App\ApplicationItemtype');
    }
    public function staff()
    {
    	return $this->belongsTo('App\Staff','staff_id');
    }
    public function pick_staff()
    {
        return $this->belongsTo('App\Staff','pick_staff_id');
    }
    public function return_staff()
    {
        return $this->belongsTo('App\Staff','return_staff_id');
    }
    public function application_item()
    {
    	return $this->hasMany('App\ApplicationItem');
    }
    public function application_logs()
    {
        return $this->hasMany('App\ApplicationLog');
    }
    public function application_status()
    {
        return $this->belongsTo('App\ApplicationStatus','application_status_id');
    }
    public function sub_location()
    {
        return $this->belongsTo('App\SubLocation','sub_location_id');
    }
}
