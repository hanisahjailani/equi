<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
        	'name'=>'Acer',
        ]);

        DB::table('brands')->insert([
        	'name'=>'HP',
        ]);

        DB::table('brands')->insert([
        	'name'=>'Canon',
        ]);

        DB::table('brands')->insert([
        	'name'=>'Epson',
        ]);

        DB::table('brands')->insert([
            'name'=>'Dell',
        ]);

        DB::table('brands')->insert([
            'name'=>'Vivo',
        ]);

        DB::table('brands')->insert([
            'name'=>'Asus',
        ]);

        DB::table('brands')->insert([
            'name'=>'Oki',
        ]);

        DB::table('brands')->insert([
            'name'=>'Faber Caster',
        ]);
    }
}
