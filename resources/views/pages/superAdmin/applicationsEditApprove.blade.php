@extends('layouts.superAdmin.application')

@section('content1')
<center><h2>Approve Application</h2></center>
&nbsp;<a href="{{ route('application.index') }}">Application</a> / Approve Application /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@elseif($message = Session::get('failure'))
<div class="alert alert-danger">
	<p>{{ $message }}</p>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="panel-group" id="accordion">
		<div class="panel panel-warning">
			<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong>Available Item(s)</strong> **Click here to change item(s)</a>
			</h4>
			</div>
			<div id="collapse1" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="table-responsive">
					<table id="tblItemsType" align="center" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="55%">Items</th>
								<th width="15%">Item Type</th>
								<th width="15%">Status</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 0; $i < count($items); $i++)
							<tr>
								<td>{{ $i+1 }}</td>
								<td><strong>{{ $items[$i]->tag }}</strong> - {{ $items[$i]->item_model_name }} ({{ $items[$i]->brand_name }})</td>
								<td>{{ $items[$i]->item_type_name }}</td>
								<td>
									@if($items[$i]->item_status_name == "In-Use" || $items[$i]->item_status_name == "Booked")
										Available
									@elseif($items[$i]->item_status_name != "In-Use" && $items[$i]->item_status_name != "Booked" )
										{{ $items[$i]->item_status_name }}
									@endif

								</td>
								<td>
									<form id="frmAdd" action="{{ route('applicationapprove.store',[$items[$i]->id, $applications->id]) }}" method="POST">
										@csrf
										<button id="item_type_id" name="item_type_id" class="btn btn-success" type="submit"><i class="fas fa-check"></i> Select</button>
									</form>
								</td>
							</tr>
							@endfor
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="panel panel-success">
			<div class="panel-heading">
				<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><strong>Choosen Item(s)</strong></a>
				</h4>
			</div>
			<div id="collapse2" class="panel-collapse collapse in">
				<div class="panel-body">
					You need to choose :
					@foreach($applicationitemtypes as $applicationitemtype)
						<label>{{ $applicationitemtype->item_type->name }} - {{ $applicationitemtype->quantity }} ; </label>
					@endforeach
					<div class="table-responsive">
					<table id="tblItemsType1" align="center" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th width="65%">Items</th>
								<th width="15%">Item Type</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($applicationItems as $applicationItem)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td><strong>{{  $applicationItem->items->asset->tag }}</strong> - {{ $applicationItem->items->asset->model->name }} ({{ $applicationItem->items->asset->model->brand->name }})</td>
								<td>{{ $applicationItem->items->asset->model->itemType->name }}</td>
								<td>
									<form id="frmDelete" action="{{ route('applicationapprove.destroy',[$applicationItem->id, $applications->id]) }}" method="POST">
										@csrf
										@method('DELETE')
										<button id="item_type_id" name="item_type_id" class="btn btn-danger" onclick="return confirm('Are you sure want to deselect this item?');" type="submit"><i class="fas fa-times"></i> Deselect</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<br><br>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="col-md-12" style="float:right;">
				<a href="{{ route('applicationapprovelog.create',$applications->id) }}"><button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-chevron-right"></i>&nbsp;Next</button></a>&nbsp;
				<br><br>
			</div>
		</div>
	</div>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItemsType').DataTable();
	} );
</script>

@stop