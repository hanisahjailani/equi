@extends('layouts.superAdmin.consumable')

@section('content1')
<script>
  $(document).ready(function() {
    $( "#purchase_date" ).datepicker({
      changeMonth: true,
      changeYear:true,
      dateFormat: "yy-mm-dd",
    });
  });
</script>
<center><h2>Edit Consumable</h2></center><br>
&nbsp;<a href="{{ route('consumable.index') }}">Consumables</a> / Edit Consumable /
<hr>
	<form id="frmAdd" action="{{ route('consumable.update',$consumable->id) }}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<div class=" col-md-3"><label>Item Type <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_type_id" name="item_type_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Item Type</option>
					@foreach($itemTypes->where('category_id',2) as $itemType)
						<option value="{{ $itemType->id }}"
							@if($itemType->id == old('item_type_id',$consumable->item_type_id))
								selected="selected"
							@endif
							>{{ $itemType->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_type_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Manufacture<label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="brand_id" name="brand_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Manufacture</option>
					@foreach($brands as $brand)
						<option value="{{ $brand->id }}"
							@if($brand->id == old('brand_id',$consumable->brand_id))
								selected="selected"
							@endif
							>{{ $brand->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('brand_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Status <label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="item_status_id" name="item_status_id" class="form-control">
					@foreach($itemStatuses->whereIn('name',['Available','Reserved']) as $itemStatus)
						<option value="{{ $itemStatus->id }}"
							@if($itemStatus->id == old('item_status_id',$consumable->item->item_status_id))
								selected="selected"
							@endif
							>{{ $itemStatus->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('item_status_id') }}</p>
			<!-- <input type="checkbox" name="requestable" value="Yes"> <strong>Requestable</strong> -->
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Product No <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="product_no" name="product_no" type="text" class="form-control" value="{{ old('product_no',$consumable->product_no) }}">
			<p style="color: red">{{ $errors->first('product_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Consumable Name <label style="color: red">*</label></label></div>
			<div class="col-md-9"><input id="name" name="name" type="text" class="form-control" value="{{ old('name',$consumable->item->name) }}">
			<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"><label>Location Central<label style="color: red">*</label></label></div>
			<div class="col-md-9">
				<select id="sub_location_id" name="sub_location_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Central</option>
					@foreach($sublocations as $sublocation)
						<option value="{{ $sublocation->id }}"
							@if($sublocation->id == old('sub_location_id',$consumable->item->sub_location_id))
								selected="selected"
							@endif
							>{{ $sublocation->name }}, {{ $sublocation->central->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('sub_location_id') }}</p>

			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-3"></div>
			<div class="col-md-9" style="float:right;">
				<a href="{{ route('consumable.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp;
				<button id="btnSave" name="btnSave" value="addConsumable" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>

@stop