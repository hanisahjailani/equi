@extends('layouts.admin.application')

@section('content1')
<center><h2>List of Application</h2></center>
&nbsp;Application /
<hr>
<div align="right">
	<a href="{{ route('adminapplication.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Application</button></a>
</div>
<br>
<div class="table-responsive">
<table id="tblApplication" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			
			<th width="5%">Booking ID</th>
			<th width="25%">Name</th>
			<th width="15%">Collection Date</th>
			<th width="15%">Return Date</th>
			<th width="20%">Item(s)</th>
			<th width="10%">Action</th>
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody>	
		@foreach($applications as $application)
		<tr>
			<td><a href="{{ route('adminapplication.show',$application->id) }}">#{{ $application->booking_id }}</a>
      			@foreach($configs as $config)
					@if(date('Y-m-d',strtotime($application->collection_date.'- '.$config->sla.' days')) <= date('Y-m-d',strtotime($application->request_date)))
						<p><label style="color: red">( LATE REQUEST )</label></p>
					@endif
				@endforeach
			</td>
			<td>{{ $application->staff->staff_no }} ( {{ $application->staff->user->name }} )</td>
			<td>{{ $application->collection_date }}</td>
			<td>{{ $application->return_date }}</td>
			<td>
				@foreach($application->application_item_type as $app_item_type)
					{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}
					<br>
				@endforeach
			</td>
			<td>
				<a href="{{ route('adminapplicationapprove.edit',$application->id) }}"><button id="btnEdit" type="button" class="btn btn-success"><i class="fas fa-check-circle"></i>&nbsp;Accept</button></a><br>
			</td>
			<td>
				<a href="{{ route('adminapplicationrejectlog.create',$application->id) }}"><button id="btnDelete" type="button" class="btn btn-danger" onclick="return confirm('Are you sure want to reject this application?');"><i class="fas fa-times-circle"></i>&nbsp;Reject</button></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
	<script>
		$(document).ready( function () {
			$('#tblApplication').DataTable();
		} );
	</script>
@stop