@extends('layouts.admin.reservationOverdue')

@section('content1')
<center><h2>List of Reservation (TODAY)</h2></center>
	&nbsp;Reservation / Overdue Item
	<hr>
	<div class="alert alert-info">
		<center><strong>Overdue Item</strong></center>
	</div>
	<div class="table-responsive">
	<table id="tblReturn" align="center" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">Booking No</th>
			<th width="25%">Name</th>
			<th width="20%">Item(s)</th>
			<th width="25%">Remark(s)</th>	
			<th width="10%">Status</th>							
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($overdue as $overdueDate)
			<tr>
				<td><a href="{{ route('adminreservation.show',$overdueDate->id) }}">#{{ $overdueDate->booking_id }}</a></td>
				<td>{{ $overdueDate->staff->staff_no }} - {{ $overdueDate->staff->user->name }}</td>
				<td>
					@foreach($overdueDate->application_item_type as $app_item_type)
					{{ $app_item_type->item_type->name }} - {{ $app_item_type->quantity }}<br>
					@endforeach
				</td>
				<td>
					@foreach($overdueDate->application_logs->where('application_status_id',$overdueDate->application_status_id) as $remarks_return_date)
		                    @if(empty($remarks_return_date->remark))
								Does not have remark
							@elseif(!empty($remarks_return_date->remark))
								{{ $remarks_return_date->remark }}
							@endif
		            @endforeach
				</td>
				<td>
					{{ $overdueDate->application_status->name }}
				</td>
				<td>
					@if($overdueDate->actual_collection_date == null)
						<a href="{{ route('adminreservation.edit',$overdueDate->id) }}"><button id="btnEdit" type="button" class="btn btn-success"><i class="fas fa-check-circle"></i>&nbsp;Pickup</button></a><br><br>
						<a href="{{ route('adminreservation.cancel',$overdueDate->id) }}"><button id="btnCancel" type="button" class="btn btn-danger" onclick="return confirm('Are you sure want to cancel this application?');"><i class="fas fa-window-close"></i>&nbsp;Cancel</button></a><br>
					@else
						<a href="{{ route('adminreservation.edit',$overdueDate->id) }}"><button id="btnEdit" type="button" class="btn btn-success"><i class="fas fa-exchange-alt"></i>&nbsp;Return</button></a><br>
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
</div>
<br>
	<script>
		$(document).ready( function () {
			$('#tblReturn').DataTable();
		} );
	</script>
@stop