<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.flexdatalist.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.flexdatalist.min.css')}}">
		<script src="{{URL::asset('js/jquery.flexdatalist.js')}}"></script>
		<script src="{{URL::asset('js/jquery.flexdatalist.min.js')}}"></script>

<!-- 
		<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/jquery-flexdatalist/2.2.4/jquery.flexdatalist.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.2.4/jquery.flexdatalist.css"> -->

	<!-- 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.2.4/jquery.flexdatalist.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-flexdatalist/2.2.4/jquery.flexdatalist.js"></script> -->

		<script type="text/javascript">
			$('#tag_name').flexdatalist({
			     minLength: 1
			});
		</script>
    </head>
    <body>
        <h2>TEST DATA</h2>
        <div class="form-group">
			<div class="col-md-3"><label>Tag(s) </label></div>
			<div class="col-md-9">
				<input type="text" name="tag_name" id="tag_name" class="flexdatalist" list="tag" data-min-length="1" multiple="multiple">
				<datalist id="tag">
					<option value="PHP">PHP</option>
				    <option value="JavaScript">JavaScript</option>
				    <option value="Cobol">Cobol</option>
				</datalist>
			</div>
			<br><br>
		</div>
    </body>
</html>
