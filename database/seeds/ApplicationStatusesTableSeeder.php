<?php

use Illuminate\Database\Seeder;

class ApplicationStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('application_statuses')->insert([
        	'name'=>'New',
        	'description'=>'New Application',
        ]);

        DB::table('application_statuses')->insert([
        	'name'=>'Approved',
        	'description'=>'The application has been approved by admin',
        ]);

        DB::table('application_statuses')->insert([
        	'name'=>'Rejected',
        	'description'=>'The application has been rejected by admin',
        ]);
        
        DB::table('application_statuses')->insert([
        	'name'=>'Cancelled',
        	'description'=>'The application has been cancelled by applicant',
        ]);

        DB::table('application_statuses')->insert([
            'name'=>'Receive',
            'description'=>'Applicant has been received the item(s)',
        ]);

        DB::table('application_statuses')->insert([
            'name'=>'Returned',
            'description'=>'Applicant has been returned the item(s)',
        ]);

        DB::table('application_statuses')->insert([
            'name'=>'Overdue',
            'description'=>'The item(s) not returned according the specified date',
        ]);
    }
}
