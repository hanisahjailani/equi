<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Staff;
use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\ApplicationLog;
use App\ItemType;
use App\App;
use App\AdminCentral;
use App\SubLocation;
use App\Configuration;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();  
        $applications = Application::where('application_status_id',1)->whereIn('sub_location_id',$sub_locations)->get();
        $configs = Configuration::all();
        return view('pages.admin.applications',compact('applications','configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemtype = ItemType::where('category_id',1)->get();
        $centrals = SubLocation::all();
        $applications = Application::all();
        return view('pages.admin.applicationsCreate',compact('itemtype','centrals','applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::findOrFail($id);
        $configs = Configuration::all();
        return view('pages.admin.applicationsShow',compact('application','configs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicant = Staff::all();
        $application = Application::findOrFail($id);
        return view('pages.admin.applicationsEdit',compact('applicant','application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'staff_id' => 'required',
        ]);

        $application = Application::findOrFail($id);
        $application->staff_id = $request->input('staff_id');
        $application->save();

        return redirect()->route('adminapplicationitem.create',$application->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
