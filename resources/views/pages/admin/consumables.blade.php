@extends('layouts.admin.consumable')

@section('content1')
<center><h2>List of Consumable</h2></center>
&nbsp;Consumable /
<hr>
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div align="right">
	<a href="{{ route('adminconsumable.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Consumable</button></a>
</div>
<br>
<div class="table-responsive">
	<table id="tblItems" align="center" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="10%">Product No</th>
				<th width="30%">Name</th>
				<th width="15%">Item Type</th>
				<th width="15%">Type</th>
				<th width="5%">Total</th>
				<th width="5%">Balance</th>
				<th width="25%">Location</th>
				<th width="5%"></th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($consumables as $consumable)
			<tr>
				<td><a href="{{ route('adminconsumable.show',$consumable->id) }}" >{{ $consumable->product_no }}</a></td>
				<td>{{ $consumable->item->name }}</td>
				<td>{{ $consumable->itemType->name }}</td>
				<td>{{ $consumable->total }}</td>
				<td>{{ $consumable->balance }}</td>
				<td>{{ $consumable->item->sub_location->name }}, {{ $consumable->item->sub_location->central->name }}</td>
				<td>
					<a href="{{ route('adminconsumable.restockcreate',$consumable->id) }}"><button id="btnEdit" type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Restock"><i class="fas fa-sync-alt"></i></button></a>
				</td>
				<td>
					<a href="{{ route('adminconsumable.edit',$consumable->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a>
				</td>
				<td>
					<form id="frmDelete" action="{{ route('adminconsumable.destroy',$consumable->id) }}" method="POST">
						@csrf
						@method('DELETE')
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable({
			"order": [[ 0, "desc" ]]
		});
	} );
</script>
@stop