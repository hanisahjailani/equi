<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // - id
        // - request_date
        // - collect_date
        // - application_status_id
        // - staff_id
        // - pickup_staff_id
        // - handover_staff_id

        Schema::create('consumable_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('request_date');
            $table->dateTime('collect_date')->nullable();
            
            $table->unsignedInteger('application_status_id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('pickup_staff_id')->nullable();
            $table->unsignedInteger('handover_staff_id')->nullable();
            
            $table->foreign('application_status_id')->references('id')->on('application_statuses');
            $table->foreign('staff_id')->references('id')->on('staff');
            $table->foreign('pickup_staff_id')->references('id')->on('staff');
            $table->foreign('handover_staff_id')->references('id')->on('staff');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_applications');
    }
}
