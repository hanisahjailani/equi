@extends('layouts.superAdmin.staff')

@section('content1')
<center><h2>Add Staff</h2></center>
&nbsp;<a href="{{ route('staff.index') }}">Setting</a> / <a href="{{ route('staff.index') }}">Staff</a> / Add Staff
<hr>
	<form id="frmEdit" method="POST" action="{{ route('staff.store') }}">
		@csrf	
		<div class="form-group">
			<div class=" col-md-2"><label>Department <label style="color:red">*</label></label></div>
			<div class="col-md-6">
				<select id="department_id" name="department_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Choose Department</option>
					@foreach($department as $department)
						<option value="{{ $department->id }}"
							@if($department->id == old('department_id',$deptPick))
								selected="selected"
							@endif
							><strong>({{ $department->central->short_name }})</strong> {{ $department->short_name }} - {{ $department->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('department_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="" data-toggle="modal" data-target="#modalDept"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Department</a>
				<p style="font-size: small; color: red">**If the department is not in the list**</p>
			</div>
			<div class="modal fade" id="modalDept" role="dialog">
			    <div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">New Department</h4>
						</div>
						<div class="modal-body">
						<div class="row">
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Short Name <label style="color: red">*</label></label></div>
								<div class="col-md-9"><input id="shortNameDept" name="shortNameDept" type="text" class="form-control" value="{{ old('shortNameDept') }}">
								<p style="color: red">{{ $errors->first('shortNameDept') }}</p></div>
							</div>
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Name <label style="color: red">*</label></label></div>
								<div class="col-md-9"><input id="nameDept" name="nameDept" type="text" class="form-control" value="{{ old('nameDept') }}">
								<p style="color: red">{{ $errors->first('nameDept') }}</p></div>
							</div>
							<div class="form-group">
								<div class="col-md-3" align="right"><label>Central <label style="color: red">*</label></label></div>
								<div class="col-md-9">
									<select id="centralDept" name="centralDept" class="form-control selectpicker" data-lived-search="true">
										<option value="">Choose Central</option>
										@foreach($centrals as $central)
											<option value="{{ $central->id }}" 
												>({{ $central->short_name }}) {{ $central->name }}</option>
										@endforeach
									</select>
								<p style="color: red">{{ $errors->first('centralDept') }}</p></div>
							</div>
							</div>
						</div>
						<div class="modal-footer">
						  <button id="btnSave" name="btnSave" type="submit" value="addDepartment" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>	
		<div class="form-group">
			<div class="col-md-2"><label>Staff No <label style="color:red">*</label></label></div>
			<div class="col-md-10"><input id="staff_no" name="staff_no" type="text" class="form-control" value="{{ old('staff_no') }}">
			<p style="color: red">{{ $errors->first('staff_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Name <label style="color:red">*</label></label></div>
			<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
				<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>IC No <label style="color:red">*</label></label></div>
			<div class="col-md-10"><input id="no_ic" name="no_ic" type="text" class="form-control" value="{{ old('no_ic') }}" placeholder="Insert without (-)          **will act as password for first time login**">
				<p style="color: red">{{ $errors->first('no_ic') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Email <label style="color:red">*</label></label></div>
			<div class="col-md-10"><input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}">
			<p style="color: red">{{ $errors->first('email') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Phone No <label style="color:red">*</label></label></div>
			<div class="col-md-10"><input id="phone_no" name="phone_no" type="text" class="form-control" value="{{ old('phone_no') }}">
				<p style="color: red">{{ $errors->first('phone_no') }}</p>
			</div>
			<br><br>
		</div>
		<!-- <div class="form-group">
			<div class=" col-md-2"><label>Role <label style="color:red">*</label></label></div>
			<div class="col-md-10">
				<select id="role" name="role" type="text" class="form-control">
					<option value="">Choose Role</option>
					<option value="Super Admin">Super Admin</option>
					<option value="Admin">Admin / Moderator</option>
					<option value="Staff">Staff</option>
				</select>
			<p style="color: red">{{ $errors->first('role') }}</p>
			</div>
			<br><br>
		</div> -->
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<a href="{{ route('staff.index') }}"><button id="btnBack" type="button" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnSave" name="btnSave" type="submit" class="btn btn-success" value="addStaff"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>
@stop