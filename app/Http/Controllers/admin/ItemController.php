<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Asset;
use App\ItemType;
use App\ItemStatus;
use App\User;
use App\Central;
use App\AdminCentral;
use App\SubLocation;
use App\Brand;
use App\Item;
use App\ItemModel;
use App\Application;
use App\ApplicationItem;
use App\ItemTag;
use DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();
        $items = Item::whereIn('sub_location_id',$sub_locations)->select('id')->get();
        $assets = Asset::whereIn('item_id',$items)->get();
        return view('pages.admin.items',compact('assets'));
    }

    public function indexRequestable()
    {
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get();
        $items = Item::whereIn('sub_location_id',$sub_locations)->select('id')->get();
        $assets = Asset::where('requestable','Yes')->whereIn('item_id',$items)->get();
        $app = Application::where('application_status_id',5)->select('id')->get();
        return view('pages.admin.itemsRequestable',compact('assets','app'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $itemStatuses = ItemStatus::all();
        $sublocations = SubLocation::all();
        $itemModels = ItemModel::all();
        $brands = Brand::all();
        $itemTypes = ItemType::all();
        $itemTags = ItemTag::distinct()->get(['name']);
        // dd($itemtags);
        $modelPick = $request->session()->get('modelPick');
        return view('pages.admin.itemsCreate', compact('itemStatuses','sublocations','itemModels','modelPick','brands','itemTypes','itemTags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('btnSave') == 'addModel')
        {
            $this->validate($request, [
                'nameModel' => 'required',
                'brand_id' => 'required',
                'item_type_id' => 'required',
            ]);
            // dd($request->input('name'));
            $model = ItemModel::create([
                'name' => $request->input('nameModel'),
                'model_no' => $request->input('model_no'),
                'brand_id' => $request->input('brand_id'),
                'item_type_id' => $request->input('item_type_id'),
            ]);
            $request->session()->put('modelPick',$model->id);
            return redirect()->route('adminitems.create')->withInput();
        }
        elseif($request->input('btnSave') == 'addAsset')
        {
            $this->validate($request, [
                'serial_number' => 'required',
                'model_id' => 'required',
                'item_status_id' => 'required',
                'sub_location_id'=> 'required',
            ]);
            // dd($request->input('requestable'));
            $item = Item::create([
                'sub_location_id' => $request->input('sub_location_id'),
                'staff_id' => Auth::user()->staff->id,
                'item_status_id' => $request->input('item_status_id'),

            ]);

            //validation for requestable items
            if($request->input('requestable') == 'Yes')
            {
                $req = "Yes";
            }
            else
            {
                 $req = "No";
            }

            //validation for tag number
            if($request->input('manual_tag') == 'Yes')
            {
               $this->validate($request, [
                    'tag' => 'required|unique:assets',
                ]);
               $tag = strtoupper($request->input('tag'));
            }
            else
            {
                $location = SubLocation::find($request->input('sub_location_id'));
                $type = ItemModel::find($request->input('model_id'));
                $count = ItemModel::where('item_type_id',$type->item_type_id)->count();
                $cut = str_replace(" / Notebook", "", $type->itemType->name);
                $tag = $location->central->short_name.'_'.strtoupper($cut).'_'.($count+1);
                // dd($tag);
            }

            //insert to asset
            $asset = Asset::create([
                'tag' => $tag,
                'serial_number' => $request->input('serial_number'),
                'requestable' => $req,
                'model_id' => $request->input('model_id'),
                'item_id' => $item->id,
            ]);

            //tag name
            $tags = explode(',', $request->input('tag_name'));
            foreach($tags as $tag)
            {
                if($tag != "")
                {
                    $itemTag = ItemTag::create([
                    'name' => ucfirst($tag),
                    'asset_id' => $asset->id,
                    ]);
                }
            }

            // // dd($explode,implode(",", $explode));
            $request->session()->forget(['modelPick']);
            return redirect()->route('adminitems.index')->with('success', 'Data Inserted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Asset::findOrFail($id);

        return view('pages.admin.itemsShow', compact('asset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemStatuses = ItemStatus::all();
        $itemTypes = ItemType::all();
        $sublocations = SubLocation::all();
        $asset = Asset::findOrFail($id);
        $itemModels = ItemModel::all();
        $itemTags = ItemTag::distinct()->get(['name']);
        $tags = ItemTag::where('asset_id',$id)->select('name')->get();
        // $tags = DB::table('item_tags')
        //         ->where('asset_id',$id)
        //         ->select('name')
        //         ->get();
        // foreach ($tags as $tag) {
            // $teges = array($tags);
            // dd(implode(',',$teges));
        // }
        return view('pages.admin.itemsEdit', compact('asset','itemStatuses','itemTypes','sublocations','itemModels','itemTags','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('item_status_id') == '7')
        {
            $this->validate($request, [
                'discard_date' => 'required',
                'ref_no' => 'required',
            ]);
        }

       $this->validate($request, [
            'tag' => 'required|unique:assets,tag,'.$id,
            'serial_number' => 'required',
            'model_id' => 'required',
            'item_status_id' => 'required',
            'sub_location_id'=> 'required',
        ]);

        if($request->input('requestable') == 'Yes')
        {
            $req = "Yes";
        }
        else
        {
             $req = "No";
        }

        $asset = Asset::find($id);
        $asset->tag = $request->input('tag');
        $asset->serial_number = $request->input('serial_number');
        $asset->requestable = $req;
        $asset->model_id = $request->input('model_id');
        $asset->discard_date = $request->input('discard_date');
        $asset->ref_no = $request->input('ref_no');
        $asset->save();

        $item = Item::find($asset->item_id);
        $item->sub_location_id = $request->input('sub_location_id');
        $item->item_status_id = $request->input('item_status_id');
        $item->save();

        $tags = explode(',', $request->input('tag_name'));
        // dd($request->input('tag_name'));
        $tagChecks = ItemTag::where('asset_id',$id)->whereNotIn('name',$tags)->get();

        foreach ($tagChecks as $tagCheck)
        {
           $tagCheck->delete();
        }

        foreach($tags as $tag)
        {
            if($tag != "")
            {
                $itemTags = ItemTag::where('asset_id',$id)->where('name',$tag)->get();

                if($itemTags->isEmpty())
                {
                    // dd(ucfirst($tag));
                    $tagIns = ItemTag::create([
                    'name' => ucfirst($tag),
                    'asset_id' => $asset->id,
                    ]);
                }
            }
        }
        return redirect()->route('adminitems.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::find($id);
        $asset->delete();

        $item = Item::destroy($asset->item_id);
        return redirect()->route('adminitems.index')->with('success', 'Data Successfully Removed!');
    }
}
