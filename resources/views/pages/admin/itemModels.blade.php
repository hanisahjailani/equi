@extends('layouts.admin.itemModel')

@section('content1')
<center><h2>Item Models</h2></center>
&nbsp;Setting / Item Models /
<hr>
	@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div align="right">
		<a href="{{ route('adminitemmodels.create') }}"><button class="btn btn-primary"><i class="far fa-plus-square"></i>&nbsp;New Item Model</button></a>
	</div>
	<br>
	<div class="table-responsive">
	<table id="tblItems" align="center" data-show-columns="true" data-show-refresh="true" class="table table-striped table-bordered table-hover" data-advanced-search="true">
		<thead>
			<tr>
				<th width="40%">Name</th>
				<th width="15%">Brand</th>
				<th width="15%">Model No.</th>
				<th width="5%">Qty</th>
				<th width="15%">Item Type</th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($models as $model)
			<tr>
				<td>{{ $model->name }}</td>
				<td>{{ $model->brand->name }}</td>
				<td>{{ $model->model_no }}</td>
				<td>{{ $assets->where('model_id',$model->id)->count() }}</td>
				<td>{{ $model->itemType->name }}</td>
				<td>
					<a href="{{ route('adminitemmodels.edit',$model->id) }}"><button id="btnEdit" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button></a>
				</td>
				<td>
					<form id="frmDelete" action="" method="POST">
						@csrf
						@method('DELETE')	
						<button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table></div>
<br>
<script>
	$(document).ready( function () {
		$('#tblItems').DataTable();
	} );
</script>
@stop

