<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemStatusIdToApplicationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_items', function (Blueprint $table) {
            $table->integer('item_status_id')->unsigned()->nullable();
            $table->foreign('item_status_id')->references('id')->on('item_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_items', function (Blueprint $table) {
            $table->dropColumn('item_status_id');
        });
    }
}
