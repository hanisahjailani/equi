<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumableApplication extends Model
{
    protected $guarded = [];

    public function consumables()
    {
        return $this->belongsToMany('App\Consumable', 'consumable_app_items');
    }

    public function consumable_app_items()
    {
        return $this->hasMany('App\ConsumableAppItem');
    }
}
