@extends('layouts.superAdmin.application')

@section('content1')
halaman show, detail maklumat setiap permohonan </br>
Akan papar semua item yg dimohon oleh nadhirah </br>
once click edit, keluar list macam ni juga, tapi text box quantity receive boleh edit

<script>
  $(document).ready(function() {
    $( "#collection_date" ).datepicker({
      changeMonth: true,  
      changeYear:true,      
      minDate:0,
      dateFormat: "yy-mm-dd",
      onSelect: function( selectedDate ) {
        $( "#return_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });

</script>


<center><h2>Show Approve Bkw detail</h2></center>
&nbsp;Store Approve (Finance) /
<hr>

<div align="right">
    <a href="{{ route('store.create') }}"><button class="btn btn-primary"><i class="far fa-edit"></i>&nbsp; Edit</button></a>
</div>

<br>
<form method="POST" action="{{ route('store.store') }}">
    @csrf

<div class="table-responsive">
<table id="tblStoreApproveBkwShow" align="center" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="20%">Item Name</th>
            <th width="10%">Quantity Request</th>
            <th width="10%">Quantity Approve</th>
            <th width="10%">Quantity Receive</th>
            <th width="10%">Price Per Unit (RM)</th>
            <th width="10%">Total Price</th>
            <th width="20%">Remarks</th>
        </tr>
    </thead>

    <tbody>         
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

<div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group">
                
               <label for="sel1">Date Approval:</label> 
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>
</div>

<form action="">
    <div class="form-group">
      <label for="sel1">Finance Approval:</label>
      <select class="form-control" id="sel1" name="sellist1">
        <option>Approve</option>
        <option>Not Approve</option>
      </select>

</form>
@stop