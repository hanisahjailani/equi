<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $guarded = [];

    public function department()
    {
    	return $this->belongsTo('App\Department','department_id');
    }
    public function pic_department()
    {
    	return $this->hasMany('App\Department');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function adminCentral()
    {
        return $this->hasMany('App\AdminCentral');
    }
    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }

    public function central()
    {
        return $this->belongsTo('App\Central', 'central_id');
    }
}
