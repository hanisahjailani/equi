<?php

use Illuminate\Database\Seeder;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            'id'=>'1',
            'sla'=>'3',
            'terms_condition'=>'<ol><li>Each user is responsible for the damage or loss of the item(s) that has been borrowed and under its control.</li><li>For LCD Projector, for installations requiring technical assistance should be notified before the date of use.</li><li>ICT equipment provided is limited and any lending of such ICT equipment will be given priority to applicants who make prior orders and according to work requirements.</li><li>&#39;Skim Pelajar Bekerja&#39; are <strong>not allowed</strong>&nbsp;to make ICT equipment reservation.</li></ol>',
        ]);
    }
}
