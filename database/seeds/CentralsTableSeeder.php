<?php

use Illuminate\Database\Seeder;

class CentralsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('centrals')->insert([
            'id'=>'1',
            'short_name'=>'JB',
            'name'=>'Johor Bahru',
        ]);

        DB::table('centrals')->insert([
            'id'=>'2',
            'short_name'=>'KL',
            'name'=>'Kuala Lumpur',
        ]);

    }
}
