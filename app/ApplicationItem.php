<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationItem extends Model
{
    protected $guarded = [];

    public function application()
    {
    	return $this->belongsTo('App\Application','application_id');
    }
    public function items()
    {
    	return $this->belongsTo('App\Item','item_id');
    }
    public function item_status()
    {
        return $this->belongsTo('App\ItemStatus','item_status_id');
    }
    
}
