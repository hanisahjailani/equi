@extends('layouts.superAdmin.application')

@section('content1')
<center><h2>Reject Application</h2></center>
&nbsp;<a href="{{ route('application.index') }}">Application</a> / Reject Application /
<hr>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="col-md-5"><label>Collection Date </label></div>
			<div class="col-md-7">{{ $application->collection_date }}</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-5"><label>Return Date </label></div>
			<div class="col-md-7">{{ $application->return_date }}</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-5"><label>Applicant </label></div>
			<div class="col-md-7">{{ $application->staff->staff_no }} - {{ $application->staff->user->name }}</div>
			<br><br>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<div class="col-md-4"><label>Location Use</label></div>
			<div class="col-md-8">
				@if(empty($application->location_use))
				Not decide
				@elseif(!empty($application->location_use))
				{{ $application->location_use }}
				@endif
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-4"><label>Purpose </label></div>
			<div class="col-md-8">
				@if(empty($application->purpose))
				Not decide
				@elseif(!empty($application->purpose))
				{{ $application->purpose }}
				@endif
			</div>
			<br><br>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		Chosen Item(s) :
			@foreach($applicationitemtypes as $applicationitemtype)
				<label>{{ $applicationitemtype->item_type->name }} - {{ $applicationitemtype->quantity }} ; </label>
			@endforeach
		<div class="table-responsive">
		<table id="tblItemsType1" align="center" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th width="10%">No</th>
					<th width="65%">Items</th>
					<th width="25%">Item Type</th>
				</tr>
			</thead>
			<tbody>
				@foreach($applicationItems as $applicationItem)
				<tr>
					<td>{{ $loop->iteration }}</td>
					<td><strong>{{  $applicationItem->items->asset->tag }}</strong> - {{ $applicationItem->items->asset->model->name }} ({{ $applicationItem->items->asset->model->brand->name }})</td>
					<td>{{ $applicationItem->items->asset->model->itemType->name }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<hr>
<form  action="{{ route('applicationrejectlog.store',$application->id) }}" method="POST">
	@csrf
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-2"><label>Reason <label style="color: red;">*</label></label></div>
				<div class="col-md-10"><textarea name="remark_to_applicant" id="remark_to_application" class="form-control" value="{{ old('remark_to_applicant') }}"></textarea>
					<p style="color: red">{{ $errors->first('remark_to_applicant') }}</p>
				</div>
				<br><br>
			</div>
		</div>
		<br><br>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">

				<div class="col-md-12" style="float:right;">
					<!-- <a href="reservationitemtype.php"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a> -->
					<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-calendar-check"></i>&nbsp;Confirm</button>&nbsp;
					<br><br>
				</div>
			</div>
		</div>
	</div>
</form>
<br>
<script>
	$(document).ready( function () {
		$('#tblItemsType').DataTable();
	} );
</script>
@stop