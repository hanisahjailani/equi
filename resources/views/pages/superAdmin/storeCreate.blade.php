@extends('layouts.superAdmin.application')

@section('content1')
page untuk pemohon add peralatan stor

<center><h2>New Stor Application</h2></center>

&nbsp;<a href="{{ route('store.index') }}">Store</a> / Create New Stor /
<hr>
<form method="POST" action="{{ route('store.store') }}">
    @csrf
    
<div class="table-responsive">
<table id="tblStorCreate" align="center" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="20%">Item</th>
      <th width="15%">Quantity</th>
      <th width="20%">Action</th>
    </tr>
  </thead>

<tbody>
  <tr>
    <td>1</td>
    <td>
      <div>
        <select id="consumable_id" class="form-control @error('consumable_id') is-invalid @enderror" name="consumable_id">
          @foreach ($senaraiItem as $item)
          <optgroup label="{{ $item->name }}">
            @foreach ($item->consumables as $consumable)
              <option value="{{ $consumable->id }}">
                {{ $consumable->item->name }}
              </option>
            @endforeach
          </optgroup>
          @endforeach
        </select>
      </div>
    </td>

    <td><input type="number" name="quantity" class="form-control">
    </td>

    <td>
      <form id="frmDelete" action="" method="POST">
        @csrf
        @method('DELETE') 
          <button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this item?');" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
      </form>
    </td>
  </tr>
</tbody>
</table>

  <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
         <button type="submit" class="btn btn-primary">
            {{ __('Tambah Data') }}
          </button>
          <a href="/store" class="btn btn-secondary">Back</a>
          <button type="submit" class="btn btn-primary">
            {{ __('Submit') }}
          </button>
      </div>
  </div>
  </form>

@stop