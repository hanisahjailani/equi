@extends('layouts.superAdmin.department')

@section('content1')
<center><h2>Edit Department Detail</h2></center>
&nbsp;<a href="{{ route('department.index') }}">Setting</a> / <a href="{{ route('department.index') }}">Department</a> / Edit Department Details
<hr>
	<form id="frmEdit" method="POST" action="{{ route('department.update', $department->id) }}">
		@csrf
		@method('PATCH')
		
		<div class="form-group">
			<div class="col-md-2"><label>Name</label></div>
			<div class="col-md-10">{{ $department->short_name }} - {{ $department->name }} ( {{ $department->central->name }} )
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>PIC <label style="color:red">*</label></label></div>
			<div class="col-md-6">
				<select id="pic_staff_id" name="pic_staff_id" class="form-control selectpicker" data-live-search="true">
						<option value="">Not Assign</option>
					@foreach($staffs as $staff)
						<option value="{{ $staff->id }}"
							@if($department->pic_staff_id == $staff->id)
								selected="selected"
							@endif
						>{{ $staff->staff_no }} - {{ $staff->user->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('pic_staff_id') }}</p>
			</div>
			<div class="col-md-4">
				<a href="{{ route('staff.create') }}" ><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Staff</a>
				<p style="font-size: small; color: red">**If the staff is not in the list**</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<a href="{{ route('department.index') }}"><button id="btnBack" type="button" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp; 
				<button id="btnSave" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Update</button>
				<br><br>
			</div>
		</div>
	</form>
@stop