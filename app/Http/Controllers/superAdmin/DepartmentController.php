<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Department;
use App\Staff;
use App\Central;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::all();
        $centrals = Central::all();
        $staffs = Staff::all();
        return view('pages.superAdmin.department',compact('department','centrals','staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'central_id' => 'required',
            'short_name' => 'required',
            'name' => 'required|unique:departments',
        ]);

        $department = Department::create($request->all());
        return redirect()->route('department.index')->with('success', 'Data inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::findOrFail($id);
        $staffs = Staff::all();
        return view('pages.superAdmin.departmentEdit',compact('department','staffs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'pic_staff_id' => 'required',
        ]);

        $user = Department::find($id);
        $user->fill($request->all());
        $user->save();
        return redirect()->route('department.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $department = Department::destroy($id);
        // return redirect()->route('department.index')->with('success', 'Data Successfully Removed!');
    }
}
