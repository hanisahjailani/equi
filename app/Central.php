<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Central extends Model
{
    protected $guarded = [];

    public function sub_location()
    {
        return $this->hasMany('App\SubLocation');
    }
}
