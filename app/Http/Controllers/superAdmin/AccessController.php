<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Staff;
use App\Access;
use App\RoleAccess;
use App\Central;
use App\AdminCentral;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $staff = Staff::all();
        return view('pages/superAdmin/roleAccess',compact('roles','staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accesses = Access::all();
        return view('pages/superAdmin/roleAccessCreate',compact('accesses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        $role = Role::create([
            'name' => $request->input('name'),
        ]);

        $accesses = $request->input('access');

        foreach($accesses as $access)
        {
            $roleAccess = RoleAccess::create([
                'role_id'=> $role->id,
                'access_id'=> $access,
            ]);
        }

        // dd($request->input('access'));
        return redirect()->route('role.index')->with('success','Role has been succesfully inserted!');
    }

    public function createStaff($id)
    {
        $role = Role::find($id);
        $staff = Staff::where('role_id',$id)->get();
        $listStaff = Staff::all();
        $centrals = Central::all();
        $adminCentrals = AdminCentral::all();
        return view('pages.superAdmin.roleAccessStaff',compact('role','staff','listStaff','centrals','adminCentrals'));
    }

    public function storeStaff(Request $request, $id)
    {
        //check
        $check = AdminCentral::where('staff_id',$request->input('staff_id'))->where('central_id',$request->input('central_id'))->get();
        if($check->isEmpty())
        {
            //update role_id
            $staff = Staff::find($request->input('staff_id'));
            $staff->role_id = $id;
            $staff->save();

            // select * from staff where name="" and role="";
            // Staff::where('name')->where('role')->get();
            //insert admin
            $adminCentral = AdminCentral::create([
                'central_id' => $request->input('central_id'),
                'staff_id' => $request->input('staff_id'),
            ]);

            return redirect()->route('roleStaff.create',$id);
        }
        else {
            return redirect()->route('roleStaff.create',$id)->with('failure','Already assign in this group');
        }

    }

    public function storeSuperAdmin(Request $request, $id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $accesses = Access::all();
        return view('pages.superAdmin.roleAccessEdit',compact('role','accesses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$id,
        ]);

        //update role name
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $access = $request->input('access');
        // $role_accesses = RoleAccess::where('role_id',$id)->get();

        //delete data that not in list
        $delete = RoleAccess::where('role_id',$id)->whereNotIn('access_id',$access)->get();

        foreach($delete as $del)
        {
            $del->delete();
        }

        foreach($access as $access)
        {
            $hasRow = RoleAccess::where('role_id',$id)->where('access_id',$access)->get();
            if($hasRow->isEmpty())
            {
                $insRoleAccess = RoleAccess::create([
                    'role_id' => $id,
                    'access_id' => $access,
                ]);
            }
        }

        return redirect()->route('role.index')->with('success','Role Accessibility has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyStaff($id,$staff,$roleid)
    {
        $admin = AdminCentral::destroy($id);

        //$admin = AdminCentral::where('id',$id);
        //$admin->delete();

        $check = AdminCentral::where('staff_id',$staff)->get();
        if($check->isEmpty())
        {
            $role = Staff::find($staff);
            $role->role_id = null;
            $role->save();

        }
        return redirect()->route('roleStaff.create',$roleid);
    }

    public function destroySuperAdmin($id,$roleid)
    {
        $check = Staff::where('role_id',$roleid)->count();
        if($check < 2)
        {
            return redirect()->route('roleStaff.create',$roleid)->with('failure', 'Super Admin role should have atleast one (1). Please assign new super admin to proceed remove the user');
        }
        else
        {
            $staff_disrole = Staff::find($id);
            $staff_disrole->role_id = null;
            $staff_disrole->save();
            return redirect()->route('roleStaff.create',$roleid);
        }
    }
}
