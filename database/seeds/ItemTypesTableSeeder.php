<?php

use Illuminate\Database\Seeder;

class ItemTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_types')->insert([
        	'name'=>'Laptop/Notebook',
        	'category_id'=>'1',
        ]);

        DB::table('item_types')->insert([
        	'name'=>'Printer',
        	'category_id'=>'1',
        ]);

        DB::table('item_types')->insert([
        	'name'=>'Printer Paper',
        	'category_id'=>'2',
        ]);

        DB::table('item_types')->insert([
        	'name'=>'Benang',
        	'category_id'=>'2',
        ]);

        DB::table('item_types')->insert([
            'name'=>'Buku',
            'category_id'=>'2',
        ]);

        DB::table('item_types')->insert([
            'name'=>'Bateri',
            'category_id'=>'2',
        ]);

        DB::table('item_types')->insert([
            'name'=>'Pen',
            'category_id'=>'2',
        ]);

         DB::table('item_types')->insert([
            'name'=>'Pembaris',
            'category_id'=>'2',
        ]);

          DB::table('item_types')->insert([
            'name'=>'Pemadam',
            'category_id'=>'2',
        ]);

           DB::table('item_types')->insert([
            'name'=>'File',
            'category_id'=>'2',
        ]);
    }
}
