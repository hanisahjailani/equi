<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationStatus extends Model
{
    protected $guarded = [];

    public function application_logs()
    {
    	return $this->hasMany('App\ApplicationLog');
    }
}
