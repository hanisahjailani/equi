@servers(['production' => 'asset@aset.utmspace.edu.my'])

@task('deploy', ['on' => 'production'])
    cd /var/www/html/aset.utmspace.edu.my
    git pull
    composer install --optimize-autoloader --no-dev
    php artisan migrate
    php artisan config:cache
    php artisan queue:restart
    php artisan route:cache
@endtask