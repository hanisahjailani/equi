@extends('layouts.user.user')

@section('content1')
<script>
$(document).ready(function() {
	$( "#request_date" ).datepicker({
		changeMonth: true,  
		changeYear:true,      
		minDate:0,
		dateFormat: "yy-mm-dd"
	});
});
</script>

<form action="{{ route('consumable.apply.store') }}" method="POST">
	@csrf

	<div class="form-group">
		<label>Request Date <label style="color: red">*</label></label>
		<input name="request_date" id="request_date" class="form-control" value="{{ old('request_date') }}" />
		
		@if ($errors->has('request_date'))
			<p style="color: red">{{ $errors->first('request_date') }}</p>
		@endif
	</div>

	<button class="btn btn-primary">Continue</button>
</form>

{{-- <div class="form-group">
	<label for="itemType">Item Types</label>
	<select class="form-control" id="itemType">
		<option value="">PLEASE SELECT AN ITEM TYPE</option>
		@foreach ($itemTypes as $itemType)
			<option value="{{ $itemType->id }}"{{ request('item_type') == $itemType->id ? ' selected' : '' }}>
				{{ $itemType->name }}
			</option>
		@endforeach
	</select>
</div>

<script>
	$(function () {
		$('#itemType').on('change', function () {
			var selected = $(this).val();
			window.location.href = window.location.pathname + "?item_type=" + selected;
			// window.href = "{{ url()->current() }}?item_type=" + selected;
		});
	})
</script> --}}

{{-- <table class="table">
	<thead>
		<tr>
			<th>Item</th>
			<th>Quantity</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($consumables as $consumable)
			<tr>
				<td>{{ $consumable->item->name }}</td>
				<td>
					<input name="quantity" id="quantity" class="form-control" />
				</td>
			</tr>
		@endforeach
	</tbody>
</table> --}}
@stop