@extends('layouts.superAdmin.roleAccess')

@section('content1')
<center><h2>Role Accessibility ({{ $role->name }})</h2></center>
&nbsp;<a href="{{ route('role.index') }}">Role Access</a> / Role Accessibility (Staff)
<hr>
@if($message = Session::get('failure'))
<div class="alert alert-danger">
	<p>{{ $message }}</p>
</div>
@endif
<div class="row">
        <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Add Staff
                </h4>
            </div>
                <div class="panel-body">
                    {{-- super admin --}}
                    @if($role->id == 1)
                    <form id="frmAddRole" method="POST" action="{{ route('roleSuperAdmin.store',$role->id) }}">
                    @csrf
                        <div class="form-group">
                            <div class="col-md-2">
                                Staff
                            </div>
                            <div class="col-md-10">
                                <select id="staff_id_super" name="staff_id_super" class="form-control selectpicker" data-lived-search="true">
                                    <option value="">Choose Staff</option>
                                    @foreach($listStaff->where('role_id',null) as $listStaff)
                                    <option value="{{ $listStaff->id }}">{{ $listStaff->staff_no}} - {{ $listStaff->user->name}}</option>
                                    @endforeach
                                </select>
                                <p style="color: red">{{ $errors->first('staff_id_super') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12" align="right">
                                <a href="{{ route('role.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp;
                                <button class="btn btn-success" name="btnSave" id="btnSave" type="submit"><i class="fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>
                    {{-- other --}}
                    @else
                    <form id="frmAddRole" method="POST" action="{{ route('roleStaff.store',$role->id) }}">
                    @csrf
                        <div class="form-group">
                            <div class="col-md-2">
                                Central
                            </div>
                            <div class="col-md-10">
                                <select id="central_id" name="central_id" class="form-control selectpicker" data-lived-search="true">
                                    <option value="">Choose Central</option>
                                    @foreach($centrals as $central)
                                    <option value="{{ $central->id }}">{{ $central->short_name }} - {{ $central->name }}</option>
                                    @endforeach
                                </select>
                                <p style="color: red">{{ $errors->first('central_id') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                Staff
                            </div>
                            <div class="col-md-10">
                                <select id="staff_id" name="staff_id" class="form-control selectpicker" data-lived-search="true">
                                    <option value="">Choose Staff</option>
                                    @foreach($listStaff->whereIn('role_id',[null,$role->id]) as $listStaff)
                                    <option value="{{ $listStaff->id }}">{{ $listStaff->staff_no}} - {{ $listStaff->user->name}}</option>
                                    @endforeach
                                </select>
                                <p style="color: red">{{ $errors->first('staff_id') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12" align="right">
                                <a href="{{ route('role.index') }}"><button id="btnBack" type="button" name="btnBack" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp;
                                <button class="btn btn-success" name="btnSave" id="btnSave" type="submit"><i class="fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    <div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                List of {{ $role->name }}
            </h4>
        </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="tblaccess" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="70%">Staff</th>
                                <th width="20%">Central</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($role->id == 1)
                            {{-- super admin --}}
                            @foreach($staff as $staff)
                            <tr>
                                <td>
                                    {{ $staff->staff_no}} - {{ $staff->user->name}}
                                </td>
                                <td>
                                    {{ $staff->department->central->short_name}}
                                </td>
                                <td align="center">
                                    <form id="frmDelete" action="{{ route('roleSuperAdmin.destroy',[$staff->id,$role->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button id="btnDeleteSuper" class="btn btn-danger" type="submit" onclick="return confirm('Are you sure want to remove this staff from this group?');" data-toggle="tooltip" data-placement="bottom" title="Remove"><i class="fas fa-times-circle"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            {{-- other --}}
                            @else

                            @foreach($adminCentrals as $adminCentral)
                            @if($adminCentral->staff->role_id == $role->id)
                            <tr>
                                <td>
                                    {{ $adminCentral->staff->staff_no}} - {{ $adminCentral->staff->user->name}}
                                </td>
                                <td>
                                    {{ $adminCentral->central->short_name}}
                                </td>
                                <td align="center">
                                    <form id="frmDelete" action="{{ route('roleStaff.destroy',[$adminCentral->id,$adminCentral->staff_id,$role->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button id="btnDelete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to remove this staff from this group?');" data-toggle="tooltip" data-placement="bottom" title="Remove"><i class="fas fa-times-circle"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br><br>
    </div>
</div>
@stop