<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreApplicationConsumablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_application_consumables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_application_id')->unsigned();
            $table->foreign('store_application_id')->references('id')->on('store_applications');
            $table->integer('consumable_id')->unsigned();
            $table->foreign('consumable_id')->references('id')->on('consumables');
            //nanti nk edit to quantity_request
            $table->integer('quantity')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_application_consumables');
    }
}
