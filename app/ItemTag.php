<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTag extends Model
{
    protected $guarded = [];

    public function asset()
    {
    	return $this->belongsTo('App\Asset','asset_id');
    }
}
