@extends('layouts.user.application')

@section('content1')
<center><h2>New Application</h2></center>
&nbsp;<a href="{{ route('userapplication.index') }}">Application</a> / Create New Application /
<hr>
<div class="alert alert-info">
	<center><i class="fas fa-sticky-note"></i>&nbsp;&nbsp;----------&nbsp;&nbsp;<i class="far fa-check-circle" style="color:grey;"></i></center> 
</div>
<br>
@if($message = Session::get('failure'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<form id="frmAdd" action="{{ route('userapplicationitemtype.store')}}" method="POST">
	@csrf
	<div class="row">
		<div class="col-md-12">
			@foreach($detail as $detail)
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-5"><label>Collection Date </label></div>
						<div class="col-md-7"><input name="collection_date" id="collection_date" class="form-control" value="{{ $detail['collection_date'] }}" readonly="readonly" />
						</div>
						<br><br>
					</div>
					<div class="form-group">
						<div class="col-md-5"><label>Return Date </label></div>
						<div class="col-md-7"><input name="return_date" id="return_date" class="form-control" value="{{ $detail['return_date'] }}" readonly="readonly"/>
						</div>
						<br><br>
					</div>						
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4"><label>Central Location </label></div>
						<div class="col-md-8">
							
							<input name="sub_location_id" id="sub_location_id" class="form-control" value="{{ $centralID->name }}" readonly="readonly"/>
							<input name="sub_location_id" id="sub_location_id" type="hidden" value="{{ $detail['sub_location_id'] }}"/>
							<p style="color: red">{{ $errors->first('sub_location_id') }}</p>
						</div>
						<br><br>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel-group">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" href="#collapse1"><strong> <i class="fas fa-plus-square"></i> Other Infomation</strong></a>
					      </h4>
					    </div>
					    <div id="collapse1" class="panel-collapse collapse">
					      <div class="panel-body">
					      	<div class="form-group">
								<div class="col-md-4"><label>Location Use </label>(Optional)</div>
								<div class="col-md-8"><input name="location_use" type="text" class="form-control" value="{{ $detail['location_use'] }}" readonly="readonly"/>
								</div>
								<br><br>
							</div>
							<div class="form-group">
								<div class="col-md-4"><label>Purpose </label>(Optional)</div>
								<div class="col-md-8"><input name="purpose" type="text" class="form-control" value="{{ $detail['purpose'] }}" readonly="readonly"/>
								</div>
								<br><br>
							</div>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>	
			@endforeach
			<hr>
			<input type="hidden" name="staff_id" value="{{ Auth::user()->staff->id }}">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
					<table id="tblItemsType" style="text-align: center;" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="75%">Item Type</th>
								<th width="10%">Requested</th>
								<th width="10%">Available</th>
							</tr>
						</thead>
						<tbody>
							@foreach($dataselected as $dataselected)

							@if($dataselected['available'] > $dataselected['request'])
							<tr style="background-color: #26de81">
								<input type="hidden" value="{{ $display = 'DISPLAY' }}">
							@elseif($dataselected['available'] < $dataselected['request'])
							<tr style="background-color: #fc5c65">
								<input type="hidden" value="{{ $display = 'FALSE' }}">
							@elseif($dataselected['available'] == $dataselected['request'])
							<tr style="background-color: #26de81">
								<input type="hidden" value="{{ $display = 'DISPLAY' }}">
							@endif 
								<td>{{ $loop->iteration }}</td>
								<td style="text-align: left;">{{ $dataselected['itemtypename']->name }} <input type="hidden" name="item_type_id[]" value="{{ $dataselected['itemtypename']->id }}"></td>
								<td>{{ $dataselected['request'] }}<input type="hidden" name="quantity_{{ $dataselected['itemtypename']->id }}" value="{{ $dataselected['request'] }}"></td>
								<td>{{ $dataselected['available'] }}<input type="hidden" name="available_{{ $dataselected['itemtypename']->id }}" value="{{ $dataselected['available'] }}"></td>
							</tr>
							@endforeach
						</tbody>
					</table>
					</div>
					@foreach($dataselected1 as $dataselected1)
						@if($dataselected1['available'] < $dataselected1['request'])
							<label style="color: red">** Available item type ( {{ $dataselected1['itemtypename']->name }} )  is not enough **</label><br>
						@endif 
					@endforeach
				</div>
			</div>
			<br>
			<!-- notify application collect date is not follow rule-->
			@foreach($configs as $config)
				@if(date('Y-m-d',strtotime($detail['collection_date'].'- '.$config->sla.' days')) <= date('Y-m-d'))
				<div class="alert alert-info">
					<p>The application is less than  {{ $config->sla }} day(s) before the date of item(s) will be use. It will be considered as <strong>late request</strong></p>
					<br>
					<input type="checkbox" name="remark" value="Late Request"> I aware of it and still want to proceed.
					<p style="color: red">{{ $errors->first('remark') }}</p>
				</div>
				@endif
			@endforeach 
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<input type="hidden" name="staff_id" value="{{ Auth::user()->staff->id }}">
				<button id="btnBack" type="button" name="btnBack" class="btn btn-danger" onClick="window.history.go(-1); return false;" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button>
				<button id="btnSave" type="submit" class="btn btn-success"><i class="fas fa-chevron-right"></i>&nbsp;Confirm</button>&nbsp;
				<br>
			</div>
		</div>
	</div>
</form>
<br>
<script>
	$(document).ready( function () {
		$('#tblItemsType').DataTable();
	} );
</script>
@stop