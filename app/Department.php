<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];

    public function staff()
    {
    	return $this->hasMany('App\Staff');
    }
    public function pic()
    {
    	return $this->belongsTo('App\Staff','pic_staff_id');
    }
    public function central()
    {
    	return $this->belongsTo('App\Central','central_id');
    }
}
