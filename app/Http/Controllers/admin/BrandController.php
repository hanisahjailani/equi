<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Brand;
use App\AdminCentral;
use App\SubLocation;
use DB;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        $admin = AdminCentral::where('staff_id',Auth::user()->id)->select('central_id')->get();
        $sub_locations = SubLocation::whereIn('central_id',$admin)->select('id')->get(); 
        $assets = DB::table('assets')
                ->join('item_models','assets.model_id','=','item_models.id')
                ->join('items','assets.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        $consumable = DB::table('consumables')
                ->join('items','consumables.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        $licenses = DB::table('licenses')
                ->join('items','licenses.item_id','=','items.id')
                ->whereIn('items.sub_location_id',$sub_locations)
                ->get();
        return view('pages.admin.brands',compact('brands','assets','consumable','licenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $brand = Brand::create($request->all());
        return redirect()->route('adminbrands.index')->with('success', 'Data Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $brand = Brand::find($id);
        $brand->fill($request->all());
        $brand->save();
        return redirect()->route('adminbrands.index')->with('success', 'Data Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
