<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ItemType;
use App\Item;
use App\Application;
use App\ApplicationLog;
use App\ApplicationItemType;
use App\ApplicationItem;
use App\SubLocation;
use DB;

class ApplicationItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'collection_date' => 'required',
            'return_date' => 'required',
            'item_type_id' => 'required',
            'sub_location_id' => 'required',
        ]);

        $centralID = SubLocation::find($request->input('sub_location_id'));
        $dataselected = [];
        $detail = [];
        $itemtypes = $request->input('item_type_id');

        foreach ($itemtypes as $itemtype)
        {
            //get item type name
            $row = ItemType::findOrFail($itemtype);

            //get quantity request
            $quantity = $request->input('quantity_'.$itemtype);

            //get item type available
            $hasItemRow = DB::table('assets')
                            ->join('items','assets.item_id','=','items.id')
                            ->join('item_models','assets.model_id','=','item_models.id')
                            ->join('item_types','item_models.item_type_id','=','item_types.id')
                            ->where('item_types.id',$itemtype)
                            ->where('items.sub_location_id',$request->input('sub_location_id'))
                            ->whereIn('items.item_status_id',['1','2','6'])
                            ->where('assets.requestable','Yes')
                            ->whereNull('assets.deleted_at')
                            ->whereNotIn('items.id',function ($query) use ($request) {
                                            $query->select('application_items.item_id')
                                            ->from('application_items')
                                            ->join('applications','application_items.application_id','=','applications.id')
                                            ->where('applications.collection_date','<=',$request->input('return_date'))
                                            ->Where('applications.return_date','>=',$request->input('collection_date'))
                                            ->whereNull('applications.actual_collection_date')
                                            ->WhereNull('applications.actual_return_date')
                                            ->whereIn('applications.application_status_id',['1','2','5','7']);
                                        })
                            ->count();
            //array item type selected
            $dataselected[] = ['itemtypename' => $row, 'request' => $quantity, 'available' => $hasItemRow]; 
            $dataselected1[] = ['itemtypename' => $row, 'request' => $quantity, 'available' => $hasItemRow]; 
            $dataselected2[] = ['itemtypename' => $row, 'request' => $quantity, 'available' => $hasItemRow]; 
        }

        // dd($hasItemRow);
        // array detail application
        $detail[] = ['collection_date' => $request->input('collection_date'), 'return_date' => $request->input('return_date'),'location_use' => $request->input('location_use'), 'purpose' => $request->input('purpose'),'sub_location_id' => $request->input('sub_location_id')];

        // $request->session()->put('return_date',$request->input('return_date'));
        // $request->session()->put('collection_date',$request->input('collection_date'));

        return view('pages.superAdmin.applicationsCreateItemType',compact('dataselected','detail','dataselected1','dataselected2','centralID'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create application
        $application = Application::create([
            'location_use' => $request->input('location_use'),
            'purpose' => $request->input('purpose'),
            'collection_date' => $request->input('collection_date'),
            'return_date' => $request->input('return_date'),
            'request_date' => date('Y-m-d H:i:s'),
            'application_status_id' => 1,
            'sub_location_id' => $request->input('sub_location_id')
        ]);

        //update table application add booking id
        $bookingID = str_pad($application->id,6,"0",STR_PAD_LEFT);
        $bookingApp = Application::find($application->id);
        $bookingApp->booking_id = $bookingID;
        $bookingApp->save();

        //update table application log
        $applicationlog = ApplicationLog::create([
            'application_id' => $application->id,
            'staff_id' => $request->input('staff_id'),
            'application_status_id' => '1',
        ]);

        $itemtypes = $request->input('item_type_id');

        //add item based on quantity choosen
        foreach ($itemtypes as $itemtype)
        {
            $quantity = $request->input('quantity_'.$itemtype);
            $available = $request->input('available_'.$itemtype);
            if(($available > $quantity) || ($available == $quantity))
            {
                $row = ItemType::findOrFail($itemtype);

                //create application item type
                $applicationitemtype = ApplicationItemType::create([
                    'application_id' => $application->id,
                    'item_type_id' => $row->id,
                    'quantity' => $quantity,
                ]);

                $qtyselect = $quantity+1;
                for($qty=1; $qty < $qtyselect ; $qty++)
                {
                    //select max item available based on item type
                    $item = DB::table('assets')
                                ->join('items','assets.item_id','=','items.id')
                                ->join('item_models','assets.model_id','=','item_models.id')
                                ->join('item_types','item_models.item_type_id','=','item_types.id')
                                ->where('item_types.id',$itemtype)
                                ->where('items.sub_location_id',$request->input('sub_location_id'))
                                ->whereIn('items.item_status_id',['1','2'])
                                ->where('assets.requestable','Yes')
                                ->whereNotIn('items.id',function ($query) use ($request) {
                                                $query->select('application_items.item_id')
                                                ->from('application_items')
                                                ->join('applications','application_items.application_id','=','applications.id')
                                                ->where('applications.collection_date','<=',$request->input('return_date'))
                                                ->Where('applications.return_date','>=',$request->input('collection_date'))
                                                ->whereNull('applications.actual_collection_date')
                                                ->WhereNull('applications.actual_return_date')
                                                ->whereIn('applications.application_status_id',['1','2','5','7']);
                                            })
                                ->max('items.id');

                    //update item status to '6'-booked
                    $itemselected = Item::findOrFail($item);
                    $itemselected->item_status_id = '6';
                    $itemselected->save();

                    // create application item
                    $applicationitem = ApplicationItem::create([
                        'application_id' => $application->id,
                        'item_id' => $item,
                    ]);
                    // dd($item);
                } 
            } 
        }

        return redirect()->route('application.edit',$application->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
