<?php

namespace App\Http\Controllers\superAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Application;
use App\Item;
use App\Staff;
use App\ItemStatus;
use App\ApplicationLog;
use App\ApplicationStatus;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\Asset;
use App\Consumable;
use App\License;
use App\Configuration;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(date('Y-m-d'));
        // APPLICATION_STATUS: 2 = ACCEPT; 5 = RECEIVE; 7 = OVERDUE;

        $collection_date = Application::where('collection_date',date('Y-m-d'))->get();
        $return_date = Application::where('return_date',date('Y-m-d'))->get();
        $overdue = Application::where('application_status_id',7)->count();
        $asset = Asset::all()->count();
        $consumable = Consumable::all()->count();
        $license = License::all()->count();
        return view('pages.superAdmin.dashboard',compact('collection_date','return_date','overdue','asset','consumable','license'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function cancel($id)
    {
        $application = Application::find($id);
        return view();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::findOrFail($id);
        $configs = Configuration::all();
        return view('pages.superAdmin.reservationShow',compact('application','configs'));
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showList($id)
    {
        $applicationStatus = ApplicationStatus::findOrFail($id);
        // dd($applicationStatus->name);
        if($applicationStatus->name == "Approved")
        {
            $collection_date = Application::where('collection_date',date('Y-m-d'))->where('application_status_id',$id)->get();
            return view('pages.superAdmin.reservationShowPickup',compact('collection_date'));
        }
        elseif ($applicationStatus->name == "Receive") 
        {
            $return_date = Application::where('return_date',date('Y-m-d'))->where('application_status_id',$id)->get();
            return view('pages.superAdmin.reservationShowReturn',compact('return_date'));
        }   
        elseif ($applicationStatus->name == "Overdue") 
        {
            $overdue = Application::where('application_status_id',$id)->get();
            return view('pages.superAdmin.reservationShowOverdue',compact('overdue'));
        }      
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applications = Application::where('id',$id)->get();
        $applicationss = Application::findOrFail($id);
        $applicationItems = ApplicationItem::where('application_id',$id)->get();
        foreach ($applications as $application) {
            // dd($application->application_status_id);
            $staff = Staff::all();
            if($application->application_status->name == "Approved")
            {
                return view('pages.superAdmin.reservationEditPickup',compact('applicationItems','applicationss','staff'));
            }
            elseif($application->application_status->name == "Receive" || $application->application_status->name == "Overdue")
            {
                if($application->actual_collection_date == null)
                {
                    return view('pages.superAdmin.reservationEditPickup',compact('applicationItems','applicationss','staff'));
                }
                else
                {
                    $item_statuses = ItemStatus::whereIn('id',['1','3','4'])->get();
                    return view('pages.superAdmin.reservationEditReturn',compact('applicationItems','applicationss','staff','item_statuses'));
                }
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'staff_id' => 'required',            
        ]);

        $applications = Application::where('id',$id)->get();
        foreach ($applications as $application) { 
           if($application->application_status->name == "Approved")
            {
                $applicationLog = ApplicationLog::create([
                    'remark' => $request->input('remark'),
                    'remark_to_applicant' => $request->input('remark_to_applicant'),
                    'application_id' => $id,
                    'staff_id' => Auth::user()->id,
                    'application_status_id' => '5',
                ]);

                $items = ApplicationItem::where('application_id',$id)->get();
                foreach ($items as $item) {
                    $itemStatus = Item::findOrFail($item->item_id);
                    $itemStatus->item_status_id = '2';
                    $itemStatus->save();
                    
                }

                $applicationStatus = Application::findOrFail($id);
                $applicationStatus->application_status_id = '5';
                $applicationStatus->actual_collection_date = date('Y-m-d');
                $applicationStatus->pick_staff_id = $request->input('staff_id');
                $applicationStatus->save();
               
                return redirect()->route('dashboard.index');
            }
            elseif($application->application_status->name == "Receive" || $application->application_status->name == "Overdue")
            {
                $applicationLog2 = ApplicationLog::create([
                    'application_status_id' => '6',
                    'remark' => $request->input('remark'),
                    'remark_to_applicant' => $request->input('remark_to_applicant'),
                    'application_id' => $application->id,
                    'staff_id' => Auth::user()->id,
                ]);

                $items2 = ApplicationItem::where('application_id',$id)->get();
                foreach ($items2 as $item2) {
                    // dd($request->input('item_status_id_'.$item2->item_id));
                    $itemStatus2 = Item::findOrFail($item2->item_id);
                    $itemStatus2->item_status_id = $request->input('item_status_id_'.$item2->item_id);
                    $itemStatus2->save();

                    $item2->item_status_id = $request->input('item_status_id_'.$item2->item_id);
                    $item2->save();
                }

                $applicationStatus2 = Application::findOrFail($id);
                $applicationStatus2->application_status_id = '6';
                $applicationStatus2->actual_return_date = date('Y-m-d');
                $applicationStatus2->return_staff_id = $request->input('staff_id');
                $applicationStatus2->save();

                return redirect()->route('dashboard.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
