<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

use App\Application;
use App\ApplicationLog;

class ProcessOverdue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //application_status_id(5) = receive
        $appOverdue = Application::where('return_date','<',date('Y-m-d'))->whereNull('actual_return_date')->whereIn('application_status_id',['5','2'])->select('id')->get();
        // dd($appOverdue);
        Log::info('List overdue data '.$appOverdue);
        foreach ($appOverdue as $overdue) {
            $overdue->application_status_id = 7;
            $overdue->save();
            Log::info('Successfull update data '.$overdue);
            //create application log
            $applicationlog = ApplicationLog::create([
            'application_id' => $overdue->id,
            'application_status_id' => 7,
            ]);
            Log::info('Successfull create application log '.$applicationlog);
        }
    }
}
