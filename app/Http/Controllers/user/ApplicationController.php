<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Application;
use App\ApplicationItem;
use App\ApplicationItemType;
use App\ApplicationLog;
use App\Staff;
use App\ItemType;
use App\Configuration;
use App\SubLocation;
use App\Item;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::where('staff_id', Auth::user()->id)->whereIn('application_status_id',['1','2','5','7'])->get();
        $configs = Configuration::all();
        return view('pages.user.applications',compact('applications','configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemtype = ItemType::where('category_id',1)->get();
        $centrals = SubLocation::where('central_id',Auth::user()->staff->department->central_id)->get();
        $applications = Application::all();
        return view('pages.user.applicationsCreate',compact('itemtype','centrals','applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::findOrFail($id);
        $applicationItems = ApplicationItem::where('application_id',$id)->get();
        $applicationItemTypes = ApplicationItemType::where('application_id',$id)->get();
        $applicationLogs = ApplicationLog::where('application_id',$id)->get();
        $configs = Configuration::all();
        return view('pages.user.applicationsShow',compact('application','applicationItems','applicationItemTypes','applicationLogs','configs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $application = Application::findOrFail($id);
        $applicationitemtypes = ApplicationItemType::where('application_id',$id)->get();
        $applicationItems = ApplicationItem::where('application_id',$application->id)->get();
        return view('pages.user.applicationsCancelLog',compact('application','applicationItems','applicationitemtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'remark_to_applicant' => 'required',
        ]);

        //update application_status_id(4) = cancelled
        $application = Application::findOrFail($id);
        $application->application_status_id = 4;
        $application->save();

        foreach($application->application_item as $appItem)
        {
            $item = Item::find($appItem->item_id);
            $item->item_status_id = 1;
            $item->save();

            $appItem->item_status_id = 1;
            $appItem->save();
        }

        $applicationlog = ApplicationLog::create([
            'remark' => $request->input('remark_to_applicant'),
            'remark_to_applicant' => $request->input('remark_to_applicant'),
            'application_id' => $application->id,
            'staff_id' => Auth::user()->staff->id,
            'application_status_id' => 4,
        ]);
        

        return redirect()->route('userapplicationhistory.index')->with('success', 'Successfully cancelled the application!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
