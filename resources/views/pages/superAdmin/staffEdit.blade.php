@extends('layouts.superAdmin.staff')

@section('content1')
<center><h2>Edit Staff Detail</h2></center>
&nbsp;<a href="{{ route('staff.index') }}">Setting</a> / <a href="{{ route('staff.index') }}">Staff</a> / Edit Staff Details
<hr>
	<form id="frmEdit" method="POST" action="{{ route('staff.update', $staff->id) }}">
		@csrf
		@method('PATCH')

		<div class="form-group">
			<div class="col-md-2"><label>Staff No</label></div>
			<div class="col-md-10"><input id="staff_no" name="staff_no" type="text" class="form-control" value="{{ old('staff_no',$staff->staff_no) }}" readonly="readonly">
			<p style="color: red">{{ $errors->first('staff_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Name</label></div>
			<div class="col-md-10"><input id="name" name="name" type="text" class="form-control" value="{{ old('name',$staff->user->name) }}" readonly="readonly">
				<p style="color: red">{{ $errors->first('name') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Email</label></div>
			<div class="col-md-10"><input id="email" name="email" type="email" class="form-control" value="{{ old('email',$staff->user->email) }}" readonly="readonly">
			<p style="color: red">{{ $errors->first('email') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"><label>Phone No</label></div>
			<div class="col-md-10"><input id="phone_no" name="phone_no" type="text" class="form-control" value="{{ old('phone_no',$staff->phone_no) }}">
				<p style="color: red">{{ $errors->first('phone_no') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class=" col-md-2"><label>Department</label></div>
			<div class="col-md-10">
				<select id="department_id" name="department_id" class="form-control selectpicker" data-live-search="true">
					@foreach($department as $department)
						<option value="{{ $department->id }}"
							@if($department->id == $staff->department_id)
								selected="selected"
							@endif
						>({{ $department->central->short_name }}) {{ $department->short_name }} - {{ $department->name }}</option>
					@endforeach
				</select>
			<p style="color: red">{{ $errors->first('department_id') }}</p>
			</div>
			<br><br>
		</div>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-10" style="float:right;">
				<a href="{{ route('staff.index') }}"><button id="btnBack" type="button" class="btn btn-danger" ><i class="fas fa-chevron-left"></i>&nbsp;Back</button></a>&nbsp;
				<button id="btnSave" type="submit" class="btn btn-success"><i class="far fa-save"></i>&nbsp;Save</button>
				<br><br>
			</div>
		</div>
	</form>
@stop